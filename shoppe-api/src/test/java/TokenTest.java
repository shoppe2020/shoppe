import app.ShoppeApp;
import app.config.auth.TokenDetails;
import app.config.auth.TokenService;
import dao.domain.Product;
import org.bson.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.HttpClientErrorException;
import service.exception.TestException;
import service.test.TestAssertedService;

import java.util.Date;

@SpringBootTest(classes = ShoppeApp.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles(profiles = "test")
@ContextConfiguration(classes = TestAppConfig.class)
public class TokenTest {

    @LocalServerPort
    public int port = 8444;

    @Autowired
    private TestAssertedService testAssertedService;

    @Autowired
    private MongoTemplate mongo;

    @Autowired
    private TokenService tokenService;

    @BeforeEach
    void setUp() {
        tokenService.setAccessTokenExpirationTime(10 * 1000);
        tokenService.setRefreshTokenExpirationTime(60 * 1000);
    }

    @Test
    public void testRefreshToken() throws InterruptedException {
        String tempUserId = testAssertedService.generateTempUser();
        Page<Product> tempProducts = testAssertedService.getProductsWithTemp(tempUserId);
        TokenDetails tokenDetails = testAssertedService.loginAsUser();
        Page<Product> tokenProducts = testAssertedService.getProductsWithToken(tokenDetails.getAccessToken());

        waitUntilRefreshTokenExpire(tokenDetails.getAccessExpire());

        try {
            testAssertedService.getProductsWithToken(tokenDetails.getAccessToken());
            throw new TestException("Correct response while expired token");
        } catch (HttpClientErrorException hcee) {
            validateTokenExpirationException(hcee);
            TokenDetails refreshedTokenDetails = testAssertedService.refreshToken(tokenDetails);
            testAssertedService.getProductsWithToken(refreshedTokenDetails.getAccessToken());
        }
    }

    @Test
    public void testLogout() {
        String tempUserId = testAssertedService.generateTempUser();
        Page<Product> tempProducts = testAssertedService.getProductsWithTemp(tempUserId);
        TokenDetails tokenDetails = testAssertedService.loginAsUser();
        Page<Product> tokenProducts = testAssertedService.getProductsWithToken(tokenDetails.getAccessToken());
        boolean loggedOut = testAssertedService.logout(tokenDetails.getAccessToken(), tokenDetails.getSessionId());
        try {
            testAssertedService.getProductsWithToken(tokenDetails.getAccessToken());
            throw new TestException("Correct response while expired token");
        } catch (Exception ex) {
        }
    }

    private void validateTokenExpirationException(HttpClientErrorException hcee) {
        try {
            String exMessage = hcee.getMessage();
            String messageJson = exMessage.substring(7, exMessage.length() - 1);
            Document messageDoc = Document.parse(messageJson);
            boolean tokenExpired = (boolean) messageDoc.get("tokenExpired");
            Assertions.assertTrue(tokenExpired);
            Assertions.assertTrue(hcee.getStatusCode().equals(HttpStatus.FORBIDDEN));
        } catch (Exception ex) {
            throw new TestException("Error during validate expiration exception: " + ex.getMessage());
        }
    }

    private void waitUntilRefreshTokenExpire(Long accessExpire) throws InterruptedException {
        long actualTime = new Date().getTime();
        long delay = Math.abs(accessExpire - actualTime);
        System.out.println("Waiting: " + delay / 1000 + " s");
        Thread.sleep(delay);
    }
}
