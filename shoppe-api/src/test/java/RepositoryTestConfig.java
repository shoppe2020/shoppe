import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.io.IOException;

@Configuration
@EnableMongoRepositories(basePackages = {"dao"})
@Profile("repoTest")
@SuppressWarnings("unchecked")
public class RepositoryTestConfig {

    private static final String IP = "localhost";
    private static final int PORT = 28017;

    @Bean
    public IMongodConfig embeddedMongoConfiguration() throws IOException {
        return new MongodConfigBuilder()
                .version(Version.V4_0_2) // <- set MongoDB version
                .net(new Net(IP, PORT, Network.localhostIsIPv6()))
                .build();
    }

}
