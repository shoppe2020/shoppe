import dao.domain.Product;
import dao.repo.ProductRepository;
import dto.product.ProductBaseDTO;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Map;
import java.util.Set;


@DataMongoTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(profiles = "repoTest")
@ContextConfiguration(classes = RepositoryTestConfig.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class ProductRepositoryTest {


    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @AfterAll
    public void clear() {
        mongoTemplate.dropCollection(Product.class);
    }

    private void saveProduct(String groupName, Map<String, String> params) {
        productRepository.save(Product.builder()
                .group(groupName)
                .parameters(params)
                .build());
    }

    @BeforeAll
    public void setup() {
        saveProduct("0.16", Map.of("Model", "m1", "Seria", "s1", "Typ", "t1"));
        saveProduct("0.16.0", Map.of("Model", "m2", "Seria", "s2", "Typ", "t2"));
        saveProduct("0.1", Map.of("Model", "m3", "Seria", "s3", "Typ", "t3"));
        saveProduct("0.1.1", Map.of("Model", "m4", "Seria", "s4", "Typ", "t4"));
        saveProduct("0.1.1", Map.of("Model", "m5", "Seria", "s5", "Typ", "t5"));
        saveProduct("0.1.1", Map.of("Model", "m4", "Seria", "s6", "Typ", "t4"));
        saveProduct("0.1.1", Map.of("Model", "m6", "Seria", "s4", "Typ", "t4"));
        saveProduct("0.1.1", Map.of("Model", "m4", "Seria", "s5", "Typ", "t5"));
        saveProduct("0.1.1", Map.of("Model", "m4", "Seria", "s6", "Typ", "t6"));
        saveProduct("0.1.1", Map.of("Model", "m4", "Seria", "s4", "Typ", "t4"));
        saveProduct("0.1.1", Map.of("Model", "m6", "Seria", "s6", "Typ", "t4"));
        saveProduct("0.1.1", Map.of("Model", "m4", "Seria", "s5", "Typ", "t5"));
        saveProduct("0.1.1", Map.of("Model", "m5", "Seria", "s4", "Typ", "t4"));
        saveProduct("0.1.1", Map.of("Model", "m4", "Seria", "s4", "Typ", "t6"));
        saveProduct("0.1.1", Map.of("Model", "m4", "Seria", "s6", "Typ", "t4"));

        long count = productRepository.count();
        System.out.println("ALL PRODUCTS: " + count);

    }

    @Test
    @Order(1)
    public void testFilterProducts() {
        MultiValueMap<String, List<String>> filters = new LinkedMultiValueMap<>() {{
//            put("group", new ArrayList<>(Arrays.asList()));
        }};

        String group = "0.16";
        PageRequest pageable = PageRequest.of(0, 1000, Sort.by(Sort.Order.asc("group")));
        Page<ProductBaseDTO> allFilter = productRepository.findAllFilter(filters, group, null,false, pageable, mongoTemplate);
        long total = allFilter.getTotalElements();
        List<ProductBaseDTO> results = allFilter.getContent();
        Assertions.assertTrue(total == 2);
        Assertions.assertNotNull(results);
        Assertions.assertFalse(results.isEmpty());
        Assertions.assertTrue(results.size() == 2);
        Assertions.assertTrue(results.get(0).getGroup().equals("0.16"));
        Assertions.assertTrue(results.get(1).getGroup().equals("0.16.0"));
    }

    @Test
    @Order(2)
    public void testFilterProducts2() {
        MultiValueMap<String, List<String>> filters = new LinkedMultiValueMap<>() {{
            //  put("group", new ArrayList<>(Arrays.asList("0.1")));
        }};
        String group = "0.1";
        PageRequest pageable = PageRequest.of(0, 1000, Sort.by(Sort.Order.asc("group")));
        Page<ProductBaseDTO> allFilter = productRepository.findAllFilter(filters, group, null, false, pageable, mongoTemplate);
        long total = allFilter.getTotalElements();
        List<ProductBaseDTO> results = allFilter.getContent();
        Assertions.assertTrue(total == 13);
        Assertions.assertNotNull(results);
        Assertions.assertFalse(results.isEmpty());
        Assertions.assertTrue(results.size() == 13);
        Assertions.assertTrue(results.get(0).getGroup().equals("0.1"));
    }

    @Test
    @Order(3)
    public void testFilterProducts3() {
        MultiValueMap<String, List<String>> filters = new LinkedMultiValueMap<>() {{
            //put("group", new ArrayList<>(Arrays.asList("0.1")));
        }};
        String group = "0.1";
        PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Order.asc("group")));
        Page<ProductBaseDTO> allFilter = productRepository.findAllFilter(filters, group, null, false,pageable, mongoTemplate);
        long total = allFilter.getTotalElements();
        List<ProductBaseDTO> results = allFilter.getContent();
        Assertions.assertTrue(total == 13);
        Assertions.assertNotNull(results);
        Assertions.assertFalse(results.isEmpty());
        Assertions.assertTrue(results.size() == 10);
        Assertions.assertTrue(results.get(0).getGroup().equals("0.1"));
    }

    @Test
    @Order(4)
    public void getProductsParameters() {

        Map<String, Map<String, Set<String>>> productsParameters = productRepository.getProductsParameters(mongoTemplate);
        System.out.println();
    }
}
