import app.ShoppeApp;
import app.config.auth.TokenDetails;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;
import dto.cart.CartResponseDTO;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import service.test.TestAssertedService;

import java.util.ArrayList;
import java.util.Map;

@SpringBootTest(classes = ShoppeApp.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles(profiles = "test")
@ContextConfiguration(classes = TestAppConfig.class)
public class ProductTest {

    @LocalServerPort
    public int port = 8444;

    @Autowired
    private TestAssertedService testAssertedService;

    @Autowired
    private MongoTemplate mongo;

    @BeforeEach
    void setUp() {
        clearDefaultUserCart();
    }

    @Test
    public void testGetCart() {
        TokenDetails tokenDetails = testAssertedService.loginAsUser();
        CartResponseDTO cart = testAssertedService.getCart(null, tokenDetails.getAccessToken());
        Assertions.assertNotNull(cart.getProducts());
        Assertions.assertTrue(cart.getProducts().size() == 0);

        String productId = testAssertedService.getProducts(null, tokenDetails.getAccessToken()).get().findFirst().get().getId();
        testAssertedService.addToCart(null, tokenDetails.getAccessToken(), Map.of(productId, 1));
        cart = testAssertedService.getCart(null, tokenDetails.getAccessToken());
        Assertions.assertNotNull(cart.getProducts());
        Assertions.assertTrue(cart.getProducts().size() == 1);

    }

    @Test
    public void testAddToCart() {
        TokenDetails tokenDetails = testAssertedService.loginAsUser();
        String productId = testAssertedService.getProducts(null, tokenDetails.getAccessToken()).get().findFirst().get().getId();
        CartResponseDTO cart = testAssertedService.addToCart(null, tokenDetails.getAccessToken(), Map.of(productId, 1));
        Assertions.assertNotNull(cart.getProducts());
        Assertions.assertTrue(cart.getProducts().size() > 0);
    }

    @Test
    public void testRemoveFromCart() {
        TokenDetails tokenDetails = testAssertedService.loginAsUser();
        CartResponseDTO cart = testAssertedService.getCart(null, tokenDetails.getAccessToken());
        Assertions.assertNotNull(cart.getProducts());
        Assertions.assertTrue(cart.getProducts().size() == 0);

        String productId = testAssertedService.getProducts(null, tokenDetails.getAccessToken()).get().findFirst().get().getId();
        testAssertedService.addToCart(null, tokenDetails.getAccessToken(), Map.of(productId, 1));
        cart = testAssertedService.getCart(null, tokenDetails.getAccessToken());
        Assertions.assertNotNull(cart.getProducts());
        Assertions.assertTrue(cart.getProducts().size() == 1);


        testAssertedService.removeFromCart(null, tokenDetails.getAccessToken(), Map.of(productId, 1));
        cart = testAssertedService.getCart(null, tokenDetails.getAccessToken());
        Assertions.assertNotNull(cart.getProducts());
        Assertions.assertTrue(cart.getProducts().size() == 0);

    }

    private void clearDefaultUserCart() {
        Bson userFilter = Filters.eq("email", "user@user.pl");
        Document user = mongo.getCollection("user").find(userFilter).first();
        String cartId = (String) user.get("cartId");
        Bson cartFilter = Filters.eq("_id", new ObjectId(cartId));
        Bson update = Updates.set("products", new ArrayList<>());
        UpdateResult updateResult = mongo.getCollection("cart").updateOne(cartFilter, update);
        Assertions.assertTrue(updateResult.getMatchedCount() == 1 && updateResult.getModifiedCount() == 1);
    }
}
