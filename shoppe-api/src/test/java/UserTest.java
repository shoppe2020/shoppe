import app.ShoppeApp;
import app.config.auth.TokenDetails;
import app.config.auth.TokenRequest;
import dao.domain.Address;
import dto.user.UserDTO;
import dto.user.UserRequestDTO;
import dto.user.UserUpdateRequestDTO;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.HttpClientErrorException;
import service.EmailService;
import service.test.TestAssertedService;
import service.test.TestRawService;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;

@SpringBootTest(classes = ShoppeApp.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles(profiles = "test")
@ContextConfiguration(classes = TestAppConfig.class)
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
public class UserTest {

    @LocalServerPort
    public int port = 8444;

    @Autowired
    private TestAssertedService testAssertedService;

    @Autowired
    private TestRawService testRawService;

    @Autowired
    private EmailService emailService;

    @BeforeEach
    void setUp() {
    }

    @Test
    @Order(1)
    public void testRegisterUser() {
        String tempUserId = testAssertedService.generateTempUser();
        UserRequestDTO userRequestDTO = UserRequestDTO
                .builder()
                .email("test@test.pl")
                .firstName("testFirst")
                .lastName("testLast")
                .password("test")
                .build();

        UserDTO userDTO = testAssertedService.registerUser(tempUserId, userRequestDTO);
        Assertions.assertTrue(userDTO.getFirstName().equals(userRequestDTO.getFirstName()));
        Assertions.assertTrue(userDTO.getLastName().equals(userRequestDTO.getLastName()));
        Assertions.assertTrue(userDTO.getEmail().equals(userRequestDTO.getEmail()));
    }

    @Test
    @Order(2)
    public void testRegisterUserThrowEmailValidationEx() {
        String tempUserId = testAssertedService.generateTempUser();
        UserRequestDTO userRequestDTO = UserRequestDTO
                .builder()
                .email("12f1f1f")
                .firstName("testFirst")
                .lastName("testLast")
                .password("test")
                .build();

        assertThatThrownBy(() -> {
            UserDTO userDTO = testAssertedService.registerUser(tempUserId, userRequestDTO);
            Assertions.assertTrue(userDTO.getFirstName().equals(userRequestDTO.getFirstName()));
            Assertions.assertTrue(userDTO.getLastName().equals(userRequestDTO.getLastName()));
            Assertions.assertTrue(userDTO.getEmail().equals(userRequestDTO.getEmail()));
        }).isInstanceOf(HttpClientErrorException.class)
                .hasMessageStartingWith("400 : [{\"status\":400,\"errorCode\":\"E006\",\"description\":\"Validation exception\"");
    }


    @Test
    @Order(3)
    public void testUpdateUser() {
        String tempUserId = testAssertedService.generateTempUser();
        UserRequestDTO userRequestDTO = UserRequestDTO
                .builder()
                .email("update@test.pl")
                .firstName("updateTestFirst")
                .lastName("updateTestLast")
                .password("updateTest")
                .build();
        doNothing().when(emailService).sendEmailToken(anyString(), anyString());


        UserDTO userDTO = testAssertedService.registerUser(tempUserId, userRequestDTO);
        Assertions.assertTrue(userDTO.getFirstName().equals(userRequestDTO.getFirstName()));
        Assertions.assertTrue(userDTO.getLastName().equals(userRequestDTO.getLastName()));
        Assertions.assertTrue(userDTO.getEmail().equals(userRequestDTO.getEmail()));

        TokenRequest tokenRequest = TokenRequest.builder().email(userDTO.getEmail()).password(userRequestDTO.getPassword()).build();
        TokenDetails tokenDetails = testAssertedService.login(tempUserId, tokenRequest);

        Address address = Address.builder()
                .city("Poznan")
                .street("Rocha")
                .streetNo("11a")
                .zipCode("61-142")
                .country("Poland")
                .build();

        UserUpdateRequestDTO requestDTO = UserUpdateRequestDTO.builder()
                .address(address)
                .password("updateTest")
                .build();

        UserDTO userUpdateDTO = testRawService.updateUser(tokenDetails.getAccessToken(), requestDTO);
        Assertions.assertNotNull(userUpdateDTO.getAddress());
        Assertions.assertTrue(userUpdateDTO.getAddress().equals(address));
    }

    @Test
    @Order(4)
    public void testGetUsers() {
        TokenRequest tokenRequest = TokenRequest.builder()
                .email("admin@admin.pl")
                .password("admin")
                .build();

        TokenDetails tokenDetails = testAssertedService.login(null, tokenRequest);
        Page<UserDTO> users = testRawService.getUsers(tokenDetails.getAccessToken(), 0, 10);

        Assertions.assertNotNull(users);
        Assertions.assertFalse(users.isEmpty());
        Assertions.assertNotNull(users.getContent());
        Assertions.assertFalse(users.getContent().isEmpty());
    }
}
