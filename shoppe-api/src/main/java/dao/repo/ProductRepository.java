package dao.repo;


import com.querydsl.core.types.dsl.StringExpression;
import dao.domain.Product;
import dao.domain.QProduct;
import dto.product.ProductBaseDTO;
import org.bson.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.stereotype.Repository;
import org.springframework.util.MultiValueMap;
import service.aggreation.AggregationsUtil;

import java.util.*;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Repository
public interface ProductRepository extends MongoRepository<Product, String>,
        QuerydslPredicateExecutor<Product>, QuerydslBinderCustomizer<QProduct> {

    @Override
    default void customize(QuerydslBindings bindings, QProduct product) {
        bindings.bind(product.group)
                .first(StringExpression::startsWith);
    }

    @Query(value = "{ '_id' : ?0 }", fields = "{ 'name' : 1, 'description': 1, 'count' : 1, 'brutto' : 1, 'netto' : 1, 'tax' : 1, 'group' : 1, 'thumbnail' : 1, 'parameters' : 1 , 'serialNumber' : 1}")
    Optional<ProductBaseDTO> findBaseByName(String id);

    default Map<String, Map<String, Set<String>>> getProductsParameters(MongoTemplate mongoTemplate) {
        Map<String, Map<String, Set<String>>> parametersGroups = new TreeMap<>();

        List<AggregationOperation> pipeline = new LinkedList<>();

        try {
            pipeline.add(group("parameters").push("group").as("group"));
            AggregationResults<Document> aggregate = mongoTemplate.aggregate(
                    newAggregation(pipeline),
                    Product.class,
                    Document.class);
            List<Document> mappedResults = aggregate.getMappedResults();

            for (Document mappedResult : mappedResults) {
                List<String> groups = (List<String>) mappedResult.get("group");
                mappedResult.remove("group");
                for (String group : groups) {
                    if (parametersGroups.containsKey(group)) {
                        Map<String, Set<String>> existedParameters = parametersGroups.get(group);

                        Set<String> paramsNames = mappedResult.keySet();
                        for (String paramName : paramsNames) {
                            String newParamValue = (String) mappedResult.get(paramName);
                            if (existedParameters.containsKey(paramName)) {
                                Set<String> existedParamValues = existedParameters.get(paramName);
                                existedParamValues.add(newParamValue);
                                existedParameters.replace(paramName, existedParamValues);
                            } else {
                                Set<String> newParamValues = new HashSet<>();
                                newParamValues.add(newParamValue);
                                existedParameters.put(paramName, newParamValues);
                            }
                        }
                    } else {
                        Map<String, Set<String>> newParameters = new HashMap<>();
                        Set<String> paramsNames = mappedResult.keySet();

                        for (String paramName : paramsNames) {
                            Set<String> newParamValues = new HashSet<>();
                            String newParamValue = (String) mappedResult.get(paramName);
                            newParamValues.add(newParamValue);
                            newParameters.put(paramName, newParamValues);
                        }
                        parametersGroups.put(group, newParameters);
                    }
                }
            }
            return parametersGroups;
        } catch (Exception ex) {
            System.err.println("Product aggregation exception: " + ex.getMessage());
            return new TreeMap<>();
        }
    }

    default Page<ProductBaseDTO> findAllFilter(MultiValueMap<String, List<String>> filters, String group, String name, Boolean removed, Pageable pageable, MongoTemplate mongoTemplate) {
        List<AggregationOperation> pipeline = new LinkedList<>();

        try {
            if (group != null) {
                Criteria criteria = new Criteria();
                String groupFieldName = "group";
                Criteria or1 = Criteria.where(groupFieldName).regex("^" + group + "$");
                Criteria or2 = Criteria.where(groupFieldName).regex("^" + group + "\\.");
                pipeline.add(match(criteria.orOperator(or1, or2)));
            }

            if (name != null) {
                Criteria regex = Criteria.where("name").regex(name, "i");
                pipeline.add(match(regex));
            }
            if (removed != null) {
                Criteria removedCriteria = Criteria.where("removed").is(removed);
                pipeline.add(match(removedCriteria));
            }

            if ((filters != null && !filters.isEmpty()) || group != null) {
                pipeline.add(basicFilters(filters));
            }

            pipeline.add(project("id", "name", "description", "brutto", "netto", "tax", "group", "thumbnail", "parameters", "serialNumber", "visited"));
            return AggregationsUtil.createAggregationWithPagination(
                    pipeline,
                    pageable,
                    Product.class,
                    ProductBaseDTO.class,
                    mongoTemplate);
        } catch (Exception ex) {
            System.err.println("Product aggregation exception: " + ex.getMessage());
            return new PageImpl<>(new ArrayList<>(), pageable, 0);
        }
    }

    private AggregationOperation basicFilters(MultiValueMap<String, List<String>> filters) {
        Criteria criteria = new Criteria();
        if (filters != null && !filters.isEmpty()) {
            filters.forEach((k, v) -> criteria.and(k).in(v));
        }
        return match(criteria);
    }

}
