package dao.domain;


import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class User extends ParentDAO {

    @Indexed
    @Builder.Default
    private String tempUserId = null;

    @NotNull
    @NotEmpty
    private String password;

    @Indexed
    @NotNull
    @NotEmpty
    private String email;

    @NotNull
    @NotEmpty
    private String firstName;

    @NotNull
    @NotEmpty
    private String lastName;

    @NotNull
    @NotEmpty
    private String role;

    private String emailToken;

    private boolean isVerified;

    private Address address;

    private String cartId;

    @Builder.Default
    private List<String> observedProductsIds = new ArrayList<>();

    @Builder.Default
    private List<String> ordersIds = new ArrayList<>();

    @Builder.Default
    private List<Session> sessions = new ArrayList<>();

}
