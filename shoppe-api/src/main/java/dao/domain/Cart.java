package dao.domain;


import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Cart extends ParentDAO {

    private String description;

    @Builder.Default
    private Map<String, Integer> products = new HashMap<>();

}
