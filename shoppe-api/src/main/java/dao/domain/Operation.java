package dao.domain;

import dto.operation.OperationType;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Operation extends ParentDAO {

    private String contextId;
    private String contextName;
    private String tempUserId;
    @Builder.Default
    private OperationType type = OperationType.UNKNOWN;

    private String before;
    private String after;
    @Builder.Default
    private Boolean success = Boolean.FALSE;
    private Map<String, Object> details;

    @Builder.Default
    private long time = new Date().getTime();

}
