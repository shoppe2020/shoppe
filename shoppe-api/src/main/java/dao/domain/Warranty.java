package dao.domain;

import dao.enums.WarrantyType;
import lombok.*;

@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Warranty {

    private int period;

    @Builder.Default
    private WarrantyType type = WarrantyType.NO_WARRANTY;
}
