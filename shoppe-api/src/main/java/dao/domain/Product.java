package dao.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Product extends ParentDAO {

    @Indexed
    private String group;

    @NotNull
    @NotEmpty
    private String name;

    @NotNull
    @NotEmpty
    private String description;

    @NotNull
    @NotEmpty
    private String serialNumber;

    private Integer available;

    @NotNull
    @Min(0)
    private Double netto;

    @NotNull
    @Min(0)
    private Double brutto;

    @NotNull
    @DecimalMax("1.0")
    @DecimalMin("0.0")
    private Double tax;

    private long visited;

    private Warranty warranty;

    @NotNull
    @NotEmpty
    private String thumbnail;
    private List<String> images;
    private List<String> reviewsIds;
    private Map<String, String> parameters;
}
