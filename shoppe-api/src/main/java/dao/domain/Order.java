package dao.domain;

import dao.enums.OrderStatus;
import dao.enums.Payment;
import dto.order.OrderItem;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Order extends ParentDAO {

    private String userId;
    private OrderStatus status;
    private Double totalPrice;
    private Address address;
    private Delivery delivery;
    private Payment payment;
    private Long createTime;
    private Long lastUpdateTime;

    @Builder.Default
    private List<OrderItem> orderItems = new ArrayList<>();

}
