package dao.domain;


import lombok.*;

@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Address {
    private String firstName;
    private String lastName;
    private String country;
    private String city;
    private String street;
    private String streetNo;
    private String zipCode;
    private String homeNo;
}
