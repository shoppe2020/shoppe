package dao.domain;


import lombok.*;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;

@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Session {

    @Indexed
    private String sessionId = "";
    private String accessToken = "";
    @Indexed
    private String refreshToken = "";
    private Long accessExpire;
    @Indexed
    private Long refreshExpire;
    private Long issueAt = new Date().getTime();
}
