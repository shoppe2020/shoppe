package dao.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Review extends ParentDAO {
    @NotNull
    @NotEmpty
    private String userId;
    private String description;

    @Min(1)
    @Max(5)
    private int rate = 1;

    private int good = 0;
    private int wrong = 0;

}
