package dao.enums;

import service.others.Enumable;

@Enumable
public enum WarrantyType {
    NO_WARRANTY,
    SELLER,
    LIFETIME
}
