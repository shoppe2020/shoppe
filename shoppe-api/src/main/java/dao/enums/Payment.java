package dao.enums;

import service.others.Enumable;

@Enumable
public enum Payment {
    CASH_ON_DELIVERY,
    PAYU,
    POST_TRANSFER
}
