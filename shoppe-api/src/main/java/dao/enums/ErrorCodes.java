package dao.enums;

import service.others.Enumable;

@Enumable
public enum ErrorCodes {
    CannotFindEnumException("E999", "Not found error enum"),
    UnknownException("E000", "Unknown Error"),
    EntityNotFoundException("E001", "Entity not found"),
    TokenInvalidException("E002", "Invalid token"),
    TokenExpiredException("E003", "Token has expired"),
    IllegalArgumentException("E004", "Illegal argument"),
    AuthenticationException("E005", "Authentication exception"),
    MethodArgumentNotValidException("E006", "Validation exception"),
    VerificationException("E007", "Verification exception");

    private final String errorCode;
    private final String description;

    ErrorCodes(String errorCode, String description) {
        this.errorCode = errorCode;
        this.description = description;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getDescription() {
        return description;
    }

    public static ErrorCodes safeValueOf(String enumName) {
        try {
            return ErrorCodes.valueOf(enumName);
        } catch (Exception ex) {
            return ErrorCodes.UnknownException;
        }

    }
}
