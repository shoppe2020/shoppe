package dao.enums;

import service.others.Enumable;

@Enumable
public enum OrderStatus {
    CREATED,
    IN_PROGRESS,
    PAYMENT_START,
    PAYMENT_FINISH,
    PAYMENT_CANCEL,
    PAID,
    FINISHED
}
