package app.contoller;

import dto.product.ReviewDTO;
import dto.product.ReviewResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.ReviewService;

import javax.validation.Valid;
import java.security.Principal;


@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/review", produces = MediaType.APPLICATION_JSON_VALUE)
public class ReviewController {

    private final ReviewService reviewService;

    @PostMapping("/{productId}")
    public ResponseEntity<ReviewResponseDTO> addReview(@PathVariable String productId, @Valid @RequestBody ReviewDTO reviewDTO, Principal principal) {
        return ResponseEntity.ok(reviewService.addReview(productId, reviewDTO, principal.getName()));
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<ReviewResponseDTO> removeReview(@PathVariable String productId, @Valid @RequestBody String reviewId, Principal principal) {
        return ResponseEntity.ok(reviewService.removeReview(productId, reviewId, principal.getName()));
    }

}
