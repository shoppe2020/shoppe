package app.contoller;

import dto.cart.CartResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.CartService;
import service.CommonUtils;
import service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/cart", produces = MediaType.APPLICATION_JSON_VALUE)
public class CartController {

    private final CartService cartService;

    private final UserService userService;

    @GetMapping("/self")
    public ResponseEntity<CartResponseDTO> getSelfCart(HttpServletRequest request, Principal principal) {
        String headerUserId = CommonUtils.getUserIdFromHeader(request);
        return ResponseEntity.ok(cartService.convertToDTO(userService.getActualUserCart(headerUserId, principal)));
    }

    @PostMapping("/add")
    public ResponseEntity<CartResponseDTO> addToCart(HttpServletRequest request, @RequestBody Map<String, Integer> products, Principal principal) {
        String headerUserId = CommonUtils.getUserIdFromHeader(request);
        return ResponseEntity.ok(cartService.addToCart(userService.getActualUserCart(headerUserId, principal), products));
    }

    @PostMapping("/remove")
    public ResponseEntity<CartResponseDTO> removeFromCart(HttpServletRequest request, @RequestBody Map<String, Integer> products, Principal principal) {
        String headerUserId = CommonUtils.getUserIdFromHeader(request);
        return ResponseEntity.ok(cartService.removeFromCart(userService.getActualUserCart(headerUserId, principal), products));
    }
}
