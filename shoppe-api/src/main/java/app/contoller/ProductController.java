package app.contoller;

import dao.domain.Product;
import dto.product.ProductBaseDTO;
import dto.product.ProductDTO;
import dto.product.ProductRequestDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import service.ProductService;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Set;


@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/product", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

    private final ProductService productService;

    @GetMapping("/parameters")
    public ResponseEntity<Map<String, Map<String, Set<String>>>> getProductsParameters() {
        return ResponseEntity.ok(productService.getProductsParameters());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDTO> getProductById(@PathVariable String id) {
        return ResponseEntity.ok(productService.findProductById(id));
    }

    @GetMapping
    public ResponseEntity<Page<ProductBaseDTO>> getProducts(
            @RequestParam(required = false) String group,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) Boolean removed,
            @RequestParam(required = false) MultiValueMap<String, List<String>> filters,
            @PageableDefault(page = 0, size = Integer.MAX_VALUE) Pageable pageable) {
        return ResponseEntity.ok(productService.findAllFilter(filters, group, name, removed, pageable));
    }

    @PostMapping
    public ResponseEntity<Product> createProduct(@Valid @RequestBody ProductRequestDTO request) {
        return ResponseEntity.ok(productService.create(request));
    }

    @PutMapping
    public ResponseEntity<Product> updateProduct(@Valid @RequestBody ProductRequestDTO request) {
        return ResponseEntity.ok(productService.update(request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Product> deleteProduct(@PathVariable String id) {
        return ResponseEntity.ok(productService.deleteById(id));
    }

}
