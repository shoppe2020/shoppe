package app.contoller;

import com.querydsl.core.types.Predicate;
import dao.domain.Order;
import dto.order.OrderChangeStatusDTO;
import dto.order.OrderDTO;
import dto.order.OrderRequestDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.CommonUtils;
import service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping(value = "/api/v1/order", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @GetMapping("/list")
    public ResponseEntity<Page<OrderDTO>> getOrders(@QuerydslPredicate(root = Order.class) Predicate predicate,
                                                    @PageableDefault(page = 0, size = Integer.MAX_VALUE) Pageable pageable) {

        Page<Order> allOrders = orderService.findAll(predicate, pageable);
        Page<OrderDTO> map = allOrders.map(orderService::convertToDTO);
        return ResponseEntity.ok(map);
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderDTO> getOrderById(@PathVariable String id) {
        return ResponseEntity.ok(orderService.convertToDTO(orderService.findById(id)));
    }

    @GetMapping("/self")
    public ResponseEntity<Page<OrderDTO>> getSelf(HttpServletRequest request, Principal principal, @PageableDefault(page = 0, size = Integer.MAX_VALUE) Pageable pageable) {
        String headerUserId = CommonUtils.getUserIdFromHeader(request);
        return ResponseEntity.ok(orderService.findOrdersFromPrincipal(headerUserId, principal, pageable));
    }

    @PostMapping("/cart")
    public ResponseEntity<OrderDTO> createOrderFromCart(HttpServletRequest request, @Valid @RequestBody OrderRequestDTO requestDTO, Principal principal) {
        String headerUserId = CommonUtils.getUserIdFromHeader(request);
        return ResponseEntity.ok(orderService.createOrderFromCart(headerUserId, principal, requestDTO));
    }

    @PutMapping("/status")
    public ResponseEntity<OrderDTO> changeOrderStatus(@RequestBody OrderChangeStatusDTO request, Principal principal) {
        return ResponseEntity.ok(orderService.changeOrderStatus(request, principal));
    }
}
