package app.contoller;

import com.querydsl.core.types.Predicate;
import dao.domain.User;
import dto.user.UserChangePasswordRequestDTO;
import dto.user.UserDTO;
import dto.user.UserRequestDTO;
import dto.user.UserUpdateRequestDTO;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.CommonUtils;
import service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final UserService userService;

    @GetMapping(value = "/list")
    public ResponseEntity<Page<UserDTO>> getUsers(@QuerydslPredicate(root = User.class) Predicate predicate,
                                                  @PageableDefault(page = 0, size = Integer.MAX_VALUE) Pageable pageable) {
        return ResponseEntity.ok(userService.findAll(predicate, pageable).map(userService::convertToDTO));
    }

    @GetMapping(value = "/self")
    public ResponseEntity<UserDTO> getSelf(HttpServletRequest request, Principal principal) {
        String headerUserId = CommonUtils.getUserIdFromHeader(request);
        return ResponseEntity.ok(userService.getSelf(headerUserId, principal));
    }

    @PostMapping
    public ResponseEntity<Document> generateUser() {
        return ResponseEntity.ok(userService.generateUser());
    }

    @PostMapping("/register")
    public ResponseEntity<UserDTO> registerUser(@Valid @RequestBody UserRequestDTO requestDTO) {
        return ResponseEntity.ok(userService.registerUser(requestDTO));
    }

    @PutMapping
    public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UserUpdateRequestDTO requestDTO, Principal principal, HttpServletRequest request) {
        return ResponseEntity.ok(userService.updateUser(requestDTO, principal));
    }

    @PutMapping("/confirm")
    public ResponseEntity<UserDTO> confirmUser(@RequestParam(required = false) String email,
                                               @RequestParam(required = false) String emailToken) {
        return ResponseEntity.ok(userService.confirmUser(email, emailToken));
    }

    @PutMapping("/password")
    public ResponseEntity<UserDTO> changePassword(@RequestBody UserChangePasswordRequestDTO requestDTO, Principal principal) {
        return ResponseEntity.ok(userService.changePassword(requestDTO, principal));
    }

    @GetMapping("/reset")
    public ResponseEntity<Boolean> resetPassword(@RequestParam String email) {
        return ResponseEntity.ok(userService.resetPassword(email));
    }
}
