package app.contoller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.EmailService;
import service.EnumService;
import service.GroupsGenerator;
import service.others.Node;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/home", produces = MediaType.APPLICATION_JSON_VALUE)
public class HomeController {

    private final EnumService enumService;

    private final EmailService emailService;

    @GetMapping("/enum")
    public ResponseEntity<Map<String, List<String>>> getEnums() {
        return ResponseEntity.ok(enumService.getAllEnums());
    }

    @GetMapping("/root")
    public ResponseEntity<Node> getRoot() {
        return ResponseEntity.ok(GroupsGenerator.getRoot());
    }

    @PostMapping("/email")
    public ResponseEntity<Boolean> sendTestEmail(@RequestParam(defaultValue = "piotr.k.pawlowski@student.put.poznan.pl") String to,
                                                 @RequestParam String emailToken) {
        emailService.sendEmailToken(to, emailToken);
        return ResponseEntity.ok(true);
    }
}
