package app.config;


import com.mongodb.*;
import dao.enums.ErrorCodes;
import dto.ExceptionResponseDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConverterNotFoundException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.UncategorizedMongoDbException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import service.exception.*;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.regex.Pattern;

@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus httpStatus,
                                                                  WebRequest request) {

        Object exceptionBody = createExceptionBody(ex.getMessage(), ex.getClass().getSimpleName(), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(exceptionBody, new HttpHeaders(), httpStatus);
    }


    @ExceptionHandler(value = {UncategorizedMongoDbException.class, MongoSocketWriteException.class, MongoSocketReadException.class,
            DataAccessResourceFailureException.class, MongoTimeoutException.class, MongoSocketOpenException.class,
            MongoException.class, ConverterNotFoundException.class})
    protected ResponseEntity<ExceptionResponseDTO> handleMongoExceptions(RuntimeException ex, WebRequest request) {
        return createExceptionResponse(
                ex.getStackTrace(),
                ex.getMessage(),
                ex.getClass().getSimpleName(),
                HttpStatus.REQUEST_TIMEOUT);
    }

    @ExceptionHandler(value = {AuthenticationCredentialsNotFoundException.class, NoSuchElementException.class, EntityNotFoundException.class})
    protected ResponseEntity<ExceptionResponseDTO> handleNotFound(RuntimeException ex, WebRequest request) {
        return createExceptionResponse(
                ex.getStackTrace(),
                ex.getMessage(),
                ex.getClass().getSimpleName(),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {IllegalArgumentException.class, OperationException.class, VerificationException.class})
    protected ResponseEntity<ExceptionResponseDTO> badRequest(RuntimeException ex, WebRequest request) {
        return createExceptionResponse(
                ex.getStackTrace(),
                ex.getMessage(),
                ex.getClass().getSimpleName(),
                HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(value = {AuthenticationException.class})
    protected ResponseEntity<ExceptionResponseDTO> accessDenied(RuntimeException ex, WebRequest request) {
        return createExceptionResponse(
                ex.getStackTrace(),
                ex.getMessage(),
                ex.getClass().getSimpleName(),
                HttpStatus.FORBIDDEN);

    }

    @ExceptionHandler(value = {TokenInvalidException.class, TokenExpiredException.class})
    protected ResponseEntity<ExceptionResponseDTO> tokenException(RuntimeException ex, WebRequest request) {
        return createExceptionResponse(
                ex.getStackTrace(),
                ex.getMessage(),
                ex.getClass().getSimpleName(),
                HttpStatus.NOT_ACCEPTABLE);
    }

    private ExceptionResponseDTO createExceptionBody(String exMessage, String simpleName, HttpStatus httpStatus) {
        ErrorCodes errorCode = ErrorCodes.safeValueOf(simpleName);
        return ExceptionResponseDTO.builder()
                .isTokenExpired(false)
                .status(httpStatus.value())
                .description(errorCode.getDescription())
                .message(exMessage)
                .errorCode(errorCode.getErrorCode())
                .build();
    }

    private ResponseEntity<ExceptionResponseDTO> createExceptionResponse(StackTraceElement[] stackTrace, String exMessage, String simpleName, HttpStatus httpStatus) {
        log.error(exMessage + "\n" + Arrays.toString(stackTrace).replaceAll(Pattern.quote(","), "\n"));
        ExceptionResponseDTO exceptionBody = createExceptionBody(exMessage, simpleName, httpStatus);
        return new ResponseEntity<>(exceptionBody, new HttpHeaders(), httpStatus);
    }
}