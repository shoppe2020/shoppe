package app.config.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import service.CommonUtils;
import service.exception.TokenExpiredException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class TokenFilter extends OncePerRequestFilter {

    @Autowired
    private AuthUserDetailsService authUserDetailsService;

    @Autowired
    private TokenService tokenService;

    private final String refreshTokenURI = "/api/v1/token";
    private final String refreshTokenMethod = "PUT";

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {

        String token = CommonUtils.getAccessTokenFromHeader(req);
        if (token != null) {
            if (tokenService.isOnBlacklist(token)) {
                SecurityContextHolder.getContext().setAuthentication(null);
                throw new TokenExpiredException(token);
            } else {
                String login = tokenService.extractLogin(token);
                if (login != null) {
                    authenticate(token, login, req);
                } else {
                    String requestURI = req.getRequestURI();
                    String method = req.getMethod();
                    if (requestURI.equals(refreshTokenURI) && method.equals(refreshTokenMethod)) {
                        log.info("Refresh token request");
                    } else {
                        throw new TokenExpiredException(token);
                    }
                }
            }
        }
        chain.doFilter(req, res);
    }

    private void authenticate(String token, String login, HttpServletRequest req) {
        if (login != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = this.authUserDetailsService.loadUserByUsername(login);
            tokenService.validateToken(token, login);
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(userDetails,
                            null,
                            userDetails.getAuthorities());
            usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

        }
    }

}