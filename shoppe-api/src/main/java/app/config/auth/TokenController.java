package app.config.auth;

import io.jsonwebtoken.lang.Assert;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.CommonUtils;
import service.exception.AuthenticationException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/token", produces = MediaType.APPLICATION_JSON_VALUE)
public class TokenController {

    private final TokenService tokenService;

    @PostMapping
    public ResponseEntity<TokenDetails> login(@Valid @RequestBody TokenRequest tokenRequest, HttpServletRequest request) {
        String email = tokenRequest.getEmail();
        String password = tokenRequest.getPassword();
        String tempUserId = CommonUtils.getUserIdFromHeader(request);
        TokenDetails tokenDetails = tokenService.login(email, password, tempUserId);
        return ResponseEntity.ok(tokenDetails);
    }

    @PutMapping
    public ResponseEntity<TokenDetails> refreshToken(HttpServletRequest request, Principal principal, @RequestBody String refreshToken) {
        String accessToken = CommonUtils.getAccessTokenFromHeader(request);
        String sessionId = CommonUtils.getSessionIdFromHeader(request);
        return ResponseEntity.ok(tokenService.refreshToken(principal, sessionId, accessToken, refreshToken));
    }

    @DeleteMapping
    public ResponseEntity<Boolean> logout(HttpServletRequest request, Principal principal) {
        if (principal == null) {
            throw new AuthenticationException("You are not logged in");
        }

        String sessionId = CommonUtils.getSessionIdFromHeader(request);
        String accessToken = CommonUtils.getAccessTokenFromHeader(request);
        Assert.notNull(sessionId, "SessionId is null");
        Assert.notNull(accessToken, "AccessToken is null");

        tokenService.logout(principal.getName(), sessionId, accessToken);
        return ResponseEntity.ok(true);
    }
}
