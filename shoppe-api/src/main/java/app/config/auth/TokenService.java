package app.config.auth;

import dao.domain.Session;
import dao.domain.User;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import service.UserService;
import service.exception.AuthenticationException;
import service.exception.TokenExpiredException;

import java.security.Principal;
import java.util.*;
import java.util.function.Function;

@Service
@Slf4j
public class TokenService {

    @Autowired
    private UserService userService;
    @Autowired
    private AuthenticationManager authenticationManager;

    private String SECRET_KEY = "access-secret";

    private int ACCESS_EXPIRATION_TIME = 1000 * 60 * 15;//15min
    private int REFRESH_EXPIRATION_TIME = 1000 * 60 * 30;//30min

    private Set<String> accessTokenBlacklist = new HashSet<>();

    public boolean isOnBlacklist(String token) {
        if (token == null) return false;
        return accessTokenBlacklist.contains(token);
    }

    public boolean addOnBlacklist(String token) {
        return accessTokenBlacklist.add(token);
    }

    public boolean removeFromBlacklist(String token) {
        return accessTokenBlacklist.remove(token);
    }

    public String extractLogin(String token) {
        try {
            return extractClaim(token, Claims::getSubject);
        } catch (ExpiredJwtException | UnsupportedJwtException eje) {
            return null;
        }
    }

    public Date extractExpiration(String token) {
        try {
            return extractClaim(token, Claims::getExpiration);
        } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException ex) {
            return null;
        }
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    public Boolean isTokenExpired(String token) {
        Date expirationDate = extractExpiration(token);
        if (expirationDate == null) return true;
        if (expirationDate.before(new Date())) return true;

        return false;
    }

    public TokenDetails createToken(String login) {
        Date accessExpireDate = new Date(System.currentTimeMillis() + ACCESS_EXPIRATION_TIME);
        Date refreshExpireDate = new Date(System.currentTimeMillis() + REFRESH_EXPIRATION_TIME);
        String refreshToken = new ObjectId().toString();
        String sessionId = new ObjectId().toString();

        String accessToken = Jwts.builder()
                .setClaims(new HashMap<>())
                .setSubject(login)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(accessExpireDate)
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();

        User user = userService.findByEmail(login);
        Session session = Session.builder()
                .sessionId(sessionId)
                .accessToken(accessToken)
                .accessExpire(accessExpireDate.getTime())
                .refreshToken(refreshToken)
                .refreshExpire(refreshExpireDate.getTime())
                .build();

        user.getSessions().add(session);
        userService.save(user);

        return TokenDetails.builder()
                .sessionId(sessionId)
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .accessExpire(accessExpireDate.getTime())
                .refreshExpire(refreshExpireDate.getTime())
                .build();
    }

    public void validateToken(String accessToken, String login) {
        final String extractedLogin = extractLogin(accessToken);
        boolean loginEquals = extractedLogin.equals(login);
        Boolean tokenExpired = isTokenExpired(accessToken);
        if (!loginEquals || tokenExpired) {
            throw new TokenExpiredException(accessToken);
        } else {
            log.debug("TOKEN VALIDATION SUCCESS");
        }
    }

    private Session getSessionById(List<Session> sessions, String sessionId) {
        return sessions.stream()
                .filter(s -> s.getSessionId().equals(sessionId))
                .findFirst()
                .orElseThrow(() -> new AuthenticationException("Session does not exist"));
    }

    public TokenDetails refreshToken(Principal principal, String sessionId, String accessToken, String refreshToken) {
        User user;
        if (principal != null) {
            user = userService.findByEmail(principal.getName());
        } else {
            user = userService.findByRefreshToken(refreshToken);
        }
        List<Session> sessions = user.getSessions();
        Session session = getSessionById(sessions, sessionId);
        if (session != null) {
            String dbAccessToken = session.getAccessToken();
            String dbRefreshToken = session.getRefreshToken();
            Long refreshExpireTime = session.getRefreshExpire();

            if (!dbAccessToken.equals(accessToken) || !dbRefreshToken.equals(refreshToken)) {
                throw new AuthenticationException("Tokens are not equal");
            }

            if (refreshExpireTime < new Date().getTime()) {
                throw new AuthenticationException("Refresh token has expired");
            }

            if (!isTokenExpired(accessToken) && !isOnBlacklist(accessToken)) {
                addOnBlacklist(accessToken);
            }

            sessions.remove(session);
            userService.save(user);
            return createToken(user.getEmail());
        } else {
            throw new AuthenticationException("Session doesnt exists");
        }
    }

    public void logout(String email, String sessionId, String accessToken) {
        User user = userService.findByEmail(email);
        List<Session> sessions = user.getSessions();
        Session session = getSessionById(sessions, sessionId);
        if (session != null) {
            String dbAccessToken = session.getAccessToken();
            if (!accessToken.equals(dbAccessToken)) {
                throw new AuthenticationException("Tokens are not equal");
            }
            if (!isTokenExpired(accessToken) && !isOnBlacklist(accessToken)) {
                addOnBlacklist(accessToken);
            }
            sessions.remove(session);
            userService.save(user);
            SecurityContextHolder.getContext().setAuthentication(null);
        }
    }

    public void logoutAll(String email) {
        User user = userService.findByEmail(email);
        user.setSessions(new ArrayList<>());
        userService.save(user);
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    public TokenDetails login(String email, String password, String tempUserId) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException ex) {
            throw new AuthenticationException("Incorrect username or password" + ex.getMessage());
        } catch (LockedException ex) {
            throw new AuthenticationException("User is locked" + ex.getMessage());
        } catch (Exception ex) {
            throw new AuthenticationException("Authenticate Exception: " + ex.getMessage());
        }
        final TokenDetails tokenDetails = createToken(email);

        if (tempUserId != null) {
            userService.mergeUsers(email, tempUserId);
        }
        return tokenDetails;

    }

    public void setAccessTokenExpirationTime(int accessTokenExpirationTime) {
        this.ACCESS_EXPIRATION_TIME = accessTokenExpirationTime;
    }

    public void setRefreshTokenExpirationTime(int refreshTokenExpirationTime) {
        this.REFRESH_EXPIRATION_TIME = refreshTokenExpirationTime;
    }

    public Set<String> getAccessTokenBlacklist() {
        return accessTokenBlacklist;
    }
}
