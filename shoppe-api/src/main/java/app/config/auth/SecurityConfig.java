package app.config.auth;

import app.config.CustomAuthenticationEntryPoint;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@EnableWebSecurity
@RequiredArgsConstructor
@SuppressWarnings("unchecked")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final String SALT = "salt";

    private final AuthUserDetailsService authUserDetailsService;

    private final TokenFilter tokenFilter;

    private final String appPrefix = "/api/v1";

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers(appPrefix + "/token").permitAll()
                .antMatchers(HttpMethod.PUT, appPrefix + "/token").hasAnyAuthority("USER", "ADMIN")
                .antMatchers(HttpMethod.DELETE, appPrefix + "/token").hasAnyAuthority("USER", "ADMIN")

                //USER
                .antMatchers(HttpMethod.POST, appPrefix + "/user").permitAll()
                .antMatchers(HttpMethod.GET, appPrefix + "/user/self").permitAll()
                .antMatchers(HttpMethod.POST, appPrefix + "/user/register").permitAll()
                .antMatchers(HttpMethod.PUT, appPrefix + "/user/confirm").permitAll()
                .antMatchers(HttpMethod.GET, appPrefix + "/user/reset").permitAll()
                .antMatchers(HttpMethod.PUT, appPrefix + "/user/password").hasAnyAuthority("ADMIN", "USER")
                .antMatchers(HttpMethod.GET, appPrefix + "/user/list").hasAuthority("ADMIN")
                //-------------

                //CART
                .antMatchers(appPrefix + "/cart/*").permitAll()
                //-------------

                //PRODUCT
                .antMatchers(HttpMethod.GET, appPrefix + "/product/{id}").permitAll()
                .antMatchers(HttpMethod.GET, appPrefix + "/product").permitAll()
                .antMatchers(HttpMethod.GET, appPrefix + "/product/parameters").permitAll()
                .antMatchers(HttpMethod.PUT, appPrefix + "/product").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.POST, appPrefix + "/product").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE, appPrefix + "/product/{id}").hasAuthority("ADMIN")
                //-------------

                //REVIEW
                .antMatchers(appPrefix + "/review/*").hasAnyAuthority("USER", "ADMIN")
                //-------------

                //ORDER
                .antMatchers(appPrefix + "/order/*").permitAll()
                .antMatchers(appPrefix + "/order/list").hasAuthority("ADMIN")
                .antMatchers(appPrefix + "/order/{id}}").hasAuthority("ADMIN")
                //-------------

                //DELIVERY
                .antMatchers(appPrefix + "/delivery").permitAll()
                //-------------

                .antMatchers(appPrefix + "/home/*").permitAll()
                .antMatchers("/swagger-ui.html").permitAll()
                .anyRequest().authenticated()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint());

        http.addFilterBefore(tokenFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return new CustomAuthenticationEntryPoint();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Collections.singletonList("https://localhost:4200"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
        configuration.setExposedHeaders(Arrays.asList("Authorization", "content-type", "userId", "sessionId"));
        configuration.setAllowedHeaders(Arrays.asList("Authorization", "content-type", "userId", "sessionId"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        Map encoders = new HashMap<>();
        encoders.put("bcrypt", new BCryptPasswordEncoder(12, new SecureRandom(SALT.getBytes())));
        DelegatingPasswordEncoder delegatingPasswordEncoder = new DelegatingPasswordEncoder("bcrypt", encoders);
        delegatingPasswordEncoder.setDefaultPasswordEncoderForMatches(new BCryptPasswordEncoder());
        PasswordEncoder passwordEncoder = delegatingPasswordEncoder;
        return passwordEncoder;
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(authUserDetailsService);
    }


    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
