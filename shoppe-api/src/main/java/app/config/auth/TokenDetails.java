package app.config.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TokenDetails {

    private String sessionId = "";

    private String accessToken = "";

    private String refreshToken = "";

    private Long accessExpire;

    private Long refreshExpire;
    @Builder.Default
    private Long issueAt = new Date().getTime();
}
