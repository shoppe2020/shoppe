package app.config;

import app.config.auth.TokenService;
import com.fasterxml.jackson.databind.ObjectMapper;
import dao.enums.ErrorCodes;
import dto.ExceptionResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import service.CommonUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RequiredArgsConstructor
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Autowired
    private TokenService tokenService;

    @Override
    public void commence(HttpServletRequest req, HttpServletResponse res, AuthenticationException authException) throws IOException {

        ExceptionResponseDTO responseEntity = ExceptionResponseDTO.builder()
                .status(403)
                .errorCode(ErrorCodes.AuthenticationException.getErrorCode())
                .isTokenExpired(isTokenExpired(req))
                .message("Access denied")
                .description(ErrorCodes.AuthenticationException.getDescription())
                .build();

        String message = new ObjectMapper().writeValueAsString(responseEntity);
        res.getWriter().write(message);
        res.setContentType("application/json");
        res.setStatus(403);
    }

    private boolean isTokenExpired(HttpServletRequest req) {
        String token = CommonUtils.getAccessTokenFromHeader(req);
        if (token != null) {
            if (tokenService.isTokenExpired(token) || tokenService.isOnBlacklist(token)) {
                return true;
            }
        }
        return false;
    }
}