package app.config;

import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableMongoRepositories(basePackages = {"dao"})
@Slf4j
@Profile("!test")
public class MongoConfig {

    @Value("${mongo.local.address:localhost}")
    private String databaseAddress;
    @Value("${mongo.local.port:27017}")
    private int databasePort;
    @Value("${mongo.local.database:shoppyDb}")
    private String databaseName;

//    private static final String USER = "admin";
//    private static final String PASSWORD = "secret";
//    private static final String DATABASE = "admin";

    @Bean
    public MongoClient mongoClient() {
//        MongoCredential credential = MongoCredential.createCredential(USER, DATABASE, PASSWORD.toCharArray());
//        MongoClientOptions mongoClientOptions = MongoClientOptions.builder()
//                .heartbeatConnectTimeout(10000)
//                .heartbeatSocketTimeout(10000)
//                .serverSelectionTimeout(10000)
//                .connectTimeout(10000)
//                .socketTimeout(10000)
//                .build();

        log.info(
                "\n----------------------------------------------------------\n\t"
                        + "Local mongo config:\n\t"
//                        + "User: \t\t\t{}\n\t"
//                        + "Password: \t\t{}\n\t"
                        + "Database name: \t\t{}\n\t"
                        + "Database address: \t{}\n\t"
                        + "Database port: \t\t{}\n----------------------------------------------------------",
//                USER,
//                PASSWORD,
                databaseName,
                databaseAddress,
                databasePort
        );

        return MongoClients.create(
                MongoClientSettings.builder()
                        .applyToSocketSettings(builder ->
                                builder.connectTimeout(10000, TimeUnit.MILLISECONDS))
                        .applyToClusterSettings(builder ->
                                builder.hosts(Arrays.asList(new ServerAddress(databaseAddress, databasePort))))
                        .build());
    }

    @Bean
    public MongoTemplate mongoTemplate() throws DataAccessResourceFailureException {
        MongoClient mongoClient = mongoClient();
        MongoTemplate mongoTemplate
                = new MongoTemplate(mongoClient, databaseName);
        return mongoTemplate;
    }

}
