package app;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
@EnableScheduling
public class ShoppeApp {

    public static void main(String[] args) {
        SpringApplication.run(ShoppeApp.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            System.out.println("\n\nSpring Boot registered beans :");
            String[] beanNames = ctx.getBeanDefinitionNames();
            List<String> filteredNames = new ArrayList<>();
            for (String beanName : beanNames) {
                if (beanName.toLowerCase().endsWith("controller")
                        || beanName.toLowerCase().endsWith("service")
                        || beanName.toLowerCase().endsWith("repository")) {
                    filteredNames.add(beanName);
                }
            }
            filteredNames.remove("basicErrorController");
            filteredNames.remove("mvcConversionService");
            filteredNames.remove("apiResourceController");

            System.out.println("\nControllers: ");
            filteredNames.stream().filter(b -> b.toLowerCase().endsWith("controller")).forEach(b -> System.out.println("- " + b));
            System.out.println("\nServices: ");
            filteredNames.stream().filter(b -> b.toLowerCase().endsWith("service")).forEach(b -> System.out.println("- " + b));
            System.out.println("\nRepositories: ");
            filteredNames.stream().filter(b -> b.toLowerCase().endsWith("repository")).forEach(b -> System.out.println("- " + b));
            System.out.println("\n\nSpring Boot application started successfully " + new Date().toString());
        };
    }
}
