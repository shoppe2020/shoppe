package app;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import service.GroupsGenerator;
import service.SetupService;

import java.util.Date;

@Slf4j
@Component
@RequiredArgsConstructor
public class OnStart implements ApplicationRunner {

    private final SetupService setupService;

    @Override
    public void run(ApplicationArguments args) {
        log.info("On start: " + new Date().toString());
        GroupsGenerator.getRoot();
        String userId = setupService.createDefaultUsers();
        setupService.createDefaultProducts(userId);
        setupService.createDefaultDeliveries();
        setupService.clearUsersToken();//temp for tests
    }
}
