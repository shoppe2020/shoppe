package app.aop;

import app.config.auth.TokenDetails;
import app.config.auth.TokenRequest;
import dao.domain.Operation;
import dao.domain.User;
import dao.repo.OperationRepository;
import dto.operation.OperationType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import service.CommonUtils;
import service.UserService;
import service.exception.AOPException;
import service.exception.EntityNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@EnableAspectJAutoProxy
@Aspect
@Component
@RequiredArgsConstructor
@SuppressWarnings("unchecked")
@Slf4j
public class TokenAspect {

    private final OperationRepository operationRepository;
    private final UserService userService;

    @Around("execution(* app.config.auth.TokenController.login(..)) && args(tokenRequest, request,..)")
    public ResponseEntity<TokenDetails> login(ProceedingJoinPoint joinPoint, TokenRequest tokenRequest, HttpServletRequest request) throws Throwable {

        ResponseEntity<TokenDetails> response;
        try {
            response = (ResponseEntity<TokenDetails>) joinPoint.proceed();
        } catch (Exception ex) {
            saveFailLoginOperation(ex, tokenRequest, request);
            log.error(ex.getMessage());
            throw ex;
        }

        try {
            TokenDetails tokenDetails = response.getBody();
            User user = userService.findByEmail(tokenRequest.getEmail());
            Map<String, Object> details = new HashMap<>();
            details.put("tokenRequest", tokenRequest);
            details.put("tokenDetails", tokenDetails);
            Operation operation = Operation.builder()
                    .contextId(user.getId())
                    .contextName(tokenRequest.getEmail())
                    .tempUserId(CommonUtils.getUserIdFromHeader(request))
                    .type(OperationType.LOGIN)
                    .success(true)
                    .details(details)
                    .build();

            operationRepository.save(operation);
            return response;
        } catch (EntityNotFoundException ex) {
            saveFailLoginOperation(ex, tokenRequest, request);
            log.error(ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            saveFailLoginOperation(ex, tokenRequest, request);
            log.error(ex.getMessage());
            throw new AOPException("AOP exception - login");
        }
    }

    @Around("execution(* app.config.auth.TokenController.logout(..)) && args(request, principal,..)")
    public ResponseEntity<Boolean> logout(ProceedingJoinPoint joinPoint, HttpServletRequest request, Principal principal) throws Throwable {

        ResponseEntity<Boolean> response;
        try {
            response = (ResponseEntity<Boolean>) joinPoint.proceed();
        } catch (Exception ex) {
            saveFailLogoutOperation(ex, request, principal);
            log.error(ex.getMessage());
            throw ex;
        }

        try {
            User user = userService.findByEmail(principal.getName());
            Map<String, Object> details = new HashMap<>();
            details.put("principal", principal);
            details.put("sessionId", CommonUtils.getSessionIdFromHeader(request));
            Operation operation = Operation.builder()
                    .contextId(user.getId())
                    .contextName(principal.getName())
                    .tempUserId(CommonUtils.getUserIdFromHeader(request))
                    .type(OperationType.LOGOUT)
                    .success(true)
                    .details(details)
                    .build();


            operationRepository.save(operation);
            return response;
        } catch (EntityNotFoundException ex) {
            saveFailLogoutOperation(ex, request, principal);
            log.error(ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            saveFailLogoutOperation(ex, request, principal);
            log.error(ex.getMessage());
            throw new AOPException("AOP exception - login");
        }
    }

    @Around("execution(* app.config.auth.TokenController.refreshToken(..)) && args(request, principal, refreshToken)")
    public ResponseEntity<TokenDetails> refresh(ProceedingJoinPoint joinPoint, HttpServletRequest request, Principal principal, String refreshToken) throws Throwable {
        User user;
        if (principal != null) {
            user = userService.findByEmail(principal.getName());
        } else {
            user = userService.findByRefreshToken(refreshToken);
        }
        ResponseEntity<TokenDetails> response;
        try {
            response = (ResponseEntity<TokenDetails>) joinPoint.proceed();
        } catch (Exception ex) {
            saveFailRefreshOperation(ex, request, principal, refreshToken);
            log.error(ex.getMessage());
            throw ex;
        }

        try {
            TokenDetails tokenDetails = response.getBody();

            Map<String, Object> details = new HashMap<>();
            details.put("principal", principal);
            details.put("tokenDetails", tokenDetails);
            details.put("refreshToken", refreshToken);
            details.put("sessionId", CommonUtils.getSessionIdFromHeader(request));
            details.put("accessToken", CommonUtils.getAccessTokenFromHeader(request));
            Operation operation = Operation.builder()
                    .contextId(user.getId())
                    .contextName(user.getEmail())
                    .tempUserId(CommonUtils.getUserIdFromHeader(request))
                    .type(OperationType.REFRESH)
                    .success(true)
                    .details(details)
                    .build();
            operationRepository.save(operation);
            return response;
        } catch (EntityNotFoundException ex) {
            saveFailRefreshOperation(ex, request, principal, refreshToken);
            log.error(ex.getMessage());
            throw ex;
        } catch (Exception ex) {
            saveFailRefreshOperation(ex, request, principal, refreshToken);
            log.error(ex.getMessage());
            throw new AOPException("AOP exception - login");
        }
    }

    private void saveFailLoginOperation(Exception ex, TokenRequest tokenRequest, HttpServletRequest request) {
        Map<String, Object> exDetails = new HashMap<>();
        exDetails.put("tokenRequest", tokenRequest);
        exDetails.put("accessToken", CommonUtils.getAccessTokenFromHeader(request));
        exDetails.put("exception", ex.getMessage());
        Operation operation = Operation.builder()
                .contextName(tokenRequest.getEmail())
                .tempUserId(CommonUtils.getUserIdFromHeader(request))
                .type(OperationType.LOGIN)
                .success(false)
                .details(exDetails)
                .build();
        operationRepository.save(operation);
    }

    private void saveFailLogoutOperation(Exception ex, HttpServletRequest request, Principal principal) {
        Map<String, Object> exDetails = new HashMap<>();
        exDetails.put("principal", principal);
        exDetails.put("accessToken", CommonUtils.getAccessTokenFromHeader(request));
        exDetails.put("exception", ex.getMessage());
        Operation operation = Operation.builder()
                .contextName(principal.getName())
                .tempUserId(CommonUtils.getUserIdFromHeader(request))
                .type(OperationType.LOGOUT)
                .success(false)
                .details(exDetails)
                .build();
        operationRepository.save(operation);
    }

    private void saveFailRefreshOperation(Exception ex, HttpServletRequest request, Principal principal, String refreshToken) {
        Map<String, Object> exDetails = new HashMap<>();
        exDetails.put("accessToken", CommonUtils.getAccessTokenFromHeader(request));
        exDetails.put("refreshToken", refreshToken);
        exDetails.put("exception", ex.getMessage());
        Operation operation = Operation.builder()
                .tempUserId(CommonUtils.getUserIdFromHeader(request))
                .type(OperationType.REFRESH)
                .success(false)
                .details(exDetails)
                .build();

        if (principal != null) {
            exDetails.put("principal", principal);
            operation.setContextName(principal == null ? null : principal.getName());
        }
        operationRepository.save(operation);
    }

}
