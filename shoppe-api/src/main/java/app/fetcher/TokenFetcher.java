package app.fetcher;


import app.config.auth.TokenService;
import com.mongodb.client.result.UpdateResult;
import dao.domain.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Component
@RequiredArgsConstructor
public class TokenFetcher {

    private final TokenService tokenService;

    private final MongoTemplate mongoTemplate;

    @Scheduled(fixedRate = 60 * 1000)//60s
    private void run() {
        removeExpiredTokensFromUsers();
        removeExpiredTokensFromBlacklist();
    }

    private void removeExpiredTokensFromUsers() {
        long actualTime = new Date().getTime();
        Query filter = Query.query(Criteria.where("sessions.refreshExpire").lte(actualTime));

        UpdateResult updateResult = mongoTemplate.updateMulti(filter,
                new Update().pull("sessions", Query.query(Criteria.where("refreshExpire").lte(actualTime))), User.class);

        long matchedCount = updateResult.getMatchedCount();
        long modifiedCount = updateResult.getModifiedCount();
        if (matchedCount > 0 && modifiedCount > 0) {
            log.debug("matchedCount: " + matchedCount + " modifiedCount: " + modifiedCount);
        }
    }

    private void removeExpiredTokensFromBlacklist() {
        Set<String> accessTokenBlacklist = tokenService.getAccessTokenBlacklist();
        if (accessTokenBlacklist.isEmpty()) return;
        AtomicInteger expired = new AtomicInteger();

        accessTokenBlacklist.forEach((accessToken) -> {
            if (tokenService.isTokenExpired(accessToken)) {
                tokenService.removeFromBlacklist(accessToken);
                expired.incrementAndGet();
            }
        });

        if (expired.get() > 0) {
            log.debug("Expired: " + expired.get());
        }
    }

}
