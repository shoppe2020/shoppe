package service.test;

import org.springframework.http.HttpHeaders;

public class HeadersUtils {

    public static HttpHeaders appendAccessTokenHeader(HttpHeaders headers, String accessToken) {
        if (headers == null) {
            headers = new HttpHeaders();
        }

        headers.add("Authorization", "Bearer " + accessToken);
        return headers;
    }

    public static HttpHeaders appendUserIdHeader(HttpHeaders headers, String userId) {
        if (headers == null) {
            headers = new HttpHeaders();
        }

        headers.add("userId", userId);
        return headers;
    }

    public static HttpHeaders appendSessionIdHeader(HttpHeaders headers, String sessionId) {
        if (headers == null) {
            headers = new HttpHeaders();
        }

        headers.add("sessionId", sessionId);
        return headers;
    }
}
