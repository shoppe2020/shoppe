package service.test;

import app.config.auth.TokenDetails;
import app.config.auth.TokenRequest;
import dao.domain.Product;
import dto.cart.CartResponseDTO;
import dto.user.UserDTO;
import dto.user.UserRequestDTO;
import dto.user.UserUpdateRequestDTO;
import lombok.RequiredArgsConstructor;
import org.bson.Document;
import org.junit.jupiter.api.Assertions;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import service.CommonUtils;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Service
@Profile("test")
@RequiredArgsConstructor
public class TestRawService {

    private final RestTemplate restTemplateSSigned;

    private String URL = "https://localhost:8444/api/v1";

    public UserDTO registerUser(String userId, UserRequestDTO requestDTOF) {

        HttpHeaders headers = HeadersUtils.appendUserIdHeader(null, userId);
        UserDTO registerUser = restTemplateSSigned.exchange(
                URL + "/user/register",
                HttpMethod.POST,
                new HttpEntity<>(requestDTOF, headers),
                UserDTO.class).getBody();

        Assertions.assertNotNull(registerUser);
        return registerUser;
    }

    public UserDTO updateUser(String accessToken, UserUpdateRequestDTO requestDTO) {
        HttpHeaders headers = HeadersUtils.appendAccessTokenHeader(null, accessToken);
        UserDTO registerUser = restTemplateSSigned.exchange(
                URL + "/user",
                HttpMethod.PUT,
                new HttpEntity<>(requestDTO, headers),
                UserDTO.class).getBody();

        Assertions.assertNotNull(registerUser);
        return registerUser;
    }

    public CartResponseDTO getCart(String userId, String accessToken) {
        HttpHeaders headers = combineHeaders(userId, accessToken);

        CartResponseDTO cart = restTemplateSSigned.exchange(
                URL + "/cart/self",
                HttpMethod.GET,
                new HttpEntity<>("body", headers),
                CartResponseDTO.class).getBody();

        Assertions.assertNotNull(cart);
        return cart;
    }

    public CartResponseDTO addToCart(String userId, String accessToken, Map<String, Integer> products) {
        HttpHeaders headers = combineHeaders(userId, accessToken);

        CartResponseDTO cart = restTemplateSSigned.postForEntity(
                URL + "/cart/add",
                new HttpEntity<>(products, headers),
                CartResponseDTO.class).getBody();

        Assertions.assertNotNull(cart);
        return cart;
    }

    public CartResponseDTO removeFromCart(String userId, String accessToken, Map<String, Integer> products) {
        HttpHeaders headers = combineHeaders(userId, accessToken);

        CartResponseDTO cart = restTemplateSSigned.postForEntity(
                URL + "/cart/remove",
                new HttpEntity<>(products, headers),
                CartResponseDTO.class).getBody();

        Assertions.assertNotNull(cart);
        return cart;
    }

    public Page<UserDTO> getUsers(String accessToken, int page, int size) {

        HttpHeaders httpHeaders = HeadersUtils.appendAccessTokenHeader(null, accessToken);
        ParameterizedTypeReference<CustomPageImpl<UserDTO>> type = new ParameterizedTypeReference<>() {
        };

        return restTemplateSSigned.exchange(
                URL + "/user/list?page=" + page + "&size=" + size,
                HttpMethod.GET,
                new HttpEntity<>("body", httpHeaders),
                type).getBody();
    }

    public String generateTempUser() {
        ResponseEntity<Document> response = restTemplateSSigned.postForEntity(URL + "/user", null, Document.class);
        assertTrue(response.getStatusCode().is2xxSuccessful());
        Document body = response.getBody();
        Object userId = body.get("userId");
        Assertions.assertNotNull(body);
        Assertions.assertNotNull(userId);
        Assertions.assertTrue(userId instanceof String);
        return (String) userId;
    }

    public Page<Product> getProducts(String userId, String accessToken) {
        HttpHeaders headers = combineHeaders(userId, accessToken);
        return getProducts(headers, 0, 1000);
    }

    public Page<Product> getProducts(String userId, String accessToken, int page, int size) {

        HttpHeaders headers = new HttpHeaders();
        userId = userId == null ? null : userId;
        userId = userId.isEmpty() ? null : userId;

        if (userId != null) {
            headers = HeadersUtils.appendUserIdHeader(null, userId);
        }
        accessToken = accessToken == null ? null : accessToken;
        accessToken = accessToken.isEmpty() ? null : accessToken;
        if (accessToken != null) {
            headers = HeadersUtils.appendAccessTokenHeader(null, accessToken);
        }
        Assertions.assertFalse((userId == null && accessToken == null));
        Assertions.assertTrue(page >= 0 && size >= 0);
        return getProducts(headers, page, size);
    }

    public Page<Product> getProducts(HttpHeaders headers, int page, int size) {
        ParameterizedTypeReference<CustomPageImpl<Product>> type = new ParameterizedTypeReference<>() {
        };
        return restTemplateSSigned.exchange(
                URL + "/product?page=" + page + "&size=" + size,
                HttpMethod.GET,
                new HttpEntity<>("body", headers),
                type).getBody();
    }

    public TokenDetails loginAsUser() {
        TokenRequest credentials = TokenRequest.builder().email("user@user.pl").password("user").build();
        HttpHeaders headers = HeadersUtils.appendUserIdHeader(null, generateTempUser());
        ResponseEntity<TokenDetails> loginResponse = restTemplateSSigned.postForEntity(
                URL + "/token",
                new HttpEntity<>(credentials, headers),
                TokenDetails.class);

        return loginResponse.getBody();
    }

    public TokenDetails login(String userId, TokenRequest tokenRequest) {

        HttpHeaders headers = HeadersUtils.appendUserIdHeader(null, userId);
        ResponseEntity<TokenDetails> loginResponse = restTemplateSSigned.postForEntity(
                URL + "/token",
                new HttpEntity<>(tokenRequest, headers),
                TokenDetails.class);

        return loginResponse.getBody();
    }

    public TokenDetails refreshToken(String accessToken, String refreshToken, String sessionId) {

        HttpHeaders headers = HeadersUtils.appendAccessTokenHeader(null, accessToken);
        headers = HeadersUtils.appendSessionIdHeader(headers, sessionId);
        ResponseEntity<TokenDetails> loginResponse = restTemplateSSigned.exchange(
                URL + "/token",
                HttpMethod.PUT,
                new HttpEntity<>(refreshToken, headers),
                TokenDetails.class);

        return loginResponse.getBody();
    }

    public Boolean logout(String accessToken, String sessionId) {

        HttpHeaders headers = HeadersUtils.appendAccessTokenHeader(null, accessToken);
        headers = HeadersUtils.appendSessionIdHeader(headers, sessionId);
        ResponseEntity<Boolean> loginResponse = restTemplateSSigned.exchange(
                URL + "/token",
                HttpMethod.DELETE,
                new HttpEntity<>("body", headers),
                Boolean.class);

        return loginResponse.getBody();
    }


    private HttpHeaders combineHeaders(String userId, String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        userId = CommonUtils.emptyToNull(userId);
        if (userId != null) {
            headers = HeadersUtils.appendUserIdHeader(null, userId);
        }
        accessToken = CommonUtils.emptyToNull(accessToken);
        if (accessToken != null) {
            headers = HeadersUtils.appendAccessTokenHeader(null, accessToken);
        }
        return headers;
    }

}
