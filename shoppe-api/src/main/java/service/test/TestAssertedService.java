package service.test;

import app.config.auth.TokenDetails;
import app.config.auth.TokenRequest;
import dao.domain.Product;
import dto.cart.CartResponseDTO;
import dto.user.UserDTO;
import dto.user.UserRequestDTO;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import service.CommonUtils;

import java.util.Date;
import java.util.Map;

@Service
@Profile("test")
@RequiredArgsConstructor
public class TestAssertedService {

    private final TestRawService testRawService;

    public UserDTO registerUser(String userId, UserRequestDTO requestDTOF) {
        UserDTO userDTO = testRawService.registerUser(userId, requestDTOF);

        Assertions.assertNotNull(userDTO);
        Assertions.assertNotNull(userDTO.getEmail());
        Assertions.assertFalse(userDTO.getEmail().isEmpty());
        Assertions.assertNotNull(userDTO.getCartId());
        Assertions.assertFalse(userDTO.getCartId().isEmpty());

        Assertions.assertNotNull(userDTO.getFirstName());
        Assertions.assertFalse(userDTO.getFirstName().isEmpty());
        Assertions.assertNotNull(userDTO.getLastName());
        Assertions.assertFalse(userDTO.getLastName().isEmpty());
        return userDTO;
    }

    public String generateTempUser() {
        String tempUserId = testRawService.generateTempUser();
        Assertions.assertNotNull(tempUserId);
        Assertions.assertFalse(tempUserId.isEmpty());
        return tempUserId;
    }

    public Page<Product> getProductsWithToken(String accessToken) {
        return testRawService.getProducts(null, accessToken);
    }

    public Page<Product> getProductsWithTemp(String tempUserId) {
        return testRawService.getProducts(tempUserId, null);
    }

    public Page<Product> getProducts(String userId, String accessToken) {
        Page<Product> products = testRawService.getProducts(userId, accessToken);
        Assertions.assertNotNull(products);
        Assertions.assertTrue(products.getTotalElements() > 0);
        return products;
    }

    public Page<Product> getProducts(String userId, String accessToken, int page, int size) {
        return testRawService.getProducts(userId, accessToken, page, size);
    }

    private Page<Product> getProducts(HttpHeaders headers, int page, int size) {
        return testRawService.getProducts(headers, page, size);
    }

    public CartResponseDTO getCart(String userId, String accessToken) {
        return testRawService.getCart(userId, accessToken);
    }

    public CartResponseDTO addToCart(String userId, String accessToken, Map<String, Integer> products) {
        return testRawService.addToCart(userId, accessToken, products);
    }

    public CartResponseDTO removeFromCart(String userId, String accessToken, Map<String, Integer> products) {
        return testRawService.removeFromCart(userId, accessToken, products);
    }

    public TokenDetails loginAsUser() {
        TokenDetails tokenDetails = testRawService.loginAsUser();
        validateTokenDetails(tokenDetails);
        return tokenDetails;
    }

    public TokenDetails login(String userId, TokenRequest tokenRequest) {
        TokenDetails tokenDetails = testRawService.login(userId, tokenRequest);
        validateTokenDetails(tokenDetails);
        return tokenDetails;
    }

    public TokenDetails refreshToken(TokenDetails tokenDetails) {
        return testRawService.refreshToken(tokenDetails.getAccessToken(), tokenDetails.getRefreshToken(), tokenDetails.getSessionId());
    }

    public TokenDetails refreshToken(String accessToken, String refreshToken, String sessionId) {
        Assertions.assertNotNull(accessToken);
        Assertions.assertFalse(accessToken.isEmpty());
        Assertions.assertNotNull(refreshToken);
        Assertions.assertFalse(refreshToken.isEmpty());
        Assertions.assertNotNull(sessionId);
        Assertions.assertFalse(sessionId.isEmpty());
        TokenDetails refreshedTokenDetails = testRawService.refreshToken(accessToken, refreshToken, sessionId);
        validateTokenDetails(refreshedTokenDetails);
        return refreshedTokenDetails;
    }

    public Boolean logout(String accessToken, String sessionId) {
        accessToken = CommonUtils.emptyToNull(accessToken);
        sessionId = CommonUtils.emptyToNull(sessionId);

        Assertions.assertNotNull(accessToken);
        Assertions.assertNotNull(sessionId);
        Boolean logout = testRawService.logout(accessToken, sessionId);
        Assertions.assertTrue(logout);
        return true;
    }

    private void validateTokenDetails(TokenDetails tokenDetails) {
        Assertions.assertNotNull(tokenDetails);
        Assertions.assertNotNull(tokenDetails.getAccessToken());
        Assertions.assertFalse(tokenDetails.getAccessToken().isEmpty());
        Assertions.assertNotNull(tokenDetails.getRefreshToken());
        Assertions.assertFalse(tokenDetails.getRefreshToken().isEmpty());
        Assertions.assertNotNull(tokenDetails.getSessionId());
        Assertions.assertFalse(tokenDetails.getSessionId().isEmpty());
        Assertions.assertNotNull(tokenDetails.getSessionId());
        Assertions.assertFalse(tokenDetails.getSessionId().isEmpty());
        Assertions.assertNotNull(tokenDetails.getAccessExpire());
        Assertions.assertTrue(tokenDetails.getAccessExpire() > new Date().getTime());
        Assertions.assertNotNull(tokenDetails.getRefreshToken());
        Assertions.assertTrue(tokenDetails.getRefreshExpire() > tokenDetails.getAccessExpire());

        System.out.println("----------------------------------------------");
        System.out.println("accessToken: " + tokenDetails.getAccessToken());
        System.out.println("refreshToken: " + tokenDetails.getRefreshToken());
        System.out.println("accessTokenExpireDate: " + new Date(tokenDetails.getAccessExpire()));
        System.out.println("refreshTokenExpireDate: " + new Date(tokenDetails.getRefreshExpire()));
        System.out.println("----------------------------------------------");
    }
}
