package service;

import com.mongodb.client.gridfs.model.GridFSFile;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import service.exception.FileException;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Base64;

@Service
@Slf4j
public class FileService {

    @Autowired
    private GridFsTemplate gridFsTemplate;

    private static Tika tika = new Tika();

    public String saveFile(File file) {
        try {
            String type = tika.detect(file);
            String filename = file.getName();
            FileInputStream inputStream = new FileInputStream(file);
            return gridFsTemplate.store(inputStream, filename, type).toString();
        } catch (Exception ex) {
            log.error("File exception" + ex.getMessage());
            throw new FileException(ex.getMessage());
        }
    }

    public String getFileAsBase64(String fileId) {
        try {
            InputStream inputStream = getInputStream(fileId);
            byte[] fileData = inputStream.readAllBytes();
            return Base64.getEncoder().encodeToString(fileData);
        } catch (Exception ex) {
            log.error("File exception" + ex.getMessage());
            throw new FileException(ex.getMessage());
        }
    }


    public InputStream getInputStream(String fileId) {
        try {
            GridFSFile file = gridFsTemplate.findOne(Query.query(Criteria.where("_id").is(fileId)));
            GridFsResource resource = gridFsTemplate.getResource(file.getFilename());
            return resource.getInputStream();
        } catch (Exception ex) {
            log.error("File exception" + ex.getMessage());
            throw new FileException(ex.getMessage());
        }
    }


}
