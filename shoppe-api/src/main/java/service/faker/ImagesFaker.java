package service.faker;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import service.FileService;
import service.exception.FileException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ImagesFaker {

    private final FileService fileService;

    public String getAMD1Thumbnail() {
        return saveToGrid("amd10.png");
    }

    public List<String> getAMD1Images() {
        List<String> images = new ArrayList<>();
        images.add(saveToGrid("amd11.png"));
        images.add(saveToGrid("amd12.png"));
        return images;
    }

    public String getAMD3Thumbnail() {
        return saveToGrid("amd30.png");
    }

    public List<String> getAMD3Images() {
        List<String> images = new ArrayList<>();
        images.add(saveToGrid("amd31.png"));
        images.add(saveToGrid("amd32.png"));
        return images;
    }

    public String getBatThumbnail() {
        return saveToGrid("bat0.png");
    }

    public List<String> getBatImages() {
        List<String> images = new ArrayList<>();
        images.add(saveToGrid("bat1.png"));
        images.add(saveToGrid("bat2.png"));
        return images;
    }

    public String getCamThumbnail() {
        return saveToGrid("cam0.png");
    }

    public List<String> getCamImages() {
        List<String> images = new ArrayList<>();
        images.add(saveToGrid("cam1.png"));
        images.add(saveToGrid("cam2.png"));
        return images;
    }

    public String getIntel5Thumbnail() {
        return saveToGrid("intel50.png");
    }

    public List<String> getIntel5Images() {
        List<String> images = new ArrayList<>();
        images.add(saveToGrid("intel51.png"));
        images.add(saveToGrid("intel52.png"));
        return images;
    }

    public String getIntel7Thumbnail() {
        return saveToGrid("intel70.png");
    }

    public List<String> getIntel7Images() {
        List<String> images = new ArrayList<>();
        images.add(saveToGrid("intel71.png"));
        return images;
    }

    public String getMatThumbnail() {
        return saveToGrid("mat0.png");
    }

    public List<String> getMatImages() {
        List<String> images = new ArrayList<>();
        images.add(saveToGrid("mat1.png"));
        images.add(saveToGrid("mat2.png"));
        return images;
    }

    public String getSeatThumbnail() {
        return saveToGrid("seat0.png");
    }

    public List<String> getSeatImages() {
        List<String> images = new ArrayList<>();
        images.add(saveToGrid("seat1.png"));
        images.add(saveToGrid("seat2.png"));
        return images;
    }

    static String[] colorThumbnails = new String[6];

    public String[] getColorThumbnails() {
        if (colorThumbnails[0] == null) {
            colorThumbnails[0] = saveToGrid("colors/blue.png");
            colorThumbnails[1] = saveToGrid("colors/green.png");
            colorThumbnails[2] = saveToGrid("colors/orange.png");
            colorThumbnails[3] = saveToGrid("colors/red.png");
            colorThumbnails[4] = saveToGrid("colors/violet.png");
            colorThumbnails[5] = saveToGrid("colors/yellow.png");
        }
        return colorThumbnails;
    }

    static String[] colorThumbnailsB = new String[6];

    public String[] getColorThumbnailsBig() {
        if (colorThumbnailsB[0] == null) {
            colorThumbnailsB[0] = saveToGrid("colors/blueb.png");
            colorThumbnailsB[1] = saveToGrid("colors/greenb.png");
            colorThumbnailsB[2] = saveToGrid("colors/orangeb.png");
            colorThumbnailsB[3] = saveToGrid("colors/redb.png");
            colorThumbnailsB[4] = saveToGrid("colors/violetb.png");
            colorThumbnailsB[5] = saveToGrid("colors/yellowb.png");
        }
        return colorThumbnailsB;
    }

    private String saveToGrid(String url) {
        try {
            File file = new File(ImagesFaker.class.getClassLoader().getResource("productImages/" + url).getFile());
            return fileService.saveFile(file);
        } catch (Exception ex) {
            log.error("File not found" + ex);
            throw new FileException("File saving exception: " + ex.getMessage());
        }
    }

}
