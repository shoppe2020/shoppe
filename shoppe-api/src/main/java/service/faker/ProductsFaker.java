package service.faker;


import dao.domain.Product;
import dao.domain.Warranty;
import dao.enums.WarrantyType;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
@RequiredArgsConstructor
public class ProductsFaker {

    private final ImagesFaker imagesFaker;

    private Random rnd = new Random();

    public Product generateGamingSeat(String reviewId) {

        BigDecimal netto = new BigDecimal("419.99");
        netto = netto.setScale(2, RoundingMode.HALF_EVEN);

        BigDecimal tax = new BigDecimal("0.23");

        BigDecimal brutto = netto.add(netto.multiply(tax));
        brutto = brutto.setScale(2, RoundingMode.HALF_EVEN);

        String name = "Fotel Gamingowy Obrotowy Biurowy Kubełkowy Gracza";
        String descriptions = "FOTEL GAMINGOWY\n" +
                "\n" +
                "    SILIKONOWE KÓŁKA – zastosowanie silikonowych kółek powoduje, że podłoga nie jest narażona za zarysowania, a dywan pozostaje w stanie nienaruszonym. Wyklucza to również korzystanie z uciążliwych podkładek, które nie są łatwe w utrzymaniu czystości, a po pewnym czasie wyglądają nieestetycznie. Silikonowe kółka sprawiają, że podłoże jest bezpieczne, a poruszanie się nimi jest ciche oraz komfortowe. Kiedy większość osób regeneruje swoje siły przed kolejnym dniem, gracz nie traci ani chwili i nocami przechodzi kolejne poziomy – NASZE KÓŁKA spowodują, że podczas przemieszczania pozostali domownicy nie będą słyszeć hałasu, który mógłby zaburzyć ich sen.\n" +
                "\n" +
                "    REGULOWANE ODCHYLENIE Z BLOKADĄ – fotel posiada możliwość odchylenia w dowolnej pozycji, która umożliwi komfortowe i dopasowane użytkowanie podczas grania, ale nie tylko – doskonale sprawdzi się do wielogodzinnej pracy – fotel zadba o prawidłowe ułożenie kręgosłupa, po wybraniu odpowiedniej pozycji możemy ją zablokować wciskając dźwignię w dół. Jeżeli użytkownik zapragnie chwili relaksu wystarczy odchylić oparcie i nie blokować dźwigni, umożliwi to bujanie zapewniając jednocześnie odpowiednie krążenie krwi, a także redukuje napięcie w mięśniach. Korzystając z odchylenia możemy fotel wykorzystać również do oglądania filmów itp.\n" +
                "\n" +
                "    DODATKOWE PODUSZKI + WYSUWANY PODNÓŻEK – oprócz specjalnego, kubełkowego profilu, krzesło posiada dodatkowo 2 poduszki: pod głowę oraz pod kręgosłup, które skutecznie odciążają odcinek szyjny i lędźwiowy; poduszki są na pasach zapinanych „na klik” gwarantujących stabilność; nie przesuwają się, a także umożliwiają ustawienie ich na dogodnej dla siebie wysokości. Dodatkowo fotel posiada wysuwany, rozkładany podnóżek, chowany pod siedziskiem, zapewnia on komfort i poprawia krążenie krwi w nogach. Prawidłowa pozycja minimalizuje zmęczenie organizmu i wyklucza ból mięśni powodowany nieprawidłowym ułożeniem. Zadbaj o swoje zdrowie, a także o zdrowie swojego dziecka!\n" +
                "\n" +
                "    KUBEŁKOWE SIEDZISKO - ATRAKCYJNY WYGLĄD – fotel cechuje się ciekawym i atrakcyjnym wyglądem; utrzymany jest w sportowej, typowo męskiej stylistyce; staranne wykonanie oraz estetyczne wykończenie z dbałością o detale niewątpliwie nadają mu walorów. Krzesło posiada szerokie, kubełkowe siedzisko, które zapewni swobodę i wygodę podczas użytkowania. Świetnie utrzymuje użytkownika podczas ekstremalnych rozgrywek. W fotelu zastosowano również antypoślizgowe podłokietniki, które są odporne na odkształcenia; umożliwiają wygodne oparcie zmniejszając ból łokci, a także obciążenie kręgosłupa. Pokrowiec fotela można zdjąć i wyprać i niskiej temperaturze, dzięki czemu utrzymanie go w idealnym stanie nie stanowi problemu. Produkt posiada certyfikaty SGS oraz CE, które potwierdzają najwyższą jakość produktu.\n" +
                "\n" +
                "    SPECYFIKACJA -  certyfikaty: CE, SGS; kolor: czarno-niebieski / czarno-czerwony / czarno-pomarańczowy; maksymalne obciążenie: 180kg; materiał: metal + pianka EPE + ekoskóra; kółka: silikon; wymiary całkowite (dł/szer/wys): 124/63/63cm; waga:19,5kg; waga z opakowaniem: 19,8kg";
        Map<String, String> params = new HashMap<>() {{

            put("Producent", "MALATEC");
            put("Model", "8978");
            put("Kod producenta", "8978 8979 8980");
            put("Waga (z opakowaniem) [kg]", "19.8");
        }};


        return Product.builder()
                .name(name)
                .parameters(params)
                .description(descriptions)
                .netto(netto.doubleValue())
                .tax(tax.doubleValue())
                .thumbnail(imagesFaker.getSeatThumbnail())
                .images(imagesFaker.getSeatImages())
                .reviewsIds(new ArrayList<>(Arrays.asList(reviewId)))
                .warranty(Warranty.builder().period(12).type(WarrantyType.SELLER).build())
                .group("0.1.1")
                .serialNumber(("0.1.1" + new ObjectId().toString()).replace(".", ""))
                .brutto(brutto.doubleValue())
                .available(rnd.nextInt(3000) + 100)
                .build();
    }

    public Product generateCamera(String reviewId) {

        BigDecimal netto = new BigDecimal("74.99");
        netto = netto.setScale(2, RoundingMode.HALF_EVEN);

        BigDecimal tax = new BigDecimal("0.23");

        BigDecimal brutto = netto.add(netto.multiply(tax));
        brutto = brutto.setScale(2, RoundingMode.HALF_EVEN);

        String name = "KAMERKA MOVIE USB SONY PS3 EYE";
        String descriptions = "Wysokiej jakości kamera dedykowana do konsoli SONY  PlayStation PS3\n" +
                "\n" +
                "Kamera działa także z komputerami oraz laptopami po zainstalowaniu sterowników ( sterowniki nie są dołączone, są dostępne w internecie)\n" +
                "\n" +
                "Produkt używany, może posiadać normalne ślady eksploatacji jak   ryski, otarcia lub delikatne uszkodzenia powstałe podczas transportu. W pełni sprawny.\n" +
                "\n" +
                "Kamera nie współpracuje z komunikatorem SKYPE oraz MESSENGER w Windows 10";
        Map<String, String> params = new HashMap<>() {{

            put("Wbudowany mikrofon", "Tak");
            put("Kod producenta", "7194");
            put("Model", "2PlayStation 3 Eye");
        }};

        return Product.builder()
                .name(name)
                .parameters(params)
                .description(descriptions)
                .netto(netto.doubleValue())
                .tax(tax.doubleValue())
                .thumbnail(imagesFaker.getCamThumbnail())
                .images(imagesFaker.getCamImages())
                .reviewsIds(new ArrayList<>(Arrays.asList(reviewId)))
                .warranty(Warranty.builder().period(12).type(WarrantyType.SELLER).build())
                .group("0.1.1")
                .serialNumber(("0.1.1" + new ObjectId().toString()).replace(".", ""))
                .brutto(brutto.doubleValue())
                .available(rnd.nextInt(3000) + 100)
                .build();
    }

    public Product generateMatrix(String reviewId) {

        BigDecimal netto = new BigDecimal("183.95");
        netto = netto.setScale(2, RoundingMode.HALF_EVEN);

        BigDecimal tax = new BigDecimal("0.23");

        BigDecimal brutto = netto.add(netto.multiply(tax));
        brutto = brutto.setScale(2, RoundingMode.HALF_EVEN);

        String name = "Matryca do Lenovo IdeaPad 700-15ISK Matowa";
        String descriptions = "Matryca BOE HB156FH1-402 do laptopów 15.6 cala, 1920x1080 FHD, eDP 30 pin, matowa\n" +
                "\n" +
                "   * Fabrycznie nowa, oryginalna matryca BOE HB156FH1-402 z matowym ekranem. Wersja SLIM z czterema zaczepami. Matryca łączy się z laptopem za pomocą popularnego gniazda eDP 30 pin.\n" +
                "   * Matryca działa w rozdzielczości 1920x1080 FHD (Full High Definition), co pozwala na doskonałą jakość wyświetlanego obrazu. Jej przekątna to 15.6 cala\n" +
                "   * Matryca pracuje w standardzie 262K, czyli oferuje pełną paletę barw dostępną dla ludzkiego oka. Podświetlana jest nowoczesną metodą WLED, co zapewnia większy komfort użytkowania i redukuje uczucie zmęczonych oczu.\n" +
                "   * Matryca działa w technologi TN, zapewniającą szybki czas reakcji, a także brak efektu smużenia. Jest to szczególnie ważne podczas gier oraz dynamicznych scen filmowych.";
        Map<String, String> params = new HashMap<>() {{

            put("Rozdzielczość (px)", "1366 x 768");
            put("Przekątna", "15.6\"");
            put("Powłoka matrycy", "matowa");
            put("Ekran dotykowy", "Nie");
            put("Rodzaj podświetlenia:", "LED");
            put("Złącze", "30pin");
            put("Typ matrycy", "TN");
            put("Waga (z opakowaniem) [kg]", "0.4");
        }};
        return Product.builder()
                .name(name)
                .parameters(params)
                .description(descriptions)
                .netto(netto.doubleValue())
                .tax(tax.doubleValue())
                .thumbnail(imagesFaker.getMatThumbnail())
                .images(imagesFaker.getMatImages())
                .reviewsIds(new ArrayList<>(Arrays.asList(reviewId)))
                .warranty(Warranty.builder().period(12).type(WarrantyType.SELLER).build())
                .group("0.2.3.3")
                .serialNumber(("0.2.3.3" + new ObjectId().toString()).replace(".", ""))
                .brutto(brutto.doubleValue())
                .available(rnd.nextInt(3000) + 100)
                .build();

    }

    public Product generateBattery(String reviewId) {

        BigDecimal netto = new BigDecimal("99.99");
        netto = netto.setScale(2, RoundingMode.HALF_EVEN);

        BigDecimal tax = new BigDecimal("0.23");

        BigDecimal brutto = netto.add(netto.multiply(tax));
        brutto = brutto.setScale(2, RoundingMode.HALF_EVEN);

        String name = "Bateria Dell 0KM742 0KM752 0KM760 Nexxon";
        String descriptions = "Wysokiej klasy ogniwa oraz zaawansowana, inteligentna elektronika to gwarancja niezawodności i pewności użytkowania. Precyzja i jakość wykonania sprawiają, że baterie Nexxon są całkowicie zgodne z oryginalnymi akumulatorami.\n" +
                "\n" +
                "Baterie Nexxon to:\n" +
                "\n" +
                "    wysokiej klasy, wydajne ogniwa litowo-jonowe o rzeczywistej pojemności 4400mAh (49Wh)\n" +
                "    długa i stabilna praca, która w zależności od typu i konfiguracji komputera oraz sposobu pracy może wynieść od 1,5h do nawet 4h\n" +
                "    inteligentna elektronika, zabezpieczająca baterię przed groźnymi zdarzeniami (przegrzanie, wahania napięcia, przepięcia itp.)\n" +
                "    niespotykana wydajność, czyli 90% sprawności baterii nawet po 500 cyklach ładowania\n" +
                "    perfekcyjne wykonanie z dbałością o szczegóły, gwarantuje pełną kompatybilność z dedykowanymi laptopami Dell\n" +
                "    pewność i zaufanie, dzięki gwarancji jakości oraz sprawnej obsłudze posprzedażowej\n";
        Map<String, String> params = new HashMap<>() {{

            put("Do laptopów", "Dell");
            put("Rodzaj", "zamiennik");
            put("Pojemność [mAh] ", "4400");
            put("Napięcie [V]", "11.1");
            put("Producent", "Nexxon");
            put("Waga (z opakowaniem) [kg]", "0.15");
        }};
        return Product.builder()
                .name(name)
                .parameters(params)
                .description(descriptions)
                .netto(netto.doubleValue())
                .tax(tax.doubleValue())
                .thumbnail(imagesFaker.getBatThumbnail())
                .images(imagesFaker.getBatImages())
                .reviewsIds(new ArrayList<>(Arrays.asList(reviewId)))
                .warranty(Warranty.builder().period(12).type(WarrantyType.SELLER).build())
                .group("0.1.1")
                .serialNumber(("0.1.1" + new ObjectId().toString()).replace(".", ""))
                .brutto(brutto.doubleValue())
                .available(rnd.nextInt(3000) + 100)
                .build();
    }

    public Product generateProcessor1(String reviewId) {

        BigDecimal netto = new BigDecimal("519.99");
        netto = netto.setScale(2, RoundingMode.HALF_EVEN);

        BigDecimal tax = new BigDecimal("0.23");

        BigDecimal brutto = netto.add(netto.multiply(tax));
        brutto = brutto.setScale(2, RoundingMode.HALF_EVEN);

        String name = "PROCESOR RYZEN 5 1600AF@2600 6 RDZENI 3,6 GHZ BOX";
        String descriptions = "Gigabyte GeForce RTX 2070 Super Gaming OC 3X 8G jest jedną z kart graficznych do gier " +
                "z serii 2000 firmy NVIDIA i stanowi łącze pomiędzy produktami klasy high-end i mid-range. " +
                "Zastosowany układ graficzny to TU104-410-A1 z 2 560 rdzeniami CUDA na GeForce RTX 2070 Super. Chip " +
                "jest produkowany w procesie FinFET 12 nm i opiera się na architekturze GPU Turing. Wykorzystywana " +
                "pamięć to GDDR6-VRAM o pojemności 8 GB, natomiast rdzenie ray tracingu i tensora zapewniają " +
                "fantastyczne efekty świetlne i cieniste. W nowym superwariancie zwiększono zegar bazowy i podbicia, " +
                "a jednostki cieniujące (rdzenie cuda) zwiększono z 2.304 do 2.560. ";
        Map<String, String> params = new HashMap<>() {{
            put("Producent", "AMD");
            put("Model", "Ryzen 5 1600");
            put("Seria", "AMD Ryzen 5");
            put("Typ gniazda procesora", "AMD Socket AM4");
            put("Liczba rdzeni procesora", "6");
            put("Liczba wątków", "12");
            put("Taktowanie bazowe procesora [Ghz]", "3.2");
            put("Proces technologiczny [nm]", "14");

        }};
        return Product.builder()
                .name(name)
                .parameters(params)
                .description(descriptions)
                .netto(netto.doubleValue())
                .tax(tax.doubleValue())
                .thumbnail(imagesFaker.getAMD1Thumbnail())
                .images(imagesFaker.getAMD1Images())
                .reviewsIds(new ArrayList<>(Arrays.asList(reviewId)))
                .warranty(Warranty.builder().period(12).type(WarrantyType.SELLER).build())
                .group("0.16.1")
                .serialNumber(("0.16.1" + new ObjectId().toString()).replace(".", ""))
                .brutto(brutto.doubleValue())
                .available(rnd.nextInt(3000) + 100)
                .build();
    }

    public Product generateProcessor2(String reviewId) {

        BigDecimal netto = new BigDecimal("859.99");
        netto = netto.setScale(2, RoundingMode.HALF_EVEN);

        BigDecimal tax = new BigDecimal("0.23");

        BigDecimal brutto = netto.add(netto.multiply(tax));
        brutto = brutto.setScale(2, RoundingMode.HALF_EVEN);

        String name = "Procesor AMD Ryzen 5 3600 3.6-4.2 GHz AM4 6C/12T";
        String descriptions = "rocesor AMD Ryzen 5 3600 ma sześć rdzeni i nadaje się do umieszczenia na płycie głównej z Socket AM4. Procesor działa z częstotliwością zegara 3600 MHz i trybem turbo do 4200 MHz. Ten procesor jest wyposażony w chłodnicę Wraith Stealth.\n" +
                "\n" +
                "    Typ procesora: AMD Ryzen\n" +
                "    Model procesora: 3600\n" +
                "    Liczba rdzeni: 6\n" +
                "    Liczba wątków: 12\n" +
                "    Częstotliwość taktowania procesora [GHz]: 3.6\n" +
                "    Częstotliwość maksymalna Turbo [GHz]: 4.2\n" +
                "    Pamięć podręczna: 32 MB\n" +
                "    TDP: 65 W\n" +
                "    Typ gniazda: Socket AM4\n" +
                "    Zintegrowany układ graficzny: Nie\n" +
                "    Obsługa instrukcji 64-bit: Tak\n" +
                "    Hyper-Transport: Nie\n" +
                "    Hyper-Treading: Nie\n" +
                "    Dołączony wentylator: Tak\n" +
                "    Proces technologiczny [nm]: 7\n" +
                "    Generacja procesora: 3gen\n" +
                "    Przeznaczenie: Komputer PC\n" +
                "    Wyposażenie: Brak\n" +
                "    Załączona dokumentacja: Instrukcja obsługi w języku polskim Karta gwarancyjna ";
        Map<String, String> params = new HashMap<>() {{
            put("Producent", "AMD");
            put("Model", "Ryzen 5 1600");
            put("Seria", "AMD Ryzen 5");
            put("Typ gniazda procesora", "AMD Socket AM4");
            put("Liczba rdzeni procesora", "6");
            put("Liczba wątków", "12");
            put("Taktowanie bazowe procesora [Ghz]", "3.6");
            put("Proces technologiczny [nm]", "7");
        }};
        return Product.builder()
                .name(name)
                .parameters(params)
                .description(descriptions)
                .netto(netto.doubleValue())
                .tax(tax.doubleValue())
                .thumbnail(imagesFaker.getAMD3Thumbnail())
                .images(imagesFaker.getAMD3Images())
                .reviewsIds(new ArrayList<>(Arrays.asList(reviewId)))
                .warranty(Warranty.builder().period(12).type(WarrantyType.SELLER).build())
                .group("0.16.1")
                .serialNumber(("0.16.1" + new ObjectId().toString()).replace(".", ""))
                .brutto(brutto.doubleValue())
                .available(rnd.nextInt(3000) + 100)
                .build();
    }

    public Product generateProcessor3(String reviewId) {

        BigDecimal netto = new BigDecimal("929.99");
        netto = netto.setScale(2, RoundingMode.HALF_EVEN);

        BigDecimal tax = new BigDecimal("0.23");

        BigDecimal brutto = netto.add(netto.multiply(tax));
        brutto = brutto.setScale(2, RoundingMode.HALF_EVEN);

        String name = "Procesor INTEL Core i5-9600KF 3.7-4.6 GHz 6C/6T";
        String descriptions = "Procesor INTEL Core i5-9600KF\n" +
                "\n" +
                "    Typ procesora: Core i5\n" +
                "    Model procesora: i5-9600KF\n" +
                "    Liczba rdzeni: 6\n" +
                "    Częstotliwość taktowania procesora [GHz]: 3.7\n" +
                "    Typ gniazda: FCLGA1151\n" +
                "\n" +
                "    Indeks: 553660";
        Map<String, String> params = new HashMap<>() {{
            put("Model", "Core i5-9600KF");
            put("Seria", "Intel Core i5");
            put("Producent", "Intel");
            put("Typ gniazda procesora", "Intel Socket 1151");
            put("Liczba rdzeni procesora", "6");
            put("Liczba wątków", "6");
            put("Taktowanie bazowe procesora [Ghz]", "3.7");
            put("Proces technologiczny [nm]", "14");
        }};
        return Product.builder()
                .name(name)
                .parameters(params)
                .description(descriptions)
                .netto(netto.doubleValue())
                .tax(tax.doubleValue())
                .thumbnail(imagesFaker.getIntel5Thumbnail())
                .images(imagesFaker.getIntel5Images())
                .reviewsIds(new ArrayList<>(Arrays.asList(reviewId)))
                .warranty(Warranty.builder().period(12).type(WarrantyType.SELLER).build())
                .group("0.16.1")
                .serialNumber(("0.16.1" + new ObjectId().toString()).replace(".", ""))
                .brutto(brutto.doubleValue())
                .available(rnd.nextInt(3000) + 100)
                .build();


    }

    public Product generateProcessor4(String reviewId) {

        BigDecimal netto = new BigDecimal("1499.00");
        netto = netto.setScale(2, RoundingMode.HALF_EVEN);

        BigDecimal tax = new BigDecimal("0.23");

        BigDecimal brutto = netto.add(netto.multiply(tax));
        brutto = brutto.setScale(2, RoundingMode.HALF_EVEN);

        String name = "Intel Core i7-2600K 3.4GHz LGA1155 Odblokowany OC";
        String descriptions = "Intel Core i7 - najszybsze modele posiadające cztery lub więcej rdzeni, technologię Hyper Threading i tryby Turbo Boost, łącząc najlepsze cechy wcześniej wymienionych układów. Zapewniają bezkompromisową wydajność na każdym froncie.\n" +
                "\n" +
                "Intel seria K - procesory z odblokowanym mnożnikiem skierowane do entuzjastów podkręcania i nielimitowanej wydajności, którzy w razie potrzeby mogą samodzielnie zwiększyć ich taktowanie ponad standardowe ustawienia.\n" +
                "Specyfikacja techniczna\n" +
                "\n" +
                "    Model procesora: i7-2600\n" +
                "    Taktowanie: 3400 MHz (3,4 GHz)\n" +
                "    Taktowanie Turbo: 3800 MHz (3,8 GHz)\n" +
                "    Technologia: 32 nm\n" +
                "    Szyna danych: 64 bit\n" +
                "    Ilość rdzeni: 4\n" +
                "    Ilość wątków: 8\n" +
                "    Cache L3: 8 MB\n" +
                "    Pobór mocy: 95 W";
        Map<String, String> params = new HashMap<>() {{
            put("Model", "Core i7-2600");
            put("Seria", "Intel Core i7");
            put("Producent", "Intel");
            put("Typ gniazda procesora", "Intel Socket 1155");
            put("Liczba rdzeni procesora", "4");
            put("Liczba wątków", "8");
            put("Taktowanie bazowe procesora [Ghz]", "3.4");
            put("Proces technologiczny [nm]", "32");
        }};
        return Product.builder()
                .name(name)
                .parameters(params)
                .description(descriptions)
                .netto(netto.doubleValue())
                .tax(tax.doubleValue())
                .thumbnail(imagesFaker.getIntel7Thumbnail())
                .images(imagesFaker.getIntel7Images())
                .reviewsIds(new ArrayList<>(Arrays.asList(reviewId)))
                .warranty(Warranty.builder().period(36).type(WarrantyType.SELLER).build())
                .group("0.16.1")
                .serialNumber(("0.16.1" + new ObjectId().toString()).replace(".", ""))
                .brutto(brutto.doubleValue())
                .available(rnd.nextInt(3000) + 100)
                .build();
    }

}
