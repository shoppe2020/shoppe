package service;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Service;
import service.exception.EntityNotFoundException;
import service.others.Enumable;

import java.util.*;

@Service
@SuppressWarnings("unchecked")
public class EnumService {
    private final String enumPackagePath = "dao.enums";

    public Map<String, List<String>> getAllEnums() {
        Map<String, List<String>> enums = new HashMap<>();
        findAnnotatedClasses(enumPackagePath, enums);
        return enums;
    }

    private void findAnnotatedClasses(String scanPackage, Map<String, List<String>> enums) {
        ClassPathScanningCandidateComponentProvider provider = createComponentScanner();
        for (BeanDefinition beanDef : provider.findCandidateComponents(scanPackage)) {
            printMetadata(beanDef, enums);
        }
    }

    private void printMetadata(BeanDefinition beanDef, Map<String, List<String>> enums) {
        try {
            Class clazz = Class.forName(beanDef.getBeanClassName());
            if (clazz.getAnnotation(Enumable.class) != null) {
                enums.put(clazz.getSimpleName(), castByOrdinal(clazz));
            }
        } catch (Exception e) {
            throw new EntityNotFoundException(Enum.class.getSimpleName().toLowerCase(), "enum");
        }
    }

    private ClassPathScanningCandidateComponentProvider createComponentScanner() {
        ClassPathScanningCandidateComponentProvider provider
                = new ClassPathScanningCandidateComponentProvider(false);
        provider.addIncludeFilter(new AnnotationTypeFilter(Enumable.class));
        return provider;
    }

    private <F extends Enum> List<String> castByOrdinal(final Class<F> fClass) {
        final Iterator<F> iter = EnumSet.allOf(fClass).iterator();
        List<String> names = new ArrayList<>();
        while (iter.hasNext()) {
            names.add(iter.next().name());
        }
        return names;
    }


}
