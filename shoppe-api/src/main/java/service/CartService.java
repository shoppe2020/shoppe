package service;

import dao.domain.Cart;
import dao.repo.CartRepository;
import dto.cart.CartRequestDTO;
import dto.cart.CartResponseDTO;
import dto.product.ProductBaseDTO;
import io.jsonwebtoken.lang.Assert;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import service.exception.OperationException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class CartService extends BasicService<Cart, CartRepository, CartRequestDTO> {

    private final ProductService productService;

    public void mergeUsersCarts(String rootCartId, String tempCartId) {

        Assert.notNull(rootCartId, "Root cartId cannot be null");
        Assert.notNull(tempCartId, "Temp cartId cannot be null");

        Cart rootCart = findById(rootCartId);
        Cart tempCart = findById(tempCartId);

        Map<String, Integer> rootCartProducts = rootCart.getProducts();
        Map<String, Integer> tempCartProducts = tempCart.getProducts();

        tempCartProducts.forEach((k, v) -> {
            if (rootCartProducts.containsKey(k)) {
                Integer rootQuantity = rootCartProducts.get(k);
                rootCartProducts.replace(k, rootQuantity + v);
            } else {
                rootCartProducts.put(k, v);
            }
        });

        tempCart.getProducts().clear();
        tempCart.setDescription("");

        save(rootCart);
        save(tempCart);
    }

    public Cart createEmptyCart() {
        return save(Cart.builder().build());
    }

    public CartResponseDTO addToCart(Cart cart, Map<String, Integer> productsToAdd) {
        Assert.notNull(productsToAdd, "Products cannot be null");
        Assert.notEmpty(productsToAdd, "Products cannot be empty");
        try {
            Map<String, Integer> tempProducts = new HashMap<>();
            tempProducts.putAll(cart.getProducts());
            productsToAdd.forEach((k, v) ->
            {
                productService.findById(k);
                if (!tempProducts.containsKey(k)) {
                    tempProducts.put(k, v);
                } else {
                    Integer count = tempProducts.get(k);
                    count = count + v;
                    tempProducts.replace(k, count);
                }
            });
            cart.setProducts(tempProducts);
            return convertToDTO(save(cart));

        } catch (Exception ex) {
            log.error("Cannot add products to cart");
            throw new OperationException("Cannot add products to cart");
        }
    }

    public CartResponseDTO removeFromCart(Cart cart, Map<String, Integer> productsToRemove) {
        Assert.notNull(productsToRemove, "Products cannot be null");
        Assert.notEmpty(productsToRemove, "Products cannot be empty");

        try {
            Map<String, Integer> cartProducts = cart.getProducts();
            productsToRemove.forEach((k, v) ->
            {
                if (cartProducts.containsKey(k)) {
                    Integer count = cartProducts.get(k);
                    if (count >= v) {
                        if (count - v <= 1) {
                            cartProducts.remove(k);
                        } else {
                            cartProducts.replace(k, count - v);
                        }
                    }
                } else {
                    throw new OperationException("Product is not in cart");
                }
            });

            return convertToDTO(save(cart));

        } catch (Exception ex) {
            log.error("Product is not in cart");
            throw new OperationException("Product is not in cart");
        }
    }

    public CartResponseDTO convertToDTO(Cart cart) {
        try {
            List<ProductBaseDTO> productsDTO = productService.convertToBaseDTO(cart.getProducts());
            return CartResponseDTO.builder().id(cart.getId()).products(productsDTO).description(cart.getDescription()).build();
        } catch (Exception ex) {
            log.error("Products conversion exception");
            throw new OperationException("Products conversion exception");
        }
    }
}
