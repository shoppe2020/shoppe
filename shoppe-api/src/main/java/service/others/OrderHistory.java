package service.others;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import dao.enums.OrderStatus;

@Data
@AllArgsConstructor
@Builder
public class OrderHistory {

    private OrderStatus statusFrom;
    private OrderStatus statusTo;
    private Long time;
    private String description;
}
