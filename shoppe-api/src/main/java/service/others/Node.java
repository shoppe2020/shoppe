package service.others;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@AllArgsConstructor
@Data
public class Node {

    private String id;
    private String name;
    private List<Node> children = new LinkedList<>();
    @JsonIgnore
    private Node parent = null;

    public Node(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Node(String name) {
        this.name = name;
    }

    public void setNodes(Node... nodes) {
        List<Node> children = new LinkedList<>(Arrays.asList(nodes));
        int i = 1;
        for (Node child : children) {
            child.setId(String.valueOf(i));
            child.setParent(this);
            i++;
        }
        this.children = children;
    }

    public String getPath() {
        Node localParent = parent;
        String path = id;
        while (localParent != null) {
            path = localParent.getId() + "." + path;
            localParent = localParent.getParent();
        }
        return path;
    }

    private String getHighestGroup(String path) {
        try {
            int i = path.indexOf(".");
            if (i == -1 && path.length() > 0) {
                return path;
            }
            return path.substring(0, i);
        } catch (Exception ex) {
            return null;
        }
    }

    public String findNameByPath(String path) {
        String nameByPath = findNameByPath(this, path, "");
        return nameByPath.substring(0, nameByPath.length() - 3);
    }

    private String findNameByPath(Node child, String path, String result) {

        String highestGroup = getHighestGroup(path);
        if (highestGroup != null && highestGroup.equals(child.id)) {
            result += child.getName() + " : ";
            int i = path.indexOf(".") + 1;
            if (i == 0) return result;
            path = path.substring(i);
            for (Node ch : child.getChildren()) {
                String nameByPath = findNameByPath(ch, path, result);
                if (!nameByPath.equals(result)) {
                    return nameByPath;
                }
            }
        }


        return result;
    }


    @Override
    public String toString() {
        return "Node{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", childrenSize=" + children.size() +
                ", parent=" + parent +
                '}';
    }

}
