package service;

import dao.domain.Product;
import dao.domain.Review;
import dao.repo.ProductRepository;
import dto.product.ProductBaseDTO;
import dto.product.ProductDTO;
import dto.product.ProductRequestDTO;
import dto.product.ReviewResponseDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import service.exception.EntityNotFoundException;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductService extends BasicService<Product, ProductRepository, ProductRequestDTO> {

    @Autowired
    private FileService fileService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private MongoTemplate mongoTemplate;

    public Map<String, Map<String, Set<String>>> getProductsParameters() {
        return getRepository().getProductsParameters(mongoTemplate);
    }

    public Page<ProductBaseDTO> findAllFilter(MultiValueMap<String, List<String>> filters, String group, String name, Boolean removed, Pageable pageable) {
        MultiValueMap<String, List<String>> singleFilters = removeRedundant(filters);
        Page<ProductBaseDTO> all = getRepository().findAllFilter(singleFilters, group, name,removed, pageable, mongoTemplate);
        return all.map(this::downloadThumbnail);
    }

    public List<ProductBaseDTO> convertToBaseDTO(Map<String, Integer> products) {
        List<ProductBaseDTO> productBaseDTOS = new ArrayList<>();
        products.forEach((k, v) ->
                productBaseDTOS.add(convertToBaseDTO(k, v)));
        return productBaseDTOS;
    }

    private ProductBaseDTO convertToBaseDTO(String productId, Integer count) {
        ProductBaseDTO productBase = getRepository().findBaseByName(productId)
                .orElseThrow(() -> new EntityNotFoundException("Product not found", productId));

        productBase.setCount(count);
        downloadThumbnail(productBase);
        return productBase;
    }

    public ProductDTO findProductById(String id) {
        Product product = findById(id);
        long visited = product.getVisited();
        product.setVisited(visited + 1);
        return convertToDTO(save(product));
    }

    private ProductDTO convertToDTO(Product product) {
        List<String> reviewsIds = product.getReviewsIds();
        reviewsIds = reviewsIds == null ? new LinkedList<>() : reviewsIds;
        Set<Review> reviews = reviewsIds.stream().map(reviewService::findById).collect(Collectors.toSet());
        Set<ReviewResponseDTO> reviewsDTO = reviews.stream()
                .map(reviewService::convertToDTO)
                .collect(Collectors.toSet());

        ProductDTO mappedProduct = getModelMapper().map(product, ProductDTO.class);
        mappedProduct.setReviews(reviewsDTO);

        downloadThumbnail(mappedProduct);
        downloadImages(mappedProduct);
        return mappedProduct;
    }

    private ProductBaseDTO downloadThumbnail(ProductBaseDTO productBaseDTO) {
        String thumbnailId = productBaseDTO.getThumbnail();
        try {
            String thumbnailAsBase64 = fileService.getFileAsBase64(thumbnailId);
            productBaseDTO.setThumbnail(thumbnailAsBase64);
        } catch (Exception ex) {
            log.error("Cannot download thumbnail: " + thumbnailId);
        }
        return productBaseDTO;
    }

    private ProductDTO downloadImages(ProductDTO productDTO) {
        List<String> imagesInBase64 = new LinkedList<>();
        try {
            productDTO.getImages().forEach(imageId ->
                    imagesInBase64.add(fileService.getFileAsBase64(imageId)));
            productDTO.setImages(imagesInBase64);
        } catch (Exception ex) {
            log.error("Cannot download images for product: " + productDTO.getId());
        }
        return productDTO;
    }

    public MultiValueMap<String, List<String>> removeRedundant(MultiValueMap<String, List<String>> filters) {
        if (filters.containsKey("group")) {
            filters.remove("group");
        }
        if (filters.containsKey("name")) {
            filters.remove("name");
        }
        if (filters.containsKey("page")) {
            filters.remove("page");
        }
        if (filters.containsKey("size")) {
            filters.remove("size");
        }
        if (filters.containsKey("sort")) {
            filters.remove("sort");
        }
        if (filters.containsKey("removed")) {
            filters.remove("removed");
        }
        return filters;
    }

    public Product create(ProductRequestDTO request) {
        Product product = getModelMapper().map(request, Product.class);
        product.setId(null);
        product.setReviewsIds(new LinkedList<>());
        product.setImages(new ArrayList<>());
        product.setBrutto(CommonUtils.calculateBrutto(product.getNetto(), product.getTax()));
        return save(product);
    }

    public Product update(ProductRequestDTO request) {
        Product product = getModelMapper().map(request, Product.class);
        product.setBrutto(CommonUtils.calculateBrutto(product.getNetto(), product.getTax()));
        return save(product);
    }
}
