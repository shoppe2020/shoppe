package service;

import io.jsonwebtoken.lang.Assert;
import org.junit.jupiter.api.Assertions;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class CommonUtils {


    public static Double calculateBrutto(double netto, double tax) {
        BigDecimal nettoB = new BigDecimal(netto);
        BigDecimal taxB = new BigDecimal(tax);
        BigDecimal multiply = nettoB.multiply(taxB);
        BigDecimal brutto = multiply.add(nettoB);
        brutto.setScale(2, RoundingMode.HALF_EVEN);
        return brutto.doubleValue();
    }

    public static String partyHideString(String str) {
        int length = str.length();
        Assertions.assertNotNull(str);
        Assertions.assertFalse(str.isEmpty());
        Assertions.assertTrue(length > 2);

        String start = String.valueOf(str.charAt(0));
        String end = String.valueOf(str.charAt(length - 1));

        StringBuilder resultString = new StringBuilder();
        for (int i = 0; i < length - 2; i++) {
            resultString.append("*");
        }
        return start + resultString.toString() + end;
    }

    public static String emptyToNull(String str) {
        try {
            return str.isEmpty() ? null : str;
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getSessionIdFromHeader(HttpServletRequest request) {
        String sessionId = request.getHeader("sessionId");
        if (sessionId != null && !sessionId.isEmpty()) {
            return sessionId;
        }
        return null;
    }

    public static String getUserIdFromHeader(HttpServletRequest request) {
        String sessionId = request.getHeader("userId");
        if (sessionId != null && !sessionId.isEmpty()) {
            return sessionId;
        }
        return null;
    }

    public static String getAccessTokenFromHeader(HttpServletRequest request) {
        String tokenBearer = request.getHeader("Authorization");
        if (tokenBearer != null && !tokenBearer.isEmpty()) {
            return tokenBearer.substring(7);
        }
        return null;
    }


    public static List<String> mergeLists(List<String> rootList, List<String> tempList) {
        Assert.notNull(rootList, "Root list cannot be null");
        Assert.notNull(tempList, "Temp list cannot be null");
        if (tempList.isEmpty()) return rootList;
        tempList.forEach((e) -> {
            if (!rootList.contains(e)) {
                rootList.add(e);
            }
        });
        return rootList;
    }

}
