package service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    @Autowired
    public JavaMailSender emailSender;

    public void resetPassword(String to, String newPassword) {
        String subject = "Password remind";
        String content = "</h1> Your new  password: " + newPassword + "</h1>";
        sendSimpleMessage(to, subject, content, true);
    }


    public void sendEmailToken(String to, String emailToken) {
        String subject = "Verify your email";
        String content = "<a href = https://localhost:4200/confirm?email=" + to + "&emailToken=" + emailToken + ">Verify your email </a>";
        sendSimpleMessage(to, subject, content, true);
    }

    public void sendSimpleMessage(String to, String subject, String text, boolean html) {
        try {
            javax.mail.internet.MimeMessage mimeMessage = emailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom("Shoppe_2020");
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text, html);
            emailSender.send(mimeMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
