package service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import dao.domain.ParentDAO;
import dto.ParentDTO;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import service.exception.EntityNotFoundException;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;


/**
 * @param <E> Entity
 * @param <R> Repository
 * @param <D> DTO
 */
@Slf4j
@Getter
@SuppressWarnings({"unchecked", "SpringJavaInjectionPointsAutowiringInspection"})
public abstract class BasicService<
        E extends ParentDAO,
        R extends MongoRepository & QuerydslPredicateExecutor<E>,
        D extends ParentDTO> {

    @Autowired
    private R repository;

    @Autowired
    private ModelMapper modelMapper;

    public E findById(String id) {
        if (id == null) {
            throw new EntityNotFoundException(getClazz().getSimpleName(), "null");
        }
        Optional optEntity = repository.findById(id);

        if (optEntity.isEmpty()) {
            throw new EntityNotFoundException(getClazz().getSimpleName(), id);
        }
        return (E) optEntity.get();
    }

    public List<E> findAll() {
        return repository.findAll();
    }

    public Page<E> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public Page<E> findAll(Predicate predicate, Pageable pageable) {

        if (predicate != null) {
            if (predicate instanceof BooleanBuilder) {
                return repository.findAll(pageable);
            } else {
                Page<E> all = repository.findAll(predicate, pageable);
                if (all.getContent().isEmpty()) {
                    throw new EntityNotFoundException(getClazz().getSimpleName(), predicate.toString());
                }
                return all;
            }
        } else {
            return repository.findAll(pageable);
        }
    }

    public E save(E document) {
        return (E) repository.save(document);
    }

    public boolean saveAll(List<E> documents) {
        documents.forEach((doc) -> repository.save(doc));
        return true;
    }

    public E create(D document) {
        Class<E> clazz = getClazz();
        E newDocument = modelMapper.map(document, clazz);
        newDocument = (E) repository.save(newDocument);
        return newDocument;
    }

    public E update(D updatedDocument) {
        Class<E> clazz = getClazz();

        String id = updatedDocument.getId();
        Optional<E> opt = repository.findById(id);
        if (!opt.isPresent()) {
            throw new EntityNotFoundException(clazz.getSimpleName(), id);
        }
        E existingDocument = opt.get();
        E mappedDocument = modelMapper.map(updatedDocument, clazz);
        E mapOnlyChangedFields = mapOnlyChangedFields(existingDocument, mappedDocument);
        mapOnlyChangedFields = (E) repository.save(mapOnlyChangedFields);
        return mapOnlyChangedFields;
    }

    public E update(E existingDocument, D updatedDocument) {
        E mappedDocument = modelMapper.map(updatedDocument, getClazz());
        E mapOnlyChangedFields = mapOnlyChangedFields(existingDocument, mappedDocument);
        mapOnlyChangedFields = (E) repository.save(mapOnlyChangedFields);
        return mapOnlyChangedFields;
    }

    public E update(E existingDocument, E updatedDocument) {
        E mapOnlyChangedFields = mapOnlyChangedFields(existingDocument, updatedDocument);
        return (E) repository.save(mapOnlyChangedFields);
    }

    public E delete(E document) {
        document.setRemoved(Boolean.TRUE);
        return (E) repository.save(document);
    }

    public E deleteById(String id) {
        Optional<E> opt = repository.findById(id);
        if (!opt.isPresent()) {
            throw new EntityNotFoundException(getClazz().getSimpleName(), id);
        }
        E document = opt.get();
        document.setRemoved(Boolean.TRUE);
        return (E) repository.save(document);
    }

    public void deletePermanent(String id) {
        repository.deleteById(id);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    public void dropCollection(MongoTemplate mongoTemplate) {
        mongoTemplate.dropCollection(getClazz());
    }

    public long count() {
        return repository.count();
    }


    private Class<E> getClazz() {
        Class<E>[] clazz = (Class<E>[]) GenericTypeResolver
                .resolveTypeArguments(
                        getClass(),
                        BasicService.class);
        return clazz[0];
    }

    public E mapOnlyChangedFields(E existingDocument, E updatingDocument) {
        Field[] declaredFields = existingDocument.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            try {
                field.setAccessible(true);
                Object updatingField = field.get(updatingDocument);
                if (updatingField != null) {
                    field.set(existingDocument, updatingField);
                }
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                log.error("Cannot update entity\nfrom: " + existingDocument + ", to\n" + updatingDocument);
            }
        }
        return existingDocument;
    }

}
