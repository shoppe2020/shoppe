package service;

import dao.domain.*;
import dao.enums.OrderStatus;
import dao.repo.OrderRepository;
import dto.order.*;
import io.jsonwebtoken.lang.Assert;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import service.exception.OperationException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService extends BasicService<Order, OrderRepository, OrderRequestDTO> {

    private final ModelMapper modelMapper;

    private final ProductService productService;

    private final UserService userService;

    private final CartService cartService;

    private final DeliveryService deliveryService;


    public OrderItemDTO convertOrderItemToDTO(OrderItem orderItem) {
        OrderItemDTO mappedItem = modelMapper.map(orderItem, OrderItemDTO.class);
        Product product = productService.findById(orderItem.getProductId());
        mappedItem.setProduct(product);
        return mappedItem;
    }

    public OrderDTO convertToDTO(Order order) {
        Assert.notNull(order, "Order cannot be null");

        OrderDTO mappedOrder = modelMapper.map(order, OrderDTO.class);
        List<OrderItemDTO> orderItemDTOS = order.getOrderItems().stream().map(this::convertOrderItemToDTO).collect(Collectors.toList());
        mappedOrder.setOrderItems(orderItemDTOS);

        return mappedOrder;
    }

    public Page<OrderDTO> findOrdersFromPrincipal(String headerUserId, Principal principal, Pageable pageable) {
        String userDbId;
        if (principal == null) {
            userDbId = userService.findByTempUserId(headerUserId).getId();
        } else {
            userDbId = userService.findUserFromPrincipal(principal).getId();
        }
        Page<Order> orders = getRepository().findAllByUserId(userDbId, pageable);
        return orders.map(this::convertToDTO);
    }

    public List<OrderItem> convertProductsToOrderItems(Map<String, Integer> productsIds) {

        List<OrderItem> orderItems = new ArrayList<>();
        productsIds.forEach((k, v) ->
        {
            Product product = productService.findById(k);
            OrderItem orderItem = OrderItem.builder()
                    .productId(k)
                    .netto(product.getNetto())
                    .tax(product.getTax())
                    .brutto(product.getBrutto())
                    .count(v)
                    .build();

            orderItems.add(orderItem);
        });

        return orderItems;
    }

    public Double calculateTotalPrice(List<OrderItem> items, double deliveryPrice) {
        BigDecimal productsPrice = new BigDecimal(BigInteger.ZERO);

        for (OrderItem item : items) {
            Double brutto = item.getBrutto();
            Integer count = item.getCount();

            BigDecimal bruttoDecimal = new BigDecimal(brutto);
            BigDecimal countDecimal = new BigDecimal(count);

            productsPrice = productsPrice.add(bruttoDecimal.multiply(countDecimal));

        }

        BigDecimal delivery = new BigDecimal(deliveryPrice);
        productsPrice = productsPrice.add(delivery);
        productsPrice = productsPrice.setScale(2,
                RoundingMode.HALF_EVEN);

        return productsPrice.doubleValue();


    }

    private Order createOrderFromProducts(Map<String, Integer> productsIds, String userId, OrderRequestDTO requestDTO) {

        String deliveryId = requestDTO.getDeliveryId();
        Delivery delivery = deliveryService.findById(deliveryId);
        List<OrderItem> orderItems = convertProductsToOrderItems(productsIds);
        return save(Order.builder()
                .userId(userId)
                .orderItems(orderItems)
                .status(OrderStatus.CREATED)
                .totalPrice(calculateTotalPrice(orderItems, delivery.getPrice()))
                .payment(requestDTO.getPayment())
                .address(requestDTO.getAddress())
                .createTime(new Date().getTime())
                .lastUpdateTime(new Date().getTime())
                .delivery(delivery)
                .build());
    }

    public OrderDTO createOrderFromCart(String headerUserId, Principal principal, OrderRequestDTO requestDTO) {
        User user;
        if (principal == null) {
            user = userService.findByTempUserId(headerUserId);
        } else {
            user = userService.findUserFromPrincipal(principal);
        }

        Cart cart = cartService.findById(user.getCartId());
        Assert.notEmpty(cart.getProducts(), "Cart cannot be empty");

        Order savedOrder = createOrderFromProducts(cart.getProducts(), user.getId(), requestDTO);

        user.getOrdersIds().add(savedOrder.getId());
        user.setCartId(cartService.createEmptyCart().getId());
        userService.save(user);
        return convertToDTO(savedOrder);
    }

    public OrderDTO changeOrderStatus(OrderChangeStatusDTO request, Principal principal) {
        Assert.notNull(request, "Request is null");
        Assert.notNull(principal, "Access denied");

        OrderStatus oldStatus = request.getOldStatus();
        OrderStatus newStatus = request.getNewStatus();
        Assert.notNull(oldStatus, "OldStatus is null");
        Assert.notNull(newStatus, "NewStatus is null");
        if (oldStatus.equals(newStatus)) throw new OperationException("Statuses are equal");

        Order order = findById(request.getOrderId());
        order.setStatus(newStatus);
        order.setLastUpdateTime(new Date().getTime());
        return convertToDTO(save(order));

    }

}
