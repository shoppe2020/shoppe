package service;

import dao.domain.*;
import dao.enums.WarrantyType;
import dao.repo.DeliveryRepository;
import dao.repo.ProductRepository;
import dao.repo.ReviewRepository;
import dao.repo.UserRepository;
import dto.user.UserRequestDTO;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import service.faker.ImagesFaker;
import service.faker.ProductsFaker;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
@RequiredArgsConstructor
public class SetupService extends BasicService<User, UserRepository, UserRequestDTO> {

    private final CartService cartService;

    private final ProductRepository productRepository;

    private final UserRepository userRepository;

    private final ReviewRepository reviewRepository;

    private final PasswordEncoder passwordEncoder;

    private final ImagesFaker imagesFaker;

    private final ProductsFaker productsFaker;

    private final DeliveryRepository deliveryRepository;

    private Random rnd = new Random();

    public void createDefaultDeliveries() {

        deliveryRepository.findByName("COURIER").orElseGet(() -> deliveryRepository.save(Delivery.builder()
                .name("COURIER")
                .price(12d)
                .order(1)
                .build()));
        deliveryRepository.findByName("COURIER_CASH_ON_DELIVERY").orElseGet(() -> deliveryRepository.save(Delivery.builder()
                .name("COURIER_CASH_ON_DELIVERY")
                .price(14d)
                .order(1)
                .build()));
        deliveryRepository.findByName("AT_THE_POINT").orElseGet(() -> deliveryRepository.save(Delivery.builder()
                .name("AT_THE_POINT")
                .price(8d)
                .order(1)
                .build()));
        deliveryRepository.findByName("SELF_PICKUP").orElseGet(() -> deliveryRepository.save(Delivery.builder()
                .name("SELF_PICKUP")
                .price(0d)
                .order(1)
                .build()));

    }

    public String createDefaultUsers() {

        String admin = "admin";
        String adminEmail = "admin@admin.pl";
        userRepository.findByEmail(adminEmail).orElseGet(() -> userRepository.save(User.builder()
                .password(passwordEncoder.encode(admin))
                .email(adminEmail)
                .role(admin.toUpperCase())
                .firstName(admin)
                .lastName(admin)
                .cartId(cartService.createEmptyCart().getId())
                .build()));


        String user = "user";
        String userEmail = "user@user.pl";
        return userRepository.findByEmail(userEmail).orElseGet(() -> userRepository.save(User.builder()
                .password(passwordEncoder.encode(user))
                .email(userEmail)
                .role(user.toUpperCase())
                .firstName(user)
                .lastName(user)
                .cartId(cartService.createEmptyCart().getId())
                .build())).getId();

    }

    public void createDefaultProducts(String userId) {

        if (productRepository.count() > 0) return;
        long a = System.currentTimeMillis();

        Review review = Review.builder().rate(rnd.nextInt(5) + 1)
                .description("Dobry sprzet\n Zalety:\n -a\n -b \n -c \n\n Wady: \n -a \n -b \n -c")
                .userId(userId)
                .build();

        String reviewId = reviewRepository.save(review).getId();
        productRepository.save(productsFaker.generateBattery(reviewId));
        productRepository.save(productsFaker.generateCamera(reviewId));
        productRepository.save(productsFaker.generateGamingSeat(reviewId));
        productRepository.save(productsFaker.generateMatrix(reviewId));
        productRepository.save(productsFaker.generateProcessor1(reviewId));
        productRepository.save(productsFaker.generateProcessor2(reviewId));
        productRepository.save(productsFaker.generateProcessor3(reviewId));
        productRepository.save(productsFaker.generateProcessor4(reviewId));

        String[] colorThumbnails = imagesFaker.getColorThumbnails();
        String[] colorThumbnailsBig = imagesFaker.getColorThumbnailsBig();
        BigDecimal tax = new BigDecimal("0.23");
        Set<String> gPaths = GroupsGenerator.getPaths();
        String[] paths = gPaths.toArray(new String[0]);

        for (int i = 0; i < 100; i++) {
            Map<String, String> params = new HashMap<>() {{
                put("Model", "XYZ");
                put("Typ", "XYZ");
                put("Seria", "XYZ");
                put("SN", "XYZ");
                put("Waga", "XYZ");
            }};

            String colorThumbnail = colorThumbnails[rnd.nextInt(colorThumbnails.length)];
            String colorThumbnailB = colorThumbnailsBig[rnd.nextInt(colorThumbnailsBig.length)];
            String path = paths[rnd.nextInt(paths.length)];
            String nameByPath = GroupsGenerator.getRoot().findNameByPath(path);

            BigDecimal netto = new BigDecimal(rnd.nextDouble() * 1000);
            netto = netto.setScale(2, RoundingMode.HALF_EVEN);

            BigDecimal brutto = netto.add(netto.multiply(tax));
            brutto = brutto.setScale(2, RoundingMode.HALF_EVEN);
            productRepository.save(Product.builder()
                    .name(nameByPath)
                    .parameters(params)
                    .description("Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                            "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s," +
                            " when an unknown printer took a galley of type and scrambled it to make a type specimen book." +
                            " It has survived not only five centuries, but also the leap into electronic typesetting," +
                            " remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset " +
                            "sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like" +
                            " Aldus PageMaker including versions of Lorem Ipsum.")
                    .netto(netto.doubleValue())
                    .tax(tax.doubleValue())
                    .thumbnail(colorThumbnail)
                    .images(new ArrayList<>(Arrays.asList(colorThumbnailB)))
                    .reviewsIds(new ArrayList<>(Arrays.asList(review.getId())))
                    .warranty(Warranty.builder().period(12).type(WarrantyType.SELLER).build())
                    .group(path)
                    .serialNumber((path + new ObjectId().toString()).replace(".", ""))
                    .brutto(brutto.doubleValue())
                    .available(rnd.nextInt(3000) + 100)
                    .build());
        }


        long b = System.currentTimeMillis();
        System.out.printf("TIME: " + (b - a));

    }

    public void clearUsersToken() {
        userRepository.findAll().forEach((u) ->
        {
            u.setSessions(new ArrayList<>());
            userRepository.save(u);
        });
    }


}
