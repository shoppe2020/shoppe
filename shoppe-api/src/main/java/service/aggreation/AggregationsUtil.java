package service.aggreation;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

public class AggregationsUtil {

    private static ModelMapper modelMapper = new ModelMapper();

    public static <T> Page<T> createAggregationWithPagination(List<AggregationOperation> pipeline,
                                                              Pageable pageable,
                                                              Class<?> inputType,
                                                              Class<T> outType,
                                                              MongoTemplate mongoTemplate) {
        String totalName = "total";
        String resultsName = "results";

        pipeline.add(
                facet(count().as(totalName)).as(totalName)
                        .and(convertToPageableOperation(pageable).asArray()).as(resultsName));

        pipeline.add(project(resultsName)
                .and(ArrayOperators.ArrayElemAt.arrayOf(totalName + "." + totalName).elementAt(0)).as(totalName));

        AggregationResults<PageableAggregationResults> aggregate = mongoTemplate.aggregate(
                newAggregation(pipeline),
                inputType,
                PageableAggregationResults.class);

        PageableAggregationResults aggResults = aggregate.getMappedResults().get(0);
        List<Object> rawResults = aggResults.getResults();
        Long total = aggResults.getTotal();
        if (total == null && rawResults.isEmpty()) {
            return new PageImpl<>(new ArrayList<>(), pageable, 0);
        }
        
        if (!aggResults.getResults().isEmpty()) {
            aggResults.setResults(rawResults.stream().map(raw ->
                    modelMapper.map(raw, outType)).collect(Collectors.toList()));
            List<T> results = aggResults.getResults();
            return new PageImpl<>(results, pageable, total);
        } else {
            return new PageImpl<>(new ArrayList<>(), pageable, total);
        }
    }

    public static PageableOperations convertToPageableOperation(Pageable pageable) {
        PageableOperations pageableOperations = new PageableOperations();
        if (pageable == null) {
            pageable = PageRequest.of(0, 1000);
        }
        if (pageable.getSort() != null && pageable.getSort().isSorted()) {
            pageableOperations.setSort(sort(pageable.getSort()));
        }
        pageableOperations.setSkip(skip((long) pageable.getPageNumber() * pageable.getPageSize()));
        pageableOperations.setLimit(limit(pageable.getPageSize()));
        return pageableOperations;
    }
}
