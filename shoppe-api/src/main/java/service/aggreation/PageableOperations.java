package service.aggreation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.LimitOperation;
import org.springframework.data.mongodb.core.aggregation.SkipOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;

import java.util.Arrays;
import java.util.LinkedList;

/*
 *
 *     //ORDER:  SORT->SKIP->LIMIT
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class PageableOperations {

    private SortOperation sort;
    private SkipOperation skip;
    private LimitOperation limit;

    public boolean isSorted() {
        return sort == null ? false : true;
    }

    public LinkedList<AggregationOperation> asLinkedList() {
        if (this.isSorted()) {
            return new LinkedList<>(Arrays.asList(sort, skip, limit));
        } else {
            return new LinkedList<>(Arrays.asList(skip, limit));
        }
    }

    public AggregationOperation[] asArray() {
        LinkedList<AggregationOperation> aggregationOperations = asLinkedList();
        return aggregationOperations.toArray(new AggregationOperation[aggregationOperations.size()]);
    }

}
