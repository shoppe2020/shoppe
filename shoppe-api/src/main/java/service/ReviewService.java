package service;

import dao.domain.Product;
import dao.domain.Review;
import dao.repo.ReviewRepository;
import dto.product.ReviewDTO;
import dto.product.ReviewResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.exception.EntityNotFoundException;

@Service
public class ReviewService extends BasicService<Review, ReviewRepository, ReviewDTO> {
    @Autowired
    private ReviewService reviewService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    public ReviewResponseDTO addReview(String productId, ReviewDTO reviewDTO, String email) {
        Product product = productService.findById(productId);

        String userId = userService.findByEmail(email).getId();
        Review review = Review.builder()
                .description(reviewDTO.getDescription())
                .rate(reviewDTO.getRate())
                .userId(userId)
                .build();

        review = this.save(review);
        product.getReviewsIds().add(review.getId());
        productService.save(product);
        return convertToDTO(review);
    }

    public ReviewResponseDTO removeReview(String productId, String reviewId, String email) {
        String userId = userService.findByEmail(email).getId();
        Product product = productService.findById(productId);
        Review review = reviewService.findById(reviewId);
        if (!product.getReviewsIds().contains(reviewId)) {
            throw new EntityNotFoundException(Review.class.getSimpleName(), reviewId);
        }
        if (!review.getUserId().equals(userId)) {
            throw new EntityNotFoundException(Review.class.getSimpleName(), reviewId);
        }

        review = reviewService.delete(review);
        product.getReviewsIds().remove(reviewId);
        productService.save(product);
        return convertToDTO(review);
    }


    public ReviewResponseDTO convertToDTO(Review review) {
        String userFirstName = userService.findByIdReturnFirstName(review.getUserId());
        ReviewResponseDTO mappedReview = getModelMapper().map(review, ReviewResponseDTO.class);

        mappedReview.setUsername(CommonUtils.partyHideString(userFirstName));
        return mappedReview;
    }

}
