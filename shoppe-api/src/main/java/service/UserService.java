package service;

import app.config.auth.TokenService;
import dao.domain.Cart;
import dao.domain.User;
import dao.repo.UserRepository;
import dto.user.UserChangePasswordRequestDTO;
import dto.user.UserDTO;
import dto.user.UserRequestDTO;
import dto.user.UserUpdateRequestDTO;
import io.jsonwebtoken.lang.Assert;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import service.exception.AuthenticationException;
import service.exception.EntityNotFoundException;
import service.exception.VerificationException;

import java.security.Principal;

@Service
@Slf4j
public class UserService extends BasicService<User, UserRepository, UserRequestDTO> {

    @Autowired
    private CartService cartService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private EmailService emailService;

    public String findByIdReturnFirstName(String id) {
        User user = getRepository().findByIdReturnFirstName(id).orElseThrow(() -> new EntityNotFoundException("User", id));
        return user.getFirstName();
    }

    public User findByTempUserId(String tempUserId) {
        return getRepository().findByTempUserId(tempUserId).orElseThrow(() -> new EntityNotFoundException("User", tempUserId));
    }

    public User findByEmail(String email) {
        return getRepository().findByEmail(email).orElseThrow(() -> new EntityNotFoundException("User", email));
    }

    public User findByRefreshToken(String refreshToken) {
        return getRepository().findByRefreshToken(refreshToken).orElseThrow(() -> new AuthenticationException("Refresh token does not exist"));
    }

    public User findUserFromPrincipal(Principal principal) {
        if (principal == null || principal.getName() == null || principal.getName().isEmpty()) {
            throw new AuthenticationException("Principal is empty");
        }
        return findByEmail(principal.getName());
    }

    public UserDTO getSelf(String headerUserId, Principal principal) {
        if (principal != null) {
            return convertToDTO(findUserFromPrincipal(principal));
        } else {
            return convertToDTO(findByTempUserId(headerUserId));
        }
    }

    public UserDTO convertToDTO(User user) {
        return getModelMapper().map(user, UserDTO.class);
    }

    public Document generateUser() {
        String cartId = cartService.createEmptyCart().getId();
        String userId = new ObjectId().toString();

        save(User.builder()
                .tempUserId(userId)
                .role("TEMP")
                .cartId(cartId)
                .build());

        return new Document("userId", userId);
    }

    public UserDTO updateUser(UserUpdateRequestDTO requestDTO, Principal principal) {
        ModelMapper modelMapper = getModelMapper();
        User mappedUser = modelMapper.map(requestDTO, User.class);
        mappedUser.setSessions(null);
        mappedUser.setObservedProductsIds(null);
        mappedUser.setOrdersIds(null);

        User existUser = findUserFromPrincipal(principal);
        String requestDTOPassword = requestDTO.getPassword();
        String existUserPassword = existUser.getPassword();
        boolean matches = passwordEncoder.matches(requestDTOPassword, existUserPassword);
        if (!matches) {
            throw new AuthenticationException("Password is incorrect");
        }
        mappedUser.setPassword(null);
        mappedUser.setVerified(existUser.isVerified());
        User updatedUser = update(existUser, mappedUser);

        if (!existUser.getEmail().equals(requestDTO.getEmail())) {
            String emailToken = new ObjectId().toString();
            emailService.sendEmailToken(updatedUser.getEmail(), emailToken);
            updatedUser.setVerified(false);
            updatedUser.setEmailToken(emailToken);
            save(updatedUser);
            tokenService.logoutAll(updatedUser.getEmail());
        }
        return convertToDTO(updatedUser);
    }

    public UserDTO changePassword(UserChangePasswordRequestDTO requestDTO, Principal principal) {

        Assertions.assertNotNull(requestDTO);
        Assertions.assertNotNull(requestDTO.getNewPassword());
        Assertions.assertNotNull(requestDTO.getOldPassword());
        Assertions.assertFalse(requestDTO.getNewPassword().isEmpty());
        Assertions.assertFalse(requestDTO.getOldPassword().isEmpty());
        Assertions.assertFalse(requestDTO.getOldPassword().equals(requestDTO.getNewPassword()));

        User user = findUserFromPrincipal(principal);
        Assertions.assertTrue(user.isVerified());

        String existUserPassword = user.getPassword();
        String oldPassword = requestDTO.getOldPassword();
        boolean matches = passwordEncoder.matches(oldPassword, existUserPassword);
        if (!matches) {
            throw new AuthenticationException("Password is incorrect");
        }

        String newPassword = requestDTO.getNewPassword();
        user.setPassword(passwordEncoder.encode(newPassword));

        return convertToDTO(save(user));
    }

    public boolean resetPassword(String email) {
        Assertions.assertNotNull(email);
        Assertions.assertFalse(email.isEmpty());

        try {
            User user = findByEmail(email);
            Assertions.assertTrue(user.isVerified());

            String newPassword = new ObjectId().toString();
            emailService.resetPassword(email, newPassword);
            user.setPassword(passwordEncoder.encode(newPassword));
            save(user);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        return true;
    }

    public UserDTO confirmUser(String email, String emailToken) {
        User user = findByEmail(email);
        Assertions.assertNotNull(user.getEmailToken());
        Assertions.assertFalse(user.getEmailToken().isEmpty());
        Assertions.assertFalse(user.isVerified());
        if (!user.getEmailToken().equals(emailToken)) {
            throw new VerificationException("Email token verification exception");
        }
        user.setVerified(true);
        user.setEmailToken(null);
        return convertToDTO(save(user));
    }

    public UserDTO registerUser(UserRequestDTO requestDTO) {
        Assert.notNull(requestDTO, "Request cannot be empty");

        String cartId = cartService.createEmptyCart().getId();
        String emailToken = new ObjectId().toString();

        User user = save(User.builder()
                .role("USER")
                .firstName(requestDTO.getFirstName())
                .lastName(requestDTO.getLastName())
                .email(requestDTO.getEmail())
                .emailToken(emailToken)
                .isVerified(false)
                .password(passwordEncoder.encode(requestDTO.getPassword()))
                .cartId(cartId)
                .build());

        emailService.sendEmailToken(user.getEmail(), emailToken);

        return convertToDTO(user);
    }

    public void mergeUsers(String login, String headerUserId) {
        User root = findByEmail(login);
        User temp = findByTempUserId(headerUserId);
        if (!root.getId().equals(temp.getId())) {
            Assert.notNull(root, "Root user cannot be null");
            Assert.notNull(temp, "Temp user cannot be null");

            String rootCartId = root.getCartId();
            String tempCartId = temp.getCartId();

            cartService.mergeUsersCarts(rootCartId, tempCartId);
            root.setObservedProductsIds(CommonUtils.mergeLists(root.getObservedProductsIds(), temp.getObservedProductsIds()));
            root.setOrdersIds(CommonUtils.mergeLists(root.getOrdersIds(), temp.getOrdersIds()));
            save(root);
            save(temp);
        }
    }

    public Cart getActualUserCart(String headerUserId, Principal principal) {
        User user;
        if (principal != null) {
            user = findUserFromPrincipal(principal);
        } else {
            if (headerUserId == null) {
                throw new AuthenticationException("UserId must not be null");
            }
            user = findByTempUserId(headerUserId);
        }
        return cartService.findById(user.getCartId());
    }
}
