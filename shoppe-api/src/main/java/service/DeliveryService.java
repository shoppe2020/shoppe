package service;

import dao.domain.Delivery;
import dao.repo.DeliveryRepository;
import dto.misc.DeliveryRequestDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class DeliveryService extends BasicService<Delivery, DeliveryRepository, DeliveryRequestDTO> {

    public List<Delivery> getDeliveries() {
        return getRepository().findAll(Sort.by("order"));
    }
}
