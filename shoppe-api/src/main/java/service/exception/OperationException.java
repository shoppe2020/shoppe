package service.exception;

public class OperationException extends RuntimeException {
    public OperationException(String mess) {
        super(mess);
    }
}
