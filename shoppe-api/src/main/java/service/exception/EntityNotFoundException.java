package service.exception;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String entityName, String entityId) {
        super("Cannot find entity " + entityName + " identified with: " + entityId);
    }
}
