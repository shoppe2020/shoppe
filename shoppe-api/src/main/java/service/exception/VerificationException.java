package service.exception;

public class VerificationException extends RuntimeException {
    public VerificationException(String mess) {
        super(mess);
    }
}
