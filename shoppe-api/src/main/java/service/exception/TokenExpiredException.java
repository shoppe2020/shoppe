package service.exception;

public class TokenExpiredException extends RuntimeException {
    public TokenExpiredException(String mess) {
        super(mess);
    }
}
