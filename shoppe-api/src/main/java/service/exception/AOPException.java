package service.exception;

public class AOPException extends RuntimeException {
    public AOPException(String mess) {
        super(mess);
    }
}
