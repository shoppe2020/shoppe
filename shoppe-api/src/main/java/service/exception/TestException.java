package service.exception;

public class TestException extends RuntimeException {
    public TestException(String mess) {
        super(mess);
    }
}
