package service.exception;

public class TokenInvalidException extends RuntimeException {
    public TokenInvalidException(String mess) {
        super(mess);
    }
}
