package service;


import service.others.Node;

import java.util.HashSet;
import java.util.Set;


public class GroupsGenerator {

    private static Node getBlank() {
        Node category = new Node("Accessories");
        Node category1 = new Node("Batteries");
        Node category2 = new Node("Keyboards");
        Node category3 = new Node("Matrix");
        Node category4 = new Node("Drives");
        Node category5 = new Node("Webcams");
        Node category6 = new Node("Drums");
        Node category7 = new Node("Mainboards");
        Node category8 = new Node("Processors");
        Node category9 = new Node("Fujitsu");
        Node category10 = new Node("Toshiba");
        Node category11 = new Node("Toners");
        Node category12 = new Node("Carcases");
        Node category13 = new Node("Multifunction");
        Node category14 = new Node("Other");


        category.setNodes(category1, category2, category3, category4, category5, category6, category7, category8, category9, category10, category11, category12, category13, category14);
        return category;
    }

    private static Node root;

    private static Set<String> paths = new HashSet<>();

    public static Set<String> getPaths() {
        return paths;
    }

    private static void updatePaths(Node root) {
        for (Node child : root.getChildren()) {
            updatePaths(child, root.getId());
        }
    }

    private static void updatePaths(Node node, String actualPath) {
        actualPath = actualPath + "." + node.getId();


        if (node.getChildren().isEmpty()) {
            paths.add(actualPath);
        } else {
            for (Node child : node.getChildren()) {
                updatePaths(child, actualPath);
            }
        }

    }

    public static Node getRoot() {
        if (root == null) {
            root = new Node("0", "ROOT");

            Node root1 = getAccessories();
            Node root2 = getParts();
            Node root3 = getPrintersAndScanners();
            Node root4 = getDrives();
            Node root5 = getSpeakers();
            Node root6 = getInternet();
            Node root7 = getPCs();
            Node root8 = getCryptovalutes();
            Node root9 = getLaptops();
            Node root10 = getPowerStripsAndUPSs();
            Node root11 = getMicrophonesAndHeadphones();
            Node root12 = getMicrocomputers();
            Node root13 = getComputerMonitors();
            Node root14 = getOpticalDrivesAndMedia();
            Node root15 = getSoftware();
            Node root16 = getComputerComponents();
            Node root17 = getServersAndAccessories();
            Node root18 = getTablets();
            Node root19 = getNetworkEquipment();
            root.setNodes(root1, root2, root3, root4, root5, root6, root7, root8, root9, root10, root11, root12, root13, root14, root15, root16, root17, root18, root19);
            updatePaths(root);
        }

        return root;
    }


    private static Node getNetworkEquipment() {
        Node category = new Node("Network equipment");
        Node category1 = new Node("Network accessories");
        Node category2 = new Node("Antennas");
        Node category3 = new Node("Bluetooth");
        Node category4 = new Node("USB hubs");
        Node category5 = new Node("IP cameras");
        Node category6 = new Node("Network cards");
        Node category7 = new Node("Wi-Fi network cards");
        Node category8 = new Node("Controllers");
        Node category9 = new Node("USB wireless modems");
        Node category10 = new Node("Cable modems");
        Node category11 = new Node("Internal modems");
        Node category12 = new Node("Mobile routers");
        Node category13 = new Node("Wired routers");
        Node category14 = new Node("WiFi routers and Access Points");
        Node category15 = new Node("File Servers (NAS)");

        category.setNodes(category1, category2, category3, category4, category5, category6, category7, category8, category9, category10, category11, category12, category13, category14, category15);
        return category;
    }

    private static Node getTablets() {
        Node category = new Node("Tablets");
        Node category1 = new Node("Devices");
        Node category2 = new Node("Accessories ");
        Node category3 = new Node("Service parts");

        category.setNodes(category1, category2, category3);
        return category;
    }

    private static Node getComputerComponents() {
        Node category = new Node("Computer components");
        Node category1 = new Node("Processors");
        Node category2 = new Node("Motherboards");
        Node category3 = new Node("RAM Memory");
        Node category4 = new Node("Graphic cards");
        Node category5 = new Node("Music cards");
        Node category6 = new Node("Cases ");
        Node category7 = new Node("Power supplies");
        Node category8 = new Node("Cooling and tuning");
        Node category9 = new Node("Service equipment");

        category.setNodes(category1, category2, category3, category4, category5, category6, category7, category8, category9);
        return category;
    }

    private static Node getServersAndAccessories() {
        Node category = new Node("Servers and accessories");
        Node category1 = new Node("Servers");
        Node category2 = new Node("Matrix");
        Node category3 = new Node("Server components");
        Node category4 = new Node("Server accessories");

        category.setNodes(category1, category2, category3, category4);
        return category;
    }

    private static Node getSoftware() {
        Node category = new Node("Software");
        Node category1 = new Node("Antiviruses and security");
        Node category2 = new Node("Office and company management");
        Node category3 = new Node("Graphics and multimedia");
        Node category4 = new Node("Science and education");
        Node category5 = new Node("Operating systems");
        Node category6 = new Node("Other");

        category.setNodes(category1, category2, category3, category4, category5, category6);
        return category;
    }

    private static Node getOpticalDrivesAndMedia() {
        Node category = new Node("Optical drives and media");
        Node category1 = new Node("Recorders and drives");
        Node category2 = new Node("Floppy disk drives");
        Node category3 = new Node("Media");

        category.setNodes(category1, category2, category3);
        return category;
    }

    private static Node getComputerMonitors() {
        Node category = new Node("Computer monitors");
        Node category1 = new Node("Monitors");
        Node category2 = new Node("Parts and accessories");
        category.setNodes(category1, category2);

        return category;
    }

    private static Node getMicrocomputers() {
        Node category = new Node("Microcomputers");
        Node category1 = new Node("Arduino");
        Node category2 = new Node("HDMI Stick");
        Node category3 = new Node("Raspberry Pi");
        Node category4 = new Node("Other");

        category.setNodes(category1, category2, category3, category4);
        return category;
    }

    private static Node getMicrophonesAndHeadphones() {
        Node category = new Node("Microphones and headphones");
        Node category1 = new Node("Microphones");
        Node category2 = new Node("Wireless headphones");
        Node category3 = new Node("Wired headphones");

        category.setNodes(category1, category2, category3);
        return category;
    }

    private static Node getPowerStripsAndUPSs() {
        Node category = new Node("Power strips and UPS");
        Node category1 = new Node("Slats, network filters");
        Node category2 = new Node("UPS");
        Node category3 = new Node("Other");

        category.setNodes(category1, category2, category3);
        return category;
    }

    private static Node getCryptovalutes() {
        Node category = new Node("Cryptovalutes");
        Node category1 = new Node("Accessories");
        Node category2 = new Node("Excavators");
        Node category3 = new Node("Cases");
        Node category4 = new Node("Other");

        category.setNodes(category1, category2, category3, category4);
        return category;
    }

    private static Node getPCs() {
        return new Node("PCs");
    }

    private static Node getInternet() {
        Node category = new Node("Internet");
        Node category1 = new Node("Domains");
        Node category2 = new Node("Links");
        Node category3 = new Node("Advertisements and banners");
        Node category4 = new Node("Hosting services");
        Node category5 = new Node("Internet services");
        Node category6 = new Node("Scripts");
        Node category7 = new Node("Templates");
        Node category8 = new Node("Other");

        category.setNodes(category1, category2, category3, category4, category5, category6, category7, category8);
        return category;
    }

    private static Node getSpeakers() {
        return new Node("Speakers");
    }

    private static Node getDrives() {
        Node parts = new Node("Drives");
        Node parts1 = new Node("HDD");
        Node parts2 = new Node("SSD");
        Node parts3 = new Node("External and Portable Drives");
        Node parts4 = new Node("Mobile memories");
        Node parts5 = new Node("Enclosures and pockets");
        Node parts6 = new Node("Other");

        parts.setNodes(parts1, parts2, parts3, parts4, parts5, parts6);
        return parts;
    }

    private static Node getPrintersAndScanners() {
        Node pas = new Node("Printers and Scanners");
        Node pas1 = new Node("Drums");
        Node pas2 = new Node("Parts and accessories");
        Node pas3 = new Node("3D printers");
        Node pas4 = new Node("Inkjet and laser printers");
        Node pas5 = new Node("Dot matrix printers");
        Node pas6 = new Node("Papers, films");
        Node pas7 = new Node("Plotters");
        Node pas8 = new Node("Colouring tapes");
        Node pas9 = new Node("Toners");
        Node pas10 = new Node("Carcases");
        Node pas11 = new Node("Multifunction");
        Node pas12 = new Node("Other");

        pas.setNodes(pas1, pas2, pas3, pas4, pas5, pas6, pas7, pas8, pas9, pas10, pas11, pas12);
        return pas;
    }

    private static Node getParts() {
        Node parts = new Node("Parts");
        Node parts1 = new Node("Batteries");
        Node parts2 = new Node("Keyboards");
        Node parts3 = getMatrix();
        Node parts4 = new Node("Drives");
        Node parts5 = new Node("Enclosures and hulls");
        Node parts6 = new Node("Memory RAM");
        Node parts7 = new Node("Mainboards");
        Node parts8 = new Node("Processors");
        Node parts9 = new Node("Fans and heat sinks");
        Node parts10 = new Node("Power supplies");
        Node parts11 = new Node("Other");

        parts.setNodes(parts1, parts2, parts3, parts4, parts5, parts6, parts7, parts8, parts9, parts10, parts11);
        return parts;
    }

    private static Node getMatrix() {
        Node parts = new Node("Matrix");
        Node parts1 = new Node("17\" and bigger");
        Node parts2 = new Node("16\" - 16.9\"");
        Node parts3 = new Node("15\" - 15.9\"");
        Node parts4 = new Node("14\" - 14.9\"");
        Node parts5 = new Node("13\" - 13.9\"");
        Node parts6 = new Node("12\" and smaller");
        Node parts7 = new Node("Tapes and inverters");
        parts.setNodes(parts1, parts2, parts3, parts4, parts5, parts6, parts7);
        return parts;
    }

    private static Node getAccessories() {
        Node accessories = new Node("Accessories");
        Node accessories1 = new Node("GSM");
        Node accessories2 = new Node("Cables, tapes, extension cables");
        Node accessories3 = new Node("Webcams");
        Node accessories4 = new Node("Cards and video recorders");
        Node accessories5 = new Node("MiniPCI cards, miniPCIe");
        Node accessories6 = new Node("PCMCIA and ExpressCard");
        Node accessories7 = new Node("Cooling pads");
        Node accessories8 = new Node("Transitions, herrings");
        Node accessories9 = new Node("Converters");
        Node accessories10 = new Node("Docking stations");
        Node accessories11 = new Node("Laptop Tables");
        Node accessories12 = new Node("TV and SAT tuners");
        Node accessories13 = new Node("Video conferencing sets and cameras");
        Node accessories14 = new Node("Bags, covers");
        Node accessories15 = new Node("Toshiba");
        Node accessories16 = new Node("Others");
        accessories.setNodes(accessories1, accessories2, accessories3, accessories4, accessories5, accessories6, accessories7,
                accessories8, accessories9, accessories10, accessories11, accessories12, accessories13, accessories14, accessories15, accessories16);
        return accessories;
    }

    private static Node getLaptops() {
        Node laptops = new Node("Laptops");
        Node laptops1 = new Node("Acer");
        Node laptops2 = new Node("Apple");
        Node laptops3 = new Node("Asus");
        Node laptops4 = new Node("Dell");
        Node laptops5 = new Node("Fujitsu");
        Node laptops6 = new Node("HP, Compaq");
        Node laptops7 = new Node("Huawei");
        Node laptops8 = new Node("IBM, Lenovo");
        Node laptops9 = new Node("Kiano");
        Node laptops10 = new Node("Medion");
        Node laptops11 = new Node("Microsoft");
        Node laptops12 = new Node("MSI");
        Node laptops13 = new Node("Samsung");
        Node laptops14 = new Node("Sony");
        Node laptops15 = new Node("Toshiba");
        Node laptops16 = new Node("Others");
        laptops.setNodes(laptops1, laptops2, laptops3, laptops4, laptops5, laptops6, laptops7,
                laptops8, laptops9, laptops10, laptops11, laptops12, laptops13, laptops14, laptops15, laptops16);
        return laptops;
    }
}
