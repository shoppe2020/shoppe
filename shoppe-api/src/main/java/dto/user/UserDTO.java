package dto.user;

import dao.domain.Address;
import dto.ParentDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class UserDTO extends ParentDTO {

    private String email;

    private String firstName;

    private String lastName;

    private Address address;

    private String cartId;

    private String role;

    private boolean isVerified;

    @Builder.Default
    private List<String> observedProductsIds = new ArrayList<>();

    @Builder.Default
    private List<String> ordersIds = new ArrayList<>();
}
