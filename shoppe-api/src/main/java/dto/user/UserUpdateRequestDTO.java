package dto.user;

import dao.domain.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserUpdateRequestDTO{

    @Email
    private String email;

    @NotEmpty
    @NotNull
    private String password;
    private String firstName;
    private String lastName;
    private Address address;

}
