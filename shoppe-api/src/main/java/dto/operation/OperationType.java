package dto.operation;


import service.others.Enumable;

@Enumable
public enum OperationType {
    UNKNOWN,
    LOGIN,
    REFRESH,
    LOGOUT,
    SHOW_PRODUCT,
    ADD_TO_CART,
    REMOVE_FROM_CART,
    ADD_ORDER,
    ORDER_STATUS_CHANGE;
}
