package dto.product;

import dto.ParentDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ReviewResponseDTO extends ParentDTO {
    private String username;
    private String description;
    private int rate = 1;
    private int good = 0;
    private int wrong = 0;
}
