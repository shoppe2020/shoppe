package dto.product;

import dto.ParentDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReviewDTO extends ParentDTO {

    @NotNull
    @NotEmpty
    private String description;

    @Min(1)
    @Max(5)
    private int rate = 1;
}
