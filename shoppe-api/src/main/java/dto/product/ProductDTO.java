package dto.product;

import dao.domain.Warranty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class ProductDTO extends ProductBaseDTO {
    private Integer available;
    private boolean removed;
    private Warranty warranty;
    private List<String> images;
    private Set<ReviewResponseDTO> reviews;
}
