package dto.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Map;


@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class ProductBaseDTO {
    private String id;
    private String name;
    private String description;
    private Integer count;
    private Double brutto;
    private Double netto;
    private Double tax;
    private String group;
    private String thumbnail;
    private Map<String, String> parameters;
    private String serialNumber;
    private String visited;
}
