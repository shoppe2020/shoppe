package dto.product;

import dao.domain.Warranty;
import dto.ParentDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductRequestDTO extends ParentDTO {

    @NotNull
    @NotEmpty
    private String group;

    @NotNull
    @NotEmpty
    private String name;

    @NotNull
    @NotEmpty
    private String description;

    @NotNull
    private Integer available;

    @NotNull
    @Min(0)
    private Double netto;

    @NotNull
    @DecimalMax("1.0")
    @DecimalMin("0.0")
    private Double tax;

    private Map<String, String> parameters;

    private Warranty warranty;

    private String serialNumber;
}
