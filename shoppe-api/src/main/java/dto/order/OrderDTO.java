package dto.order;

import dao.domain.Address;
import dao.domain.Delivery;
import dao.enums.OrderStatus;
import dao.enums.Payment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class OrderDTO {

    private String id;
    private OrderStatus status;
    private Double totalPrice;
    private Address address;
    private Delivery delivery;
    private Payment payment;
    private Long createTime;
    private Long lastUpdateTime;

    @Builder.Default
    private List<OrderItemDTO> orderItems = new ArrayList<>();
}
