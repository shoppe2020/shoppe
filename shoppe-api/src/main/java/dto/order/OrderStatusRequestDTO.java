package dto.order;

import dto.ParentDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import dao.enums.OrderStatus;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderStatusRequestDTO extends ParentDTO {

    private String orderId;
    private OrderStatus statusFrom;
    private OrderStatus statusTo;
    private String description;
}
