package dto.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class OrderItem {

    private String productId;
    private Integer count;

    private Double netto;
    private Double brutto;
    private Double tax;

}
