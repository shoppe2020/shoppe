package dto.order;

import dao.domain.Address;
import dao.enums.Payment;
import dto.ParentDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequestDTO extends ParentDTO {

    @NotNull
    private String deliveryId;

    @NotNull
    private Payment payment;

    private Address address;
}
