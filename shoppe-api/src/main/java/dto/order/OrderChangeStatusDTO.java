package dto.order;

import dao.enums.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class OrderChangeStatusDTO {
    private String orderId;
    private OrderStatus oldStatus;
    private OrderStatus newStatus;
}
