package dto.order;

import dao.domain.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class OrderItemDTO {

    private Product product;
    private Integer count;

    private Double netto;
    private Double brutto;
    private Double tax;

}
