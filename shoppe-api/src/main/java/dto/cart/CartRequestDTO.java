package dto.cart;

import dto.ParentDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CartRequestDTO extends ParentDTO {

    private Map<String, Integer> products;
    private String description;
}
