package dto.cart;

import dto.ParentDTO;
import dto.product.ProductBaseDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CartResponseDTO extends ParentDTO {

    private String id;

    @Builder.Default
    private List<ProductBaseDTO> products = new ArrayList<>();
    private String description = "";
    private boolean removed;
}
