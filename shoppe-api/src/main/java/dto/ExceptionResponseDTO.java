package dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExceptionResponseDTO {
    private int status;
    private String errorCode;
    private String description;
    private String message;

    @Builder.Default
    private long timestamp = new Date().getTime();
    @Builder.Default
    private boolean isTokenExpired = false;
}
