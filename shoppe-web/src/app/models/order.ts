import {Address} from './address';
import {Delivery, OrderStatus} from './enum';
import {Item} from './item';

export class Order{
  id: string;
  address: Address = new Address();
  delivery: Delivery = new Delivery();
  orderItems: Array<OrderItem> = new Array<OrderItem>();
  status: OrderStatus;
  createTime: number;
  lastUpdateTime: number;
  totalPrice: number;
  payment: string;
}
export class OrderItem {
  public brutto: number;
  public netto: number;
  public count: number;
  public product: Item = new Item();
}
export class OrderBody {
  public address: Address;
  public deliveryId: string;
  public payment: string;
}
