
import {Review} from './review';

export class Item {
  public id: string;
  public group: string;
  public name: string;
  public description: string;
  public removed: boolean;
  public available: number;
  public brutto: number;
  public netto:	number;
  public tax: number;
  public parameters: any = new Map();
  public thumbnail: string;
  public images: Array<string>;
  public reviews: Array<Review>;
  public warranty: Warranty;
  public serialNumber: string;
}
export class Warranty{
  public period: number;
  public type: 'NO_WARRANTY';
}
