export class Enum {
  WarrantyType: Array<string> = new Array<string>();
  OrderStatus: Array<string> = new Array<string>();
  ErrorCodes: Array<string> = new Array<string>();
  Delivery: Array<string> = new Array<string>();
}
export class Delivery{
  public id: string;
  public name: string;
  public order: number;
  public price: number = 0;
  public removed: boolean;
}
export enum Payment {
  CASH_ON_DELIVERY = 1,
  PAYU = 2,
  POST_TRANSFER = 3
}
export enum OrderStatus {
  CREATED = 'CREATED',
  IN_PROGRESS = 'IN PROGRESS',
  PAYMENT_START = 'PAYMENT STARTED',
  PAYMENT_FINISH = 'PAYMENT FINISHED',
  PAYMENT_CANCEL = 'PAYMENT CANCELED',
  PAID = 'PAID',
  FINISHED = 'FINISHED'
}
