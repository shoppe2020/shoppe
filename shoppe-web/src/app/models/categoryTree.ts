export class CategoryTree {
  public id: string;
  public name: string;
  public path: string;
  public children: Array<CategoryTree> = [];
  constructor( ) {
    this.id = '0';
    this.path = '0';
    this.name = 'defaultRoot';
    this.children = [];
  }
}

