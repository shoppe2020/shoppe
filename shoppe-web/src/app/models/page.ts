
import {Item} from './item';
import {Order} from './order';

class Page {
  public empty: boolean;
  public first: boolean;
  public last: boolean;
  public number: number;
  public numberOfElements: number;
  public size: number;
  public totalElements: number;
  public totalPages: number;
  constructor() {
    // this.content = new Array<PageableItem>();
  }
}

export class ProductPage extends Page {
  public content: Array<Item>;
}
export class OrderPage extends Page{
  public content: Array<Order>;
  constructor() {
    super();
    this.content = new Array<Order>();
  }
}
