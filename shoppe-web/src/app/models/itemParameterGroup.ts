export class ItemParameterGroup {
  public path: string;
  public options: Array<ItemParameterOption> = new Array<ItemParameterOption>();
}

export class ItemParameterOption{
  public name: string;
  public values: Array<string> = new Array<string>();
}
