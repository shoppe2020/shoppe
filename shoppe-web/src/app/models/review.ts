export class Review {
  public id: string;
  public description: string;
  // public good: boolean;
  public rate: number;
  public username: string;
  // public wrong: boolean;
}
