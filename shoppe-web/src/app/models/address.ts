export class Address {
  public firstname: string;
  public lastname: string;
  public city: string;
  public country: string;
  public homeNo: string;
  public street: string;
  public streetNo: string;
  public zipCode: string;
}
