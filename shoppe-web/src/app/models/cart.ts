import {Item} from './item';

export class Cart {
  id: string;
  products: Array<CartItem>;
  removed: boolean;
  constructor() {  }
}
export class CartItem extends Item{
  public count: number;
}
