import {Item} from './item';
import {Address} from './address';

export class User {
  public id: number;
  public token: string;
  public password: string;
  public email: string;
  public firstName: string;
  public lastName: string;
  public role: string;
  public cartId: string;
  // public ordersIds: Array<>;
  public removed: boolean;
  public observedProductsIds: Array<Item>;
  public address: Address = new Address();
  public verified: boolean = false;

}
