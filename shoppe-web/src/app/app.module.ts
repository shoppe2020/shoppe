import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { TopBarComponent } from './components/shared/top-bar/top-bar.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import { MenuComponent } from './components/shared/menu/menu.component';
import {MatListModule} from '@angular/material/list';
import { ItemsComponent } from './components/items/items.component';
import {MatCardModule} from '@angular/material/card';
import { CartComponent } from './components/cart/cart.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import { RegisterComponent } from './components/register/register.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { LoginComponent } from './components/login/login.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpInterceptorService} from './interceptors/http.interceptor';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { ItemComponent } from './components/items/item/item.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import { SummaryComponent } from './components/summary/summary.component';
import {MatBadgeModule} from '@angular/material/badge';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { AddresFormComponent } from './components/cart/addres-form/addres-form.component';
import { OrderPlacedComponent } from './components/summary/order-placed/order-placed.component';
import {NgScrollbarModule} from 'ngx-scrollbar';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import { OrdersComponent } from './components/orders/orders.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import { UserSettingsUserFormComponent } from './components/user-settings/user-settings-user-form/user-settings-user-form.component';
import {MatDialogModule} from '@angular/material/dialog';
import { FilterComponent } from './components/items/filter/filter.component';
import {MatSelectModule} from '@angular/material/select';
import { SearchComponent } from './components/items/search/search.component';
import { AdminOrdersComponent } from './components/admin/admin-orders/admin-orders.component';
import { AdminItemComponent } from './components/admin/admin-product/admin-item.component';
import { ConfirmComponent } from './components/user-settings/confirm/confirm.component';
import { ChangePasswordComponent } from './components/user-settings/change-password/change-password.component';
import { RemindPasswordComponent } from './components/user-settings/remind-password/remind-password.component';
import {MatTreeModule} from '@angular/material/tree';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {OrderComponent} from './components/orders/order/order.component';
import {AdminOrderComponent} from './components/admin/admin-orders/admin-order/admin-order.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TopBarComponent,
    MenuComponent,
    ItemsComponent,
    CartComponent,
    RegisterComponent,
    LoginComponent,
    ItemComponent,
    SummaryComponent,
    AddresFormComponent,
    OrderPlacedComponent,
    UserSettingsComponent,
    OrdersComponent,
    UserSettingsUserFormComponent,
    FilterComponent,
    SearchComponent,
    AdminOrdersComponent,
    AdminItemComponent,
    ConfirmComponent,
    ChangePasswordComponent,
    RemindPasswordComponent,
    OrderComponent,
    AdminOrderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientTestingModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatButtonModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatTabsModule,
    MatTableModule,
    MatBadgeModule,
    MatCheckboxModule,
    MatRadioModule,
    NgScrollbarModule,
    MatPaginatorModule,
    MatDialogModule,
    MatSelectModule,
    MatTreeModule,
    MatAutocompleteModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
