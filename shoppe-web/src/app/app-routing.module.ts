import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {ItemsComponent} from './components/items/items.component';
import {CartComponent} from './components/cart/cart.component';
import {UserGuard} from './guard/user.guard';
import {RegisterComponent} from './components/register/register.component';
import {LoginComponent} from './components/login/login.component';
import {ItemComponent} from './components/items/item/item.component';
import {SummaryComponent} from './components/summary/summary.component';
import {OrderPlacedComponent} from './components/summary/order-placed/order-placed.component';
import {UserSettingsComponent} from './components/user-settings/user-settings.component';
import {OrdersComponent} from './components/orders/orders.component';
import {AuthorizationGuard} from './guard/authorization.guard';
import {LoggedUserGuard} from './guard/logged-user.guard';
import {AdminOrdersComponent} from './components/admin/admin-orders/admin-orders.component';
import {AdminGuard} from './guard/admin.guard';
import {AdminItemComponent} from './components/admin/admin-product/admin-item.component';
import {ConfirmComponent} from './components/user-settings/confirm/confirm.component';
import {RemindPasswordComponent} from './components/user-settings/remind-password/remind-password.component';
import {AdminOrderComponent} from './components/admin/admin-orders/admin-order/admin-order.component';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [UserGuard]},
  { path: 'home', component: HomeComponent, canActivate: [UserGuard]},
  { path: 'items', component: ItemsComponent, canActivate: [UserGuard]},
  { path: 'cart', component: CartComponent, canActivate: [UserGuard]},
  { path: 'register', component: RegisterComponent, canActivate: [UserGuard, LoggedUserGuard]},
  { path: 'login', component: LoginComponent, canActivate: [UserGuard, LoggedUserGuard]},
  { path: 'item/:idItem', component: ItemComponent, canActivate: [UserGuard]},
  { path: 'summary', component: SummaryComponent, canActivate: [UserGuard]},
  { path: 'order-placed', component: OrderPlacedComponent, canActivate: [UserGuard]},
  { path: 'orders', component: OrdersComponent, canActivate: [UserGuard, AuthorizationGuard]},
  { path: 'user-settings', component: UserSettingsComponent, canActivate: [UserGuard, AuthorizationGuard]},
  { path: 'admin-orders', component: AdminOrdersComponent, canActivate: [UserGuard, AuthorizationGuard, AdminGuard]},
  { path: 'admin-order/:idOrder', component: AdminOrderComponent, canActivate: [UserGuard, AuthorizationGuard, AdminGuard]},
  { path: 'admin-product', component: AdminItemComponent, canActivate: [UserGuard, AuthorizationGuard, AdminGuard]},
  { path: 'admin-product/:idItem', component: AdminItemComponent, canActivate: [UserGuard, AuthorizationGuard, AdminGuard]},
  { path: 'confirm', component: ConfirmComponent},
  { path: 'remind-password', component: RemindPasswordComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
