import {Injectable, SecurityContext} from '@angular/core';
import {HttpService} from './http.service';
import {BehaviorSubject} from 'rxjs';
import {ProductPage} from '../models/page';
import {Item} from '../models/item';
import {Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {MenuService} from './menu.service';
import {ItemParameterGroup, ItemParameterOption} from '../models/itemParameterGroup';
import {PopupService} from './popup.service';
import {strict} from 'assert';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  private pages: BehaviorSubject<Array<ProductPage>> = new BehaviorSubject([]);
  private parameters: BehaviorSubject<Array<ItemParameterGroup>> = new BehaviorSubject<any>([]);
  private selectedFilters: BehaviorSubject<Array<ItemParameterOption>> = new BehaviorSubject<Array<ItemParameterOption>>([]);
  private actualPageNumber: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private searchPhrase: BehaviorSubject<string> = new BehaviorSubject<string>('');
  private homePage: BehaviorSubject<ProductPage> = new BehaviorSubject<ProductPage>(new ProductPage());

  private item: BehaviorSubject<Item> = new BehaviorSubject<Item>(new Item());

  constructor(private httpService: HttpService,
              private domSanitizer: DomSanitizer,
              private menuService: MenuService,
              private popupService: PopupService,
              private router: Router) {
    this.selectedFilters.subscribe( value => {
      this.actualPageNumber.next(0);
    });
    this.searchPhrase.subscribe(value => {
      this.clearFilters();
      this.clearPages();
      this.actualPageNumber.next(0);
      this.loadProducts()
    })
  }
  getHomePage(){
    return this.homePage.asObservable();
  }
  loadHomePage(){
    const params = new Map<string, string>();
    params.set('page', '0');
    params.set('size', '6');
    params.set('sort', 'visited.desc');
    params.set('removed', 'false');
    this.httpService.get('product', params).subscribe((value: ProductPage ) => {
      this.homePage.next(value);
    });
  }
  setSearchPhrase(searchPhrase){
    this.searchPhrase.next(searchPhrase)
  }
  getSearchPhrase(){
    return this.searchPhrase.asObservable();
  }

  setActualPage(pageNumber: number){
    this.actualPageNumber.next(pageNumber);
  }
  getActualPageNumberValue(){
    return this.actualPageNumber.value;
  }
  getActualPageNumber(){
    return this.actualPageNumber.asObservable();
  }
  getProducts(){
    return this.pages.asObservable();
  }
  getProduct(){
    return this.item.asObservable();
  }
  getParametersForCatergory(path){
    const params = this.parameters.value.find(param => {
      if (param.path === path)
        return param;
    });
    if(params)
      return params.options;
    else
      return [];
  }
  loadProduct(idItem){
    this.httpService.get('product/' + idItem).subscribe((value: Item ) => {
      this.item.next(value);
    });
  }
  editProduct(item: Item){
    this.httpService.put('product', JSON.stringify(item)).subscribe( (value: Item) => {
      this.router.navigate(['item/' + value.id]);
      this.popupService.show('Product has been edited');
    });
  }
  addProduct(item: Item){
    this.httpService.post('product', JSON.stringify(item)).subscribe( (value: Item) => {
      this.router.navigate(['item/' + value.id]);
      this.popupService.show('Product has been added');
    });
  }
  deleteProduct(id){
    this.httpService.delete('product/' + id).subscribe(()=>{
      this.popupService.show('Product has been removed');
      this.router.navigate(['items'])
      let actualPageNumber = this.actualPageNumber.value;
      let actualCategory = this.menuService.getActualCategoryValue();
      this.loadProducts(actualPageNumber, actualCategory.path, 20);
    }, error => {
      this.popupService.show('Product has not been removed');
    })
  }
  clearPages(){
    this.pages.next([]);
  }
  clearFilters(){
    this.selectedFilters.next([]);
  }
  clearProduct(){
    this.item.next(new Item())
  }
  loadProducts(pageNumber = 0, categoryId: string = '0', pageSize= 20){
    if (pageNumber === 0){
      this.pages.next([]);
    }
    const params = new Map<string, string>();
    params.set('group', categoryId.toString());
    params.set('page', pageNumber.toString());
    params.set('size', pageSize.toString());
    params.set('removed', 'false');
    if(this.searchPhrase.value){
      params.set('name', this.searchPhrase.value);
    }
    for (const filter of this.selectedFilters.value){
      params.set("parameters." + filter.name, filter.values.join(','))
    }
    this.httpService.get('product', params).subscribe((value: ProductPage ) => {
      this.pages.next(this.pages.getValue().concat(value));
    });
  }
  getFiltersParameters(){
    const map = new Map<string, string>();
    for (const filter of this.selectedFilters.value){
      map.set("parameter." + filter.name, filter.values.join(','))
    }
    return map;
  }
  getProductRate(){
    let result = 0;
    if (this.item.value.reviews){
      for (const item of this.item.value.reviews){
        result += item.rate;
      }
      return (result / this.item.value.reviews.length).toFixed(2);
    }
    return '-';
  }
  getCategoryFilters(){
    return this.parameters.asObservable();
  }
  loadProductFilters(){
    this.httpService.get('product/parameters').subscribe((value: any) => {
      const filters = new Array<ItemParameterGroup>();
      if (value){
        for (const i in value) {
          if (value.hasOwnProperty(i)){
            const possibleFilter = new ItemParameterGroup();
            possibleFilter.path = i;
            for (const o in value[i]){
              if (value[i].hasOwnProperty(o)) {
                const option = new ItemParameterOption();
                option.name = o;
                option.values = value[i][o];
                possibleFilter.options.push(option);
              }
            }
            filters.push(possibleFilter);
          }
        }
      }
      this.parameters.next(filters);
    });
  }
  setFilters(filterOptions){
    const oldFitlers = this.selectedFilters.value;
    this.selectedFilters.next(filterOptions);
    if(oldFitlers != filterOptions){
      let actualPageNumber = this.actualPageNumber.value;
      let actualCategory = this.menuService.getActualCategoryValue();
      this.loadProducts(actualPageNumber, actualCategory.path, 20);
    }
  }

  sanitize(src){
    return this.domSanitizer.bypassSecurityTrustUrl('data:image/png;base64, ' + src );
  }
}
