import { Injectable } from '@angular/core';
import * as Cookies from 'js-cookie';

@Injectable({
  providedIn: 'root'
})
export class CookieService {

  constructor() { }

  delete(name: string) {
    Cookies.remove(name);
  }

  set(name: string, value: string, expires?: number) {
    if (name === 'userId'){
      Cookies.set(name, value, { expires, sameSite: 'none', secure: true});
    }
    else{
      Cookies.set(name, value, { expires, sameSite: 'strict'/*, secure: true*/});
    }
  }

  check(name: string) {
    if (Cookies.get(name) == null){
      return false;
    }
    return true;
  }

  get(name: string) {
    return Cookies.get(name);
  }
}
