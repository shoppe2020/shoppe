import { Injectable } from '@angular/core';
import {HttpService} from './http.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {CategoryTree} from '../models/categoryTree';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private categoriesTree: BehaviorSubject<CategoryTree> = new BehaviorSubject(null);
  private actualCategory: BehaviorSubject<CategoryTree> = new BehaviorSubject<CategoryTree>(new CategoryTree());
  constructor(private httpService: HttpService) { }

  getCategoriesTree(): Observable<CategoryTree>{
    return this.categoriesTree.asObservable();
  }
  getActualCategory(){
    return this.actualCategory.asObservable();
  }
  getActualCategoryValue(){
    return this.actualCategory.value;
  }
  loadCategories(){
    this.httpService.get('home/root')
      .subscribe(val => {
      this.categoriesTree.next(val as CategoryTree);
    });
  }

  searchCategory(matchingTitle, element = this.categoriesTree.value){
    if (element.path === matchingTitle){
      this.actualCategory.next(element);
      return element;
    }else if (element.children != null){
      let result = null;
      for (let i = 0; result == null && i < element.children.length; i++){
        result = this.searchCategory(matchingTitle, element.children[i]);
      }
      return result;
    }
    return null;
  }
}
