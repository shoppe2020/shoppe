import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Order} from '../models/order';
import {OrderPage, ProductPage} from '../models/page';
import {HttpService} from './http.service';
import {PopupService} from './popup.service';
import {OrderStatus} from '../models/enum';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  preparedOrder: BehaviorSubject<Order> = new BehaviorSubject<Order>(new Order());
  placedOrder: BehaviorSubject<Order> = new BehaviorSubject<Order>(new Order());
  userOrders: BehaviorSubject<OrderPage> = new BehaviorSubject<OrderPage>(new OrderPage());

  adminOrder: BehaviorSubject<Order> = new BehaviorSubject<Order>(new Order());
  constructor(private httpService: HttpService, private popupService: PopupService) { }
  changeStatus(status){
    if(status !== this.adminOrder.value.status){
      const obj = {
        newStatus: status,
        oldStatus: this.adminOrder.value.status,
        orderId: this.adminOrder.value.id
      }
      this.httpService.put('order/status', JSON.stringify(obj)).subscribe(()=>{
        this.popupService.show('Status changed');
        this.loadAdminOrder(this.adminOrder.value.id)
      },error => {
        this.popupService.show('Status not changed');
      });
    }
  }
  loadAdminOrder(id){
    const params = new Map<string, string>();
    params.set('sort', '')
    this.httpService.get('order/' + id).subscribe( (value: Order) => {
      this.adminOrder.next(value);
    },error => {
      this.popupService.show('Cannot find resource');
    })
  }
  getAdminOrder(){
    return this.adminOrder.asObservable();
  }
  loadAdminOrders(pageNumber = 0, pageSize= 20){
    const params = new Map<string, string>();
    params.set('page', pageNumber.toString());
    params.set('size', pageSize.toString());
    params.set('sort', 'createTime,desc')
    this.httpService.get('order/list', params).subscribe((value: OrderPage ) => {
      this.userOrders.next(value);
    });
  }

  setPreparedOrder(order: Order){
    this.preparedOrder.next(order);
  }
  getPreparedOrder(){
    return this.preparedOrder.asObservable();
  }
  setPlacedOrder(order: Order){
    this.placedOrder.next(order);
  }
  getPlacedOrder(){
    return this.placedOrder.asObservable();
  }

  getOrders(){
    return this.userOrders.asObservable();
  }


  loadUserOrders(pageNumber = 0, pageSize= 20){
    const params = new Map<string, string>();
    params.set('page', pageNumber.toString());
    params.set('size', pageSize.toString());
    this.httpService.get('order/self', params).subscribe((value: OrderPage ) => {
      this.userOrders.next(value);
    });
  }
  getDeliveryTitle(deliveryType) {
    switch (deliveryType) {
      case 'COURIER':
        return 'Delivery by courier with payment online';
      case 'COURIER_CASH_ON_DELIVERY':
        return 'Delivery by courier with payment on delivery';
      case 'AT_THE_POINT':
        return 'Point delivery';
      case 'SELF_PICKUP':
        return 'Self pickup';
    }
  }
  getPaymentTitle(payment){
    switch (payment) {
      case '1':
        return 'Cash on delivery';
      case '2':
        return 'Payment provider PayU';
      case '3':
        return 'Post transfer';
    }
  }
  getStatusString(status){
    return OrderStatus[status];
  }
}
