import { Injectable } from '@angular/core';
import {HttpService} from './http.service';
import {BehaviorSubject} from 'rxjs';
import {Cart} from '../models/cart';
import {PopupService} from './popup.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private cart: BehaviorSubject<Cart> = new BehaviorSubject<Cart>( null );
  constructor(private httpService: HttpService, private popupService: PopupService) {  }
  getCart(){
    return this.cart.asObservable();
  }
  loadCart(){
    this.httpService.get('cart/self').subscribe((value: Cart) => {
      this.cart.next(value);
    });
  }

  addToCart(product, quantity = 1){
    let products = new Map<string, number>();
    products.set(product.id, quantity);
    products = this.map_to_object(products);
    this.httpService.post('cart/add', JSON.stringify( products )).subscribe(value => {
      this.popupService.show('Product added', 'close');
      this.loadCart();
    }, error => {
      this.popupService.show('Error while adding product', 'close');
    });
  }
  deleteFromCart(product, quantity = 1){
    let products = new Map<string, number>();
    products.set(product.id, quantity);
    products = this.map_to_object(products);
    this.httpService.post('cart/remove', JSON.stringify( products )).subscribe(value => {
      this.popupService.show('Product deleted', 'close');
      this.loadCart();
    }, error => {
      this.popupService.show('Error while adding product', 'close');
    });
  }
  totalNetto(): number {
    if (this.cart.value) {
      let totalNetto = 0;
      for (const product of this.cart.value.products) {
        totalNetto += product.count * product.netto;
      }
      return totalNetto;
    }
    return 0;
  }
  totalBrutto(): number {
    if (this.cart.value){
      let totalBrutto = 0;
      for (const product of this.cart.value.products){
        totalBrutto += (product.count * product.brutto);
      }
      return totalBrutto as number;
    }
    return 0;
  }
  private map_to_object(map) {
    const out = Object.create(null);
    map.forEach((value, key) => {
      if (value instanceof Map) {
        out[key] = this.map_to_object(value);
      }
      else {
        out[key] = value;
      }
    });
    return out;
  }
}

