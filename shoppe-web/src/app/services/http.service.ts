import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {CookieService} from './cookie.service';


@Injectable({
  providedIn: 'root'
})
export class HttpService {

  APIurl = 'https://localhost:8443/api/v1/';
  constructor(private httpClient: HttpClient, private cookieService: CookieService) { }
  get(method, params: Map<string, string> = new Map(), headers: Map<string, string> = new Map<string, string>()){
    const httpHeaders = this.getHeaders(headers);
    const httpParams = this.getParams(params);
    return this.httpClient.get(this.APIurl + method, { params: httpParams, headers: httpHeaders});
  }
  post(method, body, params: Map<string, string> = new Map<string, string>(), headers: Map<string, string> = new Map<string, string>()){
    const httpHeaders = this.getHeaders(headers);
    const httpParams = this.getParams(params);
    return this.httpClient.post(this.APIurl + method, body , { params: httpParams, headers: httpHeaders});
  }
  put(method, body, params: Map<string, string> = new Map<string, string>(), headers: Map<string, string> = new Map<string, string>()){
    const httpHeaders = this.getHeaders(headers);
    const httpParams = this.getParams(params);
    return this.httpClient.put(this.APIurl + method, body , { params: httpParams, headers: httpHeaders });
  }
  delete(method, params: Map<string, string> = new Map(), headers: Map<string, string> = new Map<string, string>()){
    const httpHeaders = this.getHeaders(headers);
    const httpParams = this.getParams(params);
    return this.httpClient.delete(this.APIurl + method, { params: httpParams, headers: httpHeaders});
  }
  private getParams(params: Map<string, string> = new Map()){
    let httpParams = new HttpParams();
    for (const key of params.keys()) {
      httpParams = httpParams.append(key, params.get(key));
    }
    return httpParams;
  }
  private getHeaders(headers: Map<string, string> = new Map()){
    let httpHeaders = new HttpHeaders().append('Content-Type', 'application/json');
    if (this.cookieService.check('accessToken')){
      httpHeaders = httpHeaders.append('Authorization', 'Bearer ' + this.cookieService.get('accessToken'));
    }
    else {
      if (this.cookieService.check('userId')){
        httpHeaders = httpHeaders.append('userId', this.cookieService.get('userId'));
      }
    }
    for (const key of headers.keys()) {
      httpHeaders = httpHeaders.append(key, headers.get(key));
    }
    return httpHeaders;
  }
}
