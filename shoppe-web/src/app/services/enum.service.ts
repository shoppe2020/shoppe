import { Injectable } from '@angular/core';
import {HttpService} from './http.service';
import {BehaviorSubject} from 'rxjs';
import {Delivery, Enum} from '../models/enum';

@Injectable({
  providedIn: 'root'
})
export class EnumService {
  deliveryTypes: BehaviorSubject<Array<Delivery>> = new BehaviorSubject<Array<Delivery>>([]);
  warrantyType: BehaviorSubject<Array<string>> = new BehaviorSubject<Array<string>>([]);
  orderStatus: BehaviorSubject<Array<string>> = new BehaviorSubject<Array<string>>([]);
  errorCodes: BehaviorSubject<Array<string>> = new BehaviorSubject<Array<string>>([]);
  constructor(private httpService: HttpService) {
    this.httpService.get('delivery').subscribe((value: Array<Delivery>) => {
      this.deliveryTypes.next(value);
    });
  }
  getDeliveryTypes(){
    return this.deliveryTypes.asObservable();
  }
  getWarrantyType(){
    return this.warrantyType.asObservable();
  }
  getOrderStatus(){
    return this.orderStatus.asObservable();
  }
  getErrorCodes(){
    return this.errorCodes.asObservable();
  }
}
