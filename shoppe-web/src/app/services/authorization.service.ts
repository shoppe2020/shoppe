import { Injectable } from '@angular/core';
import {HttpService} from './http.service';
import {User} from '../models/user';
import {UserService} from './user.service';
import {CartService} from './cart.service';
import {Router} from '@angular/router';
import {CookieService} from './cookie.service';
import {catchError, map, tap} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {PopupService} from './popup.service';

@Injectable({
  providedIn: 'root'
})

export class AuthorizationService {
  constructor(
    private httpService: HttpService,
    private userService: UserService,
    private cookieService: CookieService,
    private cartService: CartService,
    private router: Router,
    private popupService: PopupService) { }
  authenticate(){
    this.httpService.post('user', {}).subscribe((value: UserResponse) => {
      this.cookieService.set('userId', value.userId, 9999);
      this.loadUser();
    });
  }
  authorize(userForm: User) {
    this.httpService.post('token', JSON.stringify(userForm) ).subscribe((value: Token) => {
      this.cookieService.set('accessToken', value.accessToken, value.accessExpire);
      this.cookieService.set('refreshToken', value.refreshToken, value.refreshExpire);
      this.cookieService.set('sessionId', value.sessionId);
      this.loadUser();
      this.router.navigate(['home']);
    }, error => {
      if (error.status === 403){
        this.popupService.show('Incorrect username or password');
      }
      else {
        this.popupService.show('Error occured while login to system. Contact administrator');
      }

    });
  }
  register(userForm: User){
    this.httpService.post('user/register', JSON.stringify(userForm)).subscribe((value: User) => {
      this.router.navigate(['login']);
      this.popupService.show('Account has been created, please confirm your email, or login');
    });
  }
  loadUser() {
    this.httpService.get('user/self').subscribe((value: User) => {
      this.userService.setUser(value);
      this.cartService.loadCart();
    }, error => {
      if (error.status === 404){
        this.cookieService.delete('userId');
        this.authenticate();
      }
    });
  }

  refreshToken() {
    if (this.cookieService.check('refreshToken')) {
      const headers = new Map();
      headers.set('sessionId', this.cookieService.get('sessionId'));
      return this.httpService.put('token', this.cookieService.get('refreshToken'), new Map(), headers).pipe(
        tap((token: Token) => {
        this.cookieService.set('accessToken', token.accessToken, token.accessExpire);
        this.cookieService.set('refreshToken', token.refreshToken, token.refreshExpire);
        this.cookieService.set('sessionId', token.sessionId);
        return token;
      }),
        catchError(error => {
          this.logout();
          return throwError(error);
        }));
    }
    return null;
  }
  isAuthenticated(){
    if (this.userService.getUser().value == null){
      return false;
    }
    return true;
  }
  isUserLogged(){
    return this.userService.getUser().value && this.userService.getUser().value.role !== 'TEMP';
  }
  isAdmin(){
    return this.userService.getUser().value && this.userService.getUser().value.role === 'ADMIN';
  }
  logout(){
    this.userService.setUser(null);
    const headers = new Map<string, string>();
    headers.set('sessionId', this.cookieService.get('sessionId'));
    this.httpService.delete('token', new Map(), headers ).subscribe(() => {
      this.popupService.show('Logout succesful', 'close');
    });
    this.cookieService.delete('accessToken');
    this.cookieService.delete('refreshToken');
    this.cookieService.delete('sessionId');
    this.router.navigate(['home']);
  }

  confirmUser(email, emailToken){
    const params = new Map<string, string>();
    params.set('email', email);
    params.set('emailToken', emailToken);
    this.httpService.put('user/confirm', null, params).subscribe(() => {
      this.popupService.show('Your email has been confirmed')
    },error => {
      this.popupService.show('There was a problem while confirming email')
    })
  }
  changePassword(oldPassword, newPassword){
    const body = {oldPassword: oldPassword, newPassword: newPassword}
    this.httpService.put('user/password', JSON.stringify(body)).subscribe(() => {
      this.popupService.show('Your password has been changed')
    },error => {
      this.popupService.show('There was an error while changing password')
    })
  }

  remindPassword(email){
    const params = new Map<string, string>();
    params.set('email', email);
    this.httpService.get('user/reset', params).subscribe(()=>{
      this.popupService.show('Email with new password send to given email');
    },error => {
      this.popupService.show('Error occured while sending new password');
    }, () => {
      this.router.navigate(['login']);
    })
  }
}

export interface Token {
  accessToken: string;
  refreshToken: string;
  sessionId: string;
  accessExpire: number;
  refreshExpire: number;
  issueAt: number;
}
export interface UserResponse {
  userId: string;
}
export class UserForm {
  email: string;
  firstName: string;
  id: string;
  lastName: string;
  password: string;
  removed: boolean;
  username: string;
}
