import { Injectable } from '@angular/core';
import {User} from '../models/user';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpService} from './http.service';
import {PopupService} from './popup.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private user: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  constructor(private httpService: HttpService, private popupService: PopupService) {
  }
  setUser(user){
    this.user.next(user);
  }
  getUser() {
    return this.user;
  }
  updateUser(user: User) {
    this.httpService.put('user', JSON.stringify(user)).subscribe((res: User) => {
      this.popupService.show('Your profile has been updated');
      this.setUser(res);
    }, error => {
      this.popupService.show('Error occured while updating personal data');
    });
  }
}
