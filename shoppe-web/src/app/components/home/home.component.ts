import {Component, OnDestroy, OnInit} from '@angular/core';
import {Item} from '../../models/item';
import {ItemService} from '../../services/item.service';
import {ProductPage} from '../../models/page';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  items: Array<Item> = new Array<Item>();
  subscription: Subscription;
  constructor(private itemService: ItemService, private router: Router) { }

  ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

  ngOnInit(): void {
    this.subscription = this.itemService.getHomePage().subscribe((value: ProductPage) => {
      this.items = value.content
    })
    this.itemService.loadHomePage();
  }
  openItem(id){
    this.router.navigate(['item/' + id])
  }

  sanitize(src){
    return this.itemService.sanitize(src)
  }

}
