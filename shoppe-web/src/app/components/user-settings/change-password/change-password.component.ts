import { Component, OnInit } from '@angular/core';
import {User} from '../../../models/user';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthorizationService} from '../../../services/authorization.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  user: User;
  confirmPassword = ''
  formGroup = new FormGroup({
    oldPassword: new FormControl(''),
    newPassword: new FormControl(''),
    confirmPassword: new FormControl('')
  })
  constructor(private authorizationService: AuthorizationService) { }

  ngOnInit(): void {
  }
  changePassword(){
    if(this.formGroup.valid){
      if (this.formGroup.get('confirmPassword').value === this.formGroup.get('newPassword').value){
        this.authorizationService.changePassword(this.formGroup.get('oldPassword').value, this.formGroup.get('newPassword').value)
      }
    }
  }
}
