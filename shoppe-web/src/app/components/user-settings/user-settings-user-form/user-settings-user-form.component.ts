import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {User} from '../../../models/user';
import {UserService} from '../../../services/user.service';
import {PopupService} from '../../../services/popup.service';

@Component({
  selector: 'app-user-settings-user-form',
  templateUrl: './user-settings-user-form.component.html',
  styleUrls: ['./user-settings-user-form.component.css']
})
export class UserSettingsUserFormComponent implements OnInit {
  @Input('user')
  user: User;
  constructor(private userService: UserService, private popupService: PopupService) { }
  formCompleteEvent = new EventEmitter();
  formGroup = new FormGroup({
    firstname: new FormControl(''),
    lastname: new FormControl(''),
    email: new FormControl(''),
    city	: new FormControl(''),
    country	: new FormControl(''),
    homeNo	: new FormControl(''),
    street	: new FormControl(''),
    streetNo: new FormControl(''),
    zipCode: new FormControl(''),
    password: new FormControl(''),
  });
  ngOnInit(): void {
    this.formGroup.setControl('firstname', new FormControl(this.user.firstName));
    this.formGroup.setControl('lastname', new FormControl(this.user.lastName));
    this.formGroup.setControl('password', new FormControl(this.user.password));
    this.formGroup.setControl('email', new FormControl(this.user.email));
    if (this.user.address) {
      this.formGroup.setControl('city', new FormControl(this.user.address.city));
      this.formGroup.setControl('country', new FormControl(this.user.address.country));
      this.formGroup.setControl('homeNo', new FormControl(this.user.address.homeNo));
      this.formGroup.setControl('street', new FormControl(this.user.address.street));
      this.formGroup.setControl('streetNo', new FormControl(this.user.address.streetNo));
      this.formGroup.setControl('zipCode', new FormControl(this.user.address.zipCode));
    }
  }
  validate(){
    if (!this.formGroup.invalid){
      this.formCompleteEvent.emit(this.formGroup);
    }
  }
  updateUser(){
    if (this.formGroup.valid){
      const user = new User();
      user.firstName = this.formGroup.get('firstname').value;
      user.lastName = this.formGroup.get('lastname').value;
      user.password = this.formGroup.get('password').value;
      user.email = this.formGroup.get('email').value;
      user.address.city = this.formGroup.get('city').value;
      user.address.country = this.formGroup.get('country').value;
      user.address.homeNo = this.formGroup.get('homeNo').value;
      user.address.street = this.formGroup.get('street').value;
      user.address.streetNo = this.formGroup.get('streetNo').value;
      user.address.zipCode = this.formGroup.get('zipCode').value;
      this.userService.updateUser(user);
    }
    else {
      this.popupService.show('Correct data');
    }
  }
}
