import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSettingsUserFormComponent } from './user-settings-user-form.component';

describe('UserSettingsUserFormComponent', () => {
  let component: UserSettingsUserFormComponent;
  let fixture: ComponentFixture<UserSettingsUserFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSettingsUserFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSettingsUserFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
