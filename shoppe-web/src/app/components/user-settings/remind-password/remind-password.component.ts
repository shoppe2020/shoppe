import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthorizationService} from '../../../services/authorization.service';

@Component({
  selector: 'app-remind-password',
  templateUrl: './remind-password.component.html',
  styleUrls: ['./remind-password.component.css']
})
export class RemindPasswordComponent implements OnInit {

  formGroup = new FormGroup({
    email: new FormControl('')
  })
  constructor(private authorizationService: AuthorizationService) { }

  ngOnInit(): void {
  }
  remindPassword(){
    this.authorizationService.remindPassword(this.formGroup.get('email').value);
  }
}
