import { Component, OnInit } from '@angular/core';
import {AuthorizationService} from '../../../services/authorization.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {

  constructor(private route: ActivatedRoute, private authorizationService: AuthorizationService, private router: Router) { }

  ngOnInit(): void {
    //https://localhost:4200/api/v1/confirm?email=gracmyiimtnaldaylb@awdrt.net&emailToken=5ecc1686d2e21633ce373ace
    const email = this.route.snapshot.queryParamMap.get('email')
    const emailToken = this.route.snapshot.queryParamMap.get('emailToken')

    this.authorizationService.confirmUser(email, emailToken);
    this.router.navigate(['home'])
  }

}
