import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user';
import {MatDialog} from '@angular/material/dialog';
import {UserSettingsUserFormComponent} from './user-settings-user-form/user-settings-user-form.component';
import {ChangePasswordComponent} from './change-password/change-password.component';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit {
  user: User = new User();
  constructor(private userService: UserService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.userService.getUser().subscribe((value: User) => {
      this.user = value;
    });
  }
  openUserDataDialog() {
    const dialogRef = this.dialog.open(UserSettingsUserFormComponent, {
      data: {user: this.user},
      width: '250px'
    });
    dialogRef.componentInstance.user = this.user;

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openChangePasswordDialog(){
    const dialogRef = this.dialog.open(ChangePasswordComponent, {
      data: {user: this.user},
      width: '250px'
    });
    dialogRef.componentInstance.user = this.user;

    dialogRef.afterClosed().subscribe(result => {
    });
  }


}
