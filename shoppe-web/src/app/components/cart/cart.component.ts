import { Component, OnInit } from '@angular/core';
import {Cart, CartItem} from '../../models/cart';
import {CartService} from '../../services/cart.service';
import {Router} from '@angular/router';
import {ItemService} from '../../services/item.service';
import {EnumService} from '../../services/enum.service';
import {FormGroup} from '@angular/forms';
import {Delivery} from '../../models/enum';
import {PopupService} from '../../services/popup.service';
import {Order, OrderItem} from '../../models/order';
import {OrderService} from '../../services/order.service';
import {Item} from '../../models/item';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cart: Cart = new Cart();
  cartTotalBrutto;
  cartTotalNetto;
  isQuantityDisabled = false;
  deliveryTypes = [];
  selectedDelivery: Delivery = new Delivery();
  selectedPayment;
  addressForm: FormGroup = new FormGroup({});
  isCashOnDelivery = false;

  constructor(private cartService: CartService,
              private router: Router,
              private itemService: ItemService,
              private enumService: EnumService,
              private popupService: PopupService,
              private orderService: OrderService) {  }

  ngOnInit(): void {
    this.cartService.getCart().subscribe((value: Cart) => {
      if (value) {
        this.cart = value as Cart;
        this.updateTotal();
      }
    });
    this.enumService.getDeliveryTypes().subscribe((value: Array<Delivery>) => {
      this.deliveryTypes = value;
    });
  }

  updateCart(event, product: CartItem) {
    this.isQuantityDisabled = true;
    const newValue = event.target.value;
    if (newValue > 0) {
      if (newValue > product.count) {
        this.cartService.addToCart(product, newValue - product.count);
      } else if (newValue < product.count) {
        this.cartService.deleteFromCart(product, product.count - newValue);
      }
    } else {
      this.cartService.loadCart();
    }
    this.isQuantityDisabled = false;
  }

  updateTotal() {
    this.cartTotalBrutto = this.cartService.totalBrutto();
    this.cartTotalNetto = this.cartService.totalNetto();
    this.cartTotalBrutto += this.selectedDelivery.price;
    this.cartTotalBrutto = this.cartTotalBrutto.toFixed(2);
    this.cartTotalNetto = this.cartTotalNetto.toFixed(2);
    this.checkIsCashOnDelivery();
  }

  deleteFromCart(product: CartItem) {
    this.cartService.deleteFromCart(product, product.count);
  }

  goToSummary() {
    if (this.cart.products.length !== 0){
      if (this.selectedDelivery.name === 'COURIER' ||
        this.selectedDelivery.name === 'COURIER_CASH_ON_DELIVERY' ||
        this.selectedDelivery.name === 'AT_THE_POINT' ||
        this.selectedDelivery.name === 'SELF_PICKUP')
      {
        if (this.selectedDelivery.name === 'AT_THE_POINT' ||
          this.selectedDelivery.name === 'SELF_PICKUP' ||
          this.addressForm.valid) {
          if (this.selectedPayment !== 1 || this.selectedPayment !== 2 || this.selectedPayment !== 3) {
            const order = new Order();
            if (this.selectedDelivery.name === 'COURIER' ||
              this.selectedDelivery.name === 'COURIER_CASH_ON_DELIVERY'){
              order.address.firstname = this.addressForm.get('firstname').value;
              order.address.lastname = this.addressForm.get('lastname').value;
              order.address.city = this.addressForm.get('city').value;
              order.address.country = this.addressForm.get('country').value;
              order.address.homeNo = this.addressForm.get('homeNo').value;
              order.address.street = this.addressForm.get('street').value;
              order.address.streetNo = this.addressForm.get('streetNo').value;
              order.address.zipCode = this.addressForm.get('zipCode').value;
            }
            else {
              order.address = null;
            }
            order.delivery = this.selectedDelivery;
            order.orderItems = this.convertCartToOrderItems(this.cart);
            order.totalPrice = this.cartTotalBrutto;
            order.payment = this.selectedPayment;
            this.orderService.setPreparedOrder(order);
            this.router.navigate(['summary']);
          }
          else {
            this.popupService.show('Choose payment option');
          }
        }
        else {
          this.popupService.show('Fill personal data');
        }
      }
      else {
        this.popupService.show('Choose delivery type');
      }
    }
    else
      {
      this.popupService.show('Add products to cart');
    }

  }
  sanitize(item){
    return this.itemService.sanitize(item);
  }
  getDeliveryTitle(deliveryType) {
    return this.orderService.getDeliveryTitle(deliveryType);
  }
  onAddresForm(event){
    this.addressForm = event;
  }
  checkIsCashOnDelivery(){
    this.isCashOnDelivery = this.selectedDelivery.name !== 'COURIER';
  }
  convertCartToOrderItems(cart: Cart){
    const orderItems: Array<OrderItem> = new Array<OrderItem>();
    for (const item of cart.products){
      const orderItem = new OrderItem();
      orderItem.brutto = item.count * item.brutto;
      orderItem.netto = item.count * item.netto;
      orderItem.count = item.count;
      orderItem.product = item as Item;
      orderItems.push(orderItem);
    }
    return orderItems;
  }
}
