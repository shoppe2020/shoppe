import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-addres-form',
  templateUrl: './addres-form.component.html',
  styleUrls: ['./addres-form.component.css']
})
export class AddresFormComponent implements OnInit {

  constructor() { }
  @Output() formCompleteEvent = new EventEmitter();
  formGroup = new FormGroup({
    firstname: new FormControl(''),
    lastname: new FormControl(''),
    city	: new FormControl(''),
    country	: new FormControl(''),
    homeNo	: new FormControl(''),
    street	: new FormControl(''),
    streetNo: new FormControl(''),
    zipCode: new FormControl(''),
  });
  ngOnInit(): void {
  }
  validate(){
    if (!this.formGroup.invalid){
      this.formCompleteEvent.emit(this.formGroup);
    }
  }

}
