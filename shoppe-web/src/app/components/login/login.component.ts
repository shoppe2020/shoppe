import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthorizationService} from '../../services/authorization.service';
import {User} from '../../models/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });
  constructor(private authorizationService: AuthorizationService, private router: Router) { }

  ngOnInit(): void {
  }
  onSubmit(){
    if (this.formGroup.valid){
      const user = new User();
      user.email = this.formGroup.get('email').value;
      user.password = this.formGroup.get('password').value;
      this.authorizationService.authorize(user);
    }

  }

}
