import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Order} from '../../../models/order';
import {OrderService} from '../../../services/order.service';
import {Subscription} from 'rxjs';
import {OrderStatus} from '../../../models/enum';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit, OnDestroy {
  @Input('order') preparedOrder: Order = new Order();
  subscription = new Subscription();
  displayedColumns = [ 'name', 'unitPrice', 'quantity', 'totalPrice'];
  @Input('dataSource') dataSource = [];
  constructor(private orderService: OrderService) {
  }

  ngOnInit(): void {

  }
  getDeliveryTitle(deliveryType) {
    return this.orderService.getDeliveryTitle(deliveryType);
  }
  getPaymentTitle(paymentName){
    return this.orderService.getPaymentTitle(paymentName);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  getStatusString(status){
    return this.orderService.getStatusString(status)
  }
}
