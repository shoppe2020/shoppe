import { Component, OnInit } from '@angular/core';
import {Order} from '../../models/order';
import {OrderService} from '../../services/order.service';
import {OrderPage} from '../../models/page';
import {PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orders: Array<Order> = new Array<Order>();
  ordersTotalElements = 0;
  pageEvent: PageEvent = new PageEvent();
  constructor(private orderService: OrderService) { }

  ngOnInit(): void {
    this.orderService.getOrders().subscribe((value: OrderPage) => {
      if (value){
        this.ordersTotalElements = value.totalElements;
        this.orders = [];
        for (const order of value.content){
          this.orders.push(order);
        }
      }
    });
    this.pageEvent.pageIndex = 0;
    this.pageEvent.pageSize = 5;
    this.loadPage(this.pageEvent);
  }

  loadPage(pageEvent: PageEvent){
    this.orderService.loadUserOrders(pageEvent.pageIndex, pageEvent.pageSize);
    return pageEvent;
  }
  goToOrder(order: Order){

  }
  getStatusString(status){
    return this.orderService.getStatusString(status);
  }
}
