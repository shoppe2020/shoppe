import { Component, OnInit } from '@angular/core';
import {Order, OrderBody} from '../../models/order';
import {OrderService} from '../../services/order.service';
import {Router} from '@angular/router';
import {Payment} from '../../models/enum';
import {HttpService} from '../../services/http.service';
import {PopupService} from '../../services/popup.service';
import {CartService} from '../../services/cart.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  preparedOrder: Order = new Order();
  dataSource = [];

  constructor(private orderService: OrderService,
              private router: Router,
              private httpService: HttpService,
              private popupService: PopupService,
              private cartService: CartService) { }

  ngOnInit(): void {
    this.orderService.getPreparedOrder().subscribe((value: Order) => {
      if (value){
        this.preparedOrder = value;
        const newDataSource = [];
        for (const item of value.orderItems){
          newDataSource.push({
            name: item.product.name,
            unitPrice: item.product.brutto + ' $',
            quantity: item.count,
            totalPrice: item.brutto.toFixed(2) + ' $'});
        }
        this.dataSource = newDataSource;
      }
    });
    if (this.preparedOrder.orderItems.length === 0){
      this.router.navigate(['cart']);
    }
  }
  confirmOrder(){
    const orderBody = new OrderBody();
    orderBody.address = this.preparedOrder.address;
    orderBody.deliveryId = this.preparedOrder.delivery.id;
    orderBody.payment = Payment[this.preparedOrder.payment];
    this.httpService.post('order/cart', JSON.stringify(orderBody)).subscribe(((value: Order) => {
      this.orderService.setPlacedOrder(value);
      this.cartService.loadCart();
      this.router.navigate(['order-placed']);
      this.popupService.show('Order has been placed');
    }), error => {
      this.popupService.show('Error while confirming order');
    });
  }
}
