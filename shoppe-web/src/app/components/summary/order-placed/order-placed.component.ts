import {Component, OnDestroy, OnInit} from '@angular/core';
import {OrderService} from '../../../services/order.service';
import {Order} from '../../../models/order';

@Component({
  selector: 'app-order-placed',
  templateUrl: './order-placed.component.html',
  styleUrls: ['./order-placed.component.css']
})
export class OrderPlacedComponent implements OnInit, OnDestroy {
  placedOrder: Order;
  constructor(private orderService: OrderService) { }

  ngOnDestroy(): void {
    this.orderService.setPlacedOrder(new Order());
  }

  ngOnInit(): void {
    this.orderService.getPlacedOrder().subscribe((value: Order) => {
      this.placedOrder = value;
    }).unsubscribe();
  }

}
