import { Component, OnInit } from '@angular/core';
import {User} from '../../models/user';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthorizationService} from '../../services/authorization.service';
import {PopupService} from '../../services/popup.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  formGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
    passwordConfirmation: new FormControl(''),
    lastName: new FormControl(''),
    firstName: new FormControl('')
  });

  form = new FormControl();
  constructor(private authorizationService: AuthorizationService, private popupService: PopupService) { }

  ngOnInit(): void { }
  onSubmit(){
    if (this.formGroup.valid){
      const user = new User();
      user.email = this.formGroup.get('email').value;
      user.password = this.formGroup.get('password').value;
      user.firstName = this.formGroup.get('firstName').value;
      user.lastName = this.formGroup.get('lastName').value;
      if ( user.password === this.formGroup.get('passwordConfirmation').value){
        if(user.password.match(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/)){
          this.authorizationService.register(user);
        }
        else{
          this.popupService.show("Password must contain 8 characters and at least one letter and one number")
        }
      }
      else{
        this.popupService.show("Passwords doesn't match")
      }
    }
  }
}
