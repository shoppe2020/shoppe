import { Component, OnInit } from '@angular/core';
import {Order} from '../../../../models/order';
import {OrderService} from '../../../../services/order.service';
import {ActivatedRoute, Router} from '@angular/router';
import {OrderStatus} from '../../../../models/enum';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-admin-order',
  templateUrl: './admin-order.component.html',
  styleUrls: ['./admin-order.component.css']
})
export class AdminOrderComponent implements OnInit {
  order: Order = new Order();
  dataSource = []
  subscription = new Subscription();
  constructor(private orderService: OrderService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('idOrder');

    this.subscription.add(this.orderService.adminOrder.subscribe((value: Order) => {
      const newDataSource = [];
      for (const item of value.orderItems){
        newDataSource.push({
          name: item.product.name,
          unitPrice: item.product.brutto + ' $',
          quantity: item.count,
          totalPrice: item.brutto.toFixed(2) + ' $'});
      }
      this.dataSource = newDataSource;
    }));

    this.orderService.getAdminOrder().subscribe((value: Order) => {
      if (value){
        this.order = value;
      }
    });
    this.orderService.loadAdminOrder(id);
  }
  changeStatus(status){
    this.orderService.changeStatus(status);
  }
  getDateString(value: number){
    return new Date(value).toLocaleString();
  }
}
