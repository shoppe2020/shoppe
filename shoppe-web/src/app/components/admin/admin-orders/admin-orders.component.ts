import { Component, OnInit } from '@angular/core';
import {Order} from '../../../models/order';
import {PageEvent} from '@angular/material/paginator';
import {OrderService} from '../../../services/order.service';
import {OrderPage} from '../../../models/page';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-orders',
  templateUrl: './admin-orders.component.html',
  styleUrls: ['./admin-orders.component.css']
})
export class AdminOrdersComponent implements OnInit {
  orders: Array<Order> = new Array<Order>();
  ordersTotalElements = 0;
  pageEvent: PageEvent = new PageEvent();
  constructor(private orderService: OrderService, private router: Router) { }

  ngOnInit(): void {
    this.orderService.getOrders().subscribe((value: OrderPage) => {
      if (value){
        this.ordersTotalElements = value.totalElements;
        this.orders = [];
        for (const order of value.content){
          this.orders.push(order);
        }
      }
    });
    this.pageEvent.pageIndex = 0;
    this.pageEvent.pageSize = 5;
    this.loadPage(this.pageEvent);
  }

  loadPage(pageEvent: PageEvent){
    this.orderService.loadAdminOrders(pageEvent.pageIndex, pageEvent.pageSize);
    return pageEvent;
  }
  goToOrder(order: Order){
    this.router.navigate(['admin-order/' + order.id])
  }
  getStatusString(status){
    return this.orderService.getStatusString(status);
  }
}
