import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ItemService} from '../../../services/item.service';
import {Item, Warranty} from '../../../models/item';
import {MenuService} from '../../../services/menu.service';
import {CategoryTree} from '../../../models/categoryTree';
import {FormControl, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';
import {ItemParameterOption} from '../../../models/itemParameterGroup';
import {PopupService} from '../../../services/popup.service';

@Component({
  selector: 'app-admin-product',
  templateUrl: './admin-item.component.html',
  styleUrls: ['./admin-item.component.css']
})
export class AdminItemComponent implements OnInit, OnDestroy {
  subscription = new Subscription();
  categories = new Array<Category>();
  itemId = null;
  parameters = new Array<ItemParameterOptionExtended>()
  isCategorySelected = true;

  formGroup = new FormGroup({
    brutto: new FormControl({value: '', disabled: true}),
    description: new FormControl(''),
    serialNumber: new FormControl(''),
    group: new FormControl(''),
    name: new FormControl(''),
    netto: new FormControl(''),
    quantity: new FormControl(''),
    removed: new FormControl(''),
    tax: new FormControl(''),
    warrantyType: new FormControl('NO_WARRANTY'),
    warrantyPeriod: new FormControl({value: '', disabled: true})
  })
  constructor(private route: ActivatedRoute,
              private itemService: ItemService,
              private menuService: MenuService,
              private popupService: PopupService) {  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('idItem')
    if(id !== null){
      this.itemService.loadProduct(id);
      this.subscription.add(this.itemService.getProduct().subscribe((value: Item) =>{
        this.itemId = value.id;
        this.formGroup = new FormGroup({
          brutto: new FormControl({value: value.brutto, disabled: true}),
          description: new FormControl(value.description),
          group: new FormControl(value.group),
          serialNumber: new FormControl(value.serialNumber),
          name: new FormControl(value.name),
          netto: new FormControl(value.netto),
          quantity: new FormControl(value.available),
          tax: new FormControl(value.tax * 100),
          warrantyType: new FormControl(value.warranty.type),
          warrantyPeriod: new FormControl(value.warranty.period)
        });
        this.checkIsCategorySelected(value.parameters);
      }));

    }
    else{
      this.itemService.clearProduct();
    }
    this.subscription.add(this.menuService.getCategoriesTree().subscribe((value: CategoryTree) => {
      this.categories = this.getAllLeafNodes(value)
    }))
    this.itemService.loadProductFilters();
  }
  updateBrutto(){
    const netto = this.formGroup.get('netto').value;
    const tax = this.formGroup.get('tax').value;
    const valueString = Math.round(((tax / 100) * netto + netto) * 100)/100;
    this.formGroup.setControl('brutto', new FormControl({value: valueString, disabled:true}));
  }
  onSubmit(){
    if(this.formGroup.invalid){
      this.popupService.show('Correct data');
    }
    const item = new Item();
    item.description = this.formGroup.get('description').value;
    item.group = this.formGroup.get('group').value;
    item.name = this.formGroup.get('name').value;
    item.serialNumber = this.formGroup.get('serialNumber').value;
    item.netto = this.formGroup.get('netto').value;
    item.available = this.formGroup.get('quantity').value;
    item.tax = this.formGroup.get('tax').value / 100;
    const warranty = new Warranty();
    warranty.type = this.formGroup.get('warrantyType').value;
    warranty.period = this.formGroup.get('warrantyPeriod').value;
    item.warranty = warranty;
    const parameters = {};
    for (const param of this.parameters){
      if(parameters[param.name] ){
        this.popupService.show('Duplicated parameter');
        return;
      }
      if( !param.selectedValue){
        this.popupService.show('Fill or delete empty parameter');
        return;
      }
      parameters[param.name] =  param.selectedValue
    }
    item.parameters = parameters;
    if(this.itemId){
      item.id = this.itemId;
      this.itemService.editProduct(item);
    }else{
      this.itemService.addProduct(item);
    }
  }
  updateWarrantyPeriod(){
    if(this.formGroup.get('warrantyType').value === 'SELLER' ){
      this.formGroup.setControl('warrantyPeriod', new FormControl('12'))
    }
    else{
      this.formGroup.setControl('warrantyPeriod', new FormControl({value: null, disabled: true}))
    }
  }
  checkIsCategorySelected(properties: Map<string,string> = new Map<string, string>()){
    const path = this.formGroup.get('group').value
    if (path){
      this.isCategorySelected = false;
      const parameters = this.itemService.getParametersForCatergory(path)
      const extendedParameters = new Array<ItemParameterOptionExtended>()
      for (const param of parameters){
        const paramExtended = new ItemParameterOptionExtended(param);
        paramExtended.selectedValue = properties[param.name];
        extendedParameters.push(paramExtended);
      }
      this.parameters = extendedParameters;
    }
    else {
      this.isCategorySelected = true;
    }
  }
  addParameter(){
    this.parameters.push(new ItemParameterOptionExtended(new ItemParameterOption()))
  }
  deleteParameter(parameter){
    const index = this.parameters.indexOf(parameter);
    if (index > -1) {
      this.parameters.splice(index, 1);
    }
  }

  private getAllLeafNodes(category: CategoryTree, path: string = '') {
    const leafNodes = new Array<Category>();
    if (category.children.length === 0) {
      const selectItem = new Category()
      const name = (path + ' / ' + category.name)
      selectItem.name = name.substr(10, name.length);
      selectItem.path = category.path;
      leafNodes.push(selectItem);
    } else {
      for (let child of category.children) {
        leafNodes.push(...this.getAllLeafNodes(child, path + ' / ' + category.name));
      }
    }
    return leafNodes;
  }
}
class Category {
  name: string;
  path: string;
}
 class ItemParameterOptionExtended extends ItemParameterOption{
  selectedValue: string;
   constructor(item: ItemParameterOption) {
     super();
     this.name = item.name;
     this.values = item.values;
   }

 }
