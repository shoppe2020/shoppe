import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../services/user.service';
import {User} from '../../../models/user';
import {AuthorizationService} from '../../../services/authorization.service';
import {CartService} from '../../../services/cart.service';
import {Cart} from '../../../models/cart';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {
  user: User = null;
  cartItemsQuantity = 0;
  isUserMenu = false;
  constructor(private userService: UserService, private authorizationService: AuthorizationService, private cartService: CartService) { }

  ngOnInit(): void {
    this.userService.getUser().subscribe(value => {
      this.user = value;
    });
    this.cartService.getCart().subscribe((value: Cart) => {
      if (value){
        this.cartItemsQuantity = value.products.length;
      }
    });
  }
  logout(){
    this.authorizationService.logout();
  }
  openUserMenu(){
    this.isUserMenu = true;
  }
  closeUserMenu(){
    this.isUserMenu = false;
  }
}
