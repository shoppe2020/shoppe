import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuService} from '../../../services/menu.service';
import {CategoryTree} from '../../../models/categoryTree';
import {ItemService} from '../../../services/item.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit, OnDestroy {
  public categoryTree: CategoryTree = new CategoryTree();
  public actualCategory = new CategoryTree();
  constructor(private homeService: MenuService, private itemService: ItemService) { }

  ngOnInit(): void {
    this.homeService.getCategoriesTree().subscribe(value => {
      this.categoryTree = value;
    });
    this.homeService.getActualCategory().subscribe( value => {
      this.actualCategory = value;
    });
    this.homeService.loadCategories();
  }
  getSubcategory(category){
    this.categoryTree = this.homeService.searchCategory(category.path);
    this.itemService.clearPages();
    this.itemService.clearFilters();
    this.itemService.loadProducts(0, this.categoryTree.path)
  }
  previousCategory(categoryTree){
    let path = categoryTree.path.split('.');
    this.itemService.clearPages();
    this.itemService.clearFilters();
    path.pop();
    path = path.join('.');
    this.categoryTree = this.homeService.searchCategory( path);
    this.actualCategory = categoryTree;
    this.itemService.loadProducts(0, path);
  }
  ngOnDestroy(): void {
  }

}
