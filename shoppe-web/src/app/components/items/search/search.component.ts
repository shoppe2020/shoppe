import { Component, OnInit } from '@angular/core';
import {ItemService} from '../../../services/item.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchPhrase: string = ''
  constructor(private itemService: ItemService) { }

  ngOnInit(): void {
    this.itemService.getSearchPhrase().subscribe((value: string) => {
      this.searchPhrase = value;
    })
  }
  clearInput(){
    this.itemService.setSearchPhrase('');
  }
  search(){
    this.itemService.setSearchPhrase(this.searchPhrase);
  }
}
