import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ItemService} from '../../../services/item.service';
import {Item} from '../../../models/item';
import {CartService} from '../../../services/cart.service';
import {DomSanitizer} from '@angular/platform-browser';
import {AuthorizationService} from '../../../services/authorization.service';
import {MatTable} from '@angular/material/table';
import {PopupService} from '../../../services/popup.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @ViewChild(MatTable) table: MatTable<any>;

  item: Item = new Item();
  dataSource = [];
  quantity = 1;
  constructor(private route: ActivatedRoute,
              private itemService: ItemService,
              private cartService: CartService,
              private domSanitizer: DomSanitizer,
              private authorizationService: AuthorizationService,
              private router: Router,
              private popupService: PopupService) { }

  ngOnInit(): void {
    const idItem = this.route.snapshot.paramMap.get('idItem');
    this.itemService.getProduct().subscribe((item: Item) => {
      this.item = item;
      const newDataSource = []
      for (const param in item.parameters){
        newDataSource.push({parameter: param, name: item.parameters[param]});
      }
      this.dataSource = newDataSource;
    });
    this.itemService.loadProduct(idItem);
  }
  addToCart(product){
      this.cartService.addToCart(product, Math.ceil(this.quantity));
  }
  deleteProduct(id){
    this.itemService.deleteProduct(id)
  }
  validateQuantity(){
    if (this.quantity <= 0){
      this.quantity = 1;
    }
  }
  sanitize(item){
    return this.itemService.sanitize(item);
  }
  getProductRate(){
    return this.itemService.getProductRate();
  }
  isAdmin(){
    return this.authorizationService.isAdmin();
  }
  editProduct(){
    this.router.navigate(['admin-product/' + this.item.id])
  }
}
