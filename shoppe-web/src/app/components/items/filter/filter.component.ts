import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuService} from '../../../services/menu.service';
import {ItemService} from '../../../services/item.service';
import {ItemParameterGroup, ItemParameterOption} from '../../../models/itemParameterGroup';
import {CategoryTree} from '../../../models/categoryTree';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit, OnDestroy {
  filters: Array<ItemParameterGroup> = new Array<ItemParameterGroup>();
  actualFilter: ItemParameterGroup;
  subscription;
  formGroup = new FormGroup({});

  isExpanded = false;
  constructor(private itemService: ItemService, private menuService: MenuService) { }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription = this.itemService.getCategoryFilters().subscribe((value: Array<ItemParameterGroup>) => {
      this.filters = value;
    });
    this.subscription.add(this.menuService.getActualCategory().subscribe((value: CategoryTree) => {
      this.actualFilter = this.filters.find((filter: ItemParameterGroup) => {
        if (filter.path === value.path){
          for (const option of filter.options){
            this.formGroup.addControl(option.name, new FormControl(''));
          }
          return filter;
        }
      });
    }));
    this.itemService.loadProductFilters();
  }

  loadResults(){
    const filterOptions = new Array<ItemParameterOption>();
    for (const possibleOption of this.actualFilter.options){
      if (this.formGroup.get(possibleOption.name).value){
        const filterOption = new ItemParameterOption();
        filterOption.name = possibleOption.name;
        filterOption.values = this.formGroup.get(possibleOption.name).value;
        filterOptions.push(filterOption);
      }
    }
    this.isExpanded = false;
    this.itemService.setFilters(filterOptions);
  }
}
