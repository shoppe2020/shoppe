import {Component, OnInit} from '@angular/core';
import {ItemService} from '../../services/item.service';
import {Item} from '../../models/item';
import {ProductPage} from '../../models/page';
import {CartService} from '../../services/cart.service';
import {Router} from '@angular/router';
import {MenuService} from '../../services/menu.service';
import {CategoryTree} from '../../models/categoryTree';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  products: Array<Item> = new Array<Item>();
  actualPage = 0;
  actualCategory: CategoryTree;
  constructor(private itemService: ItemService,
              private cartService: CartService,
              private router: Router,
              private menuService: MenuService
              ) {}

  ngOnInit(): void {
    this.itemService.getProducts().subscribe((value: Array<ProductPage>) => {
      if (value.length > 0){
        const newPage = value[value.length - 1];
        this.products.push(... newPage.content);
      }
      else {
        this.products = [];
        this.itemService.setActualPage(0);
      }
    });
    this.menuService.getActualCategory().subscribe((value: CategoryTree) => {
      this.actualCategory = value;
    });
    this.itemService.getActualPageNumber().subscribe((value: number) => {
      this.actualPage = value;
    })
  }
  loadMore(){
    this.itemService.setActualPage(this.itemService.getActualPageNumberValue() + 1);
    this.itemService.loadProducts(this.itemService.getActualPageNumberValue(), this.actualCategory.path);
  }
  addToCart(product){
    this.cartService.addToCart(product);
  }
  showDetails(id){
    this.router.navigate(['item/' + id]);
  }
  sanitize(item){
    return this.itemService.sanitize(item);
  }
  getProductParameters(product: Item){
    let result = '';
    let count = 0;
    if (product.parameters){
      for (const param in product.parameters){
        if (count > 2){
          return result.substring(0, result.length - 2);
        }
        result += param + ': ' + product.parameters[param] + ', ';
        count += 1;
      }
    }
  }
}
